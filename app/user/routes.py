from datetime import datetime, date
from flask import render_template, url_for, flash, redirect, request, abort, current_app
from flask_login import current_user, login_user, logout_user, login_required
from sqlalchemy.exc import MultipleResultsFound, OperationalError
from .. import db
from ..models import User
from . import bp
from .forms import LoginForm, RegistrationForm, EditProfileForm


@bp.before_app_request
def before_request():
    if not current_user.is_authenticated:
        return
    try:
        current_user.last_active = datetime.utcnow()
        db.session.commit()
    except OperationalError as e:
        db.session.rollback()
        flash("Database currently locked. Page load may be slow", "error")
        current_app.logger.error(
            f"Database error: {e}. Did not update last active time for {current_user.username}"
        )


@bp.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            current_app.logger.debug("/login - Unsuccessful login for {form.username}")
            flash("Invalid username or password")
            return redirect(url_for("user.login"))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get("next")
        if not next_page or url_parse(next_page).netloc != "":
            return redirect(url_for("main.index"))
        return redirect(url_for(next_page))
    return render_template("user/login.html", title="Sign In", form=form)


@bp.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("main.index"))


@bp.route("/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for("main.index"))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash("Congratulations, you're now registered!")
        current_app.logger.info(f"/register - {form.username.data}")
        return redirect(url_for("main.index"))
    return render_template("user/register.html", title="Register", form=form)


@bp.route("/user/<username>")
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    return render_template("user/user.html", user=user, css="person.css")


@bp.route("/edit_profile", methods=["GET", "POST"])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.bio = form.bio.data
        db.session.commit()
        flash("Your changes have been saved")
        return redirect(url_for("user.user", username=current_user.username))
    elif request.method == "GET":
        form.username.data = current_user.username
        form.bio.data = current_user.bio
    return render_template("user/edit_profile.html", title="Edit Profile", form=form)
