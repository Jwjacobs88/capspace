from datetime import date
from flask import current_app
from sqlalchemy import extract
from .. import db
from ..models import Season


def get_season_data(season_id):
    season = Season.query.filter(Season.id == season_id).first()
    if not season:
        return {}
    return {
        "name": season.name,
        "salary_cap": season.salary_cap,
        "salary_floor": season.salary_floor,
        "minimum_salary": season.minimum_salary,
        "max_buriable_hit": season.max_buriable_hit,
        "bonus_overage_limit": season.bonus_overage_limit,
    }


def get_season_id_from_draft_year(year):
    season_data = query_season_id(year)
    if not season_data:
        return None
    return season_data[0]


def get_current_draft_year():
    draft_date = query_current_draft()
    if draft_date:
        draft_date = draft_date[0]
    else:
        return None

    if draft_date:
        return draft_date.year
    return draft_date


def get_draft_years():
    draft_dates = query_draft_dates()
    # return [(draft[0], draft[1].year) for draft in draft_dates]
    return [draft[0].year for draft in draft_dates if draft[0]]


def query_draft_dates():
    max_year = date.today().year + 4
    return (
        Season.query.filter(Season.draft_date != "")
        .order_by(Season.draft_date.asc())
        .filter(Season.draft_date <= date.today().replace(year=max_year))
        .with_entities(
            # Season.id,
            Season.draft_date
        )
        .all()
    )


def query_season_id(year):
    return (
        Season.query.filter(extract("year", Season.draft_date) == year)
        .order_by(Season.id)
        .with_entities(Season.id)
        .first()
    )


def query_current_draft():
    season = Season.query.get(
        current_app.config["DRAFT_SEASON"] or current_app.config["CURRENT_SEASON"]
    )
    if season:
        return (season.draft_date,)
    return (
        Season.query.filter(Season.draft_date >= date.today())
        .with_entities(
            Season.draft_date,
        )
        .order_by(Season.draft_date.asc())
        .first()
    )
