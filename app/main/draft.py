from datetime import date, datetime
from markupsafe import escape
from sqlalchemy.orm import aliased
from flask import render_template, url_for, redirect, abort, current_app

from .. import db
from ..models import Asset, DraftPick, Team, Person, Season, TeamSeason
from .forms import SelectDraftSeason
from .season import get_season_id_from_draft_year, get_current_draft_year
from .notes import get_notes


def draft_page(year=None):
    default_season = year or get_current_draft_year()
    picks = get_draft_picks(default_season)
    if picks is None:
        abort(404)
    logos = get_logos(get_season_id_from_draft_year(default_season))
    data = {
        "picks": picks,
        "logos": logos,
    }
    data["teams"] = sorted(list(set([pick["team"] for pick in data["picks"]])))
    season_picker = SelectDraftSeason()
    season_picker.season.data = str(default_season)
    return render_template(
        "draft.html",
        title=f"Draft {default_season}",
        css="draft.css",
        data=data,
        form=season_picker,
    )


def get_draft_picks(year=None):
    season = current_app.config["CURRENT_SEASON"]
    if year:
        season = get_season_id_from_draft_year(year)
    if season is None:
        return None
    raw_picks = query_draft_picks(season)
    picks = [
        {
            "round": pick[0],
            "position": pick[1],
            "team": pick[3] or "NHL",
            "original": pick[4] if pick[4] != pick[3] else None,
            "player": pick[5],
            "person_id": pick[6],
            "note": [item["note"] for item in get_notes("draft_pick", pick[2])],
        }
        for pick in raw_picks
    ]
    return picks


def get_logos(season_id=None):
    current_logos = Team.query.with_entities(Team.abbreviation, Team.logo).all()
    logos = {row[0]: row[1] for row in current_logos}
    if season_id:
        season_logos = (
            TeamSeason.query.join(Team, Team.id == TeamSeason.team_id)
            .join(Season, Season.id == TeamSeason.season_id)
            .filter(Season.id == season_id)
            .with_entities(Team.abbreviation, TeamSeason.logo, Team.logo)
            .all()
        )
        for entry in season_logos:
            if entry[1]:
                logos[entry[0]] = entry[1]
    return logos


def query_draft_picks(season_id):
    owner = aliased(Team)
    creator = aliased(Team)
    return (
        DraftPick.query.join(Asset, Asset.id == DraftPick.id)
        .join(creator, creator.id == Asset.originating_team, isouter=True)
        .join(owner, owner.id == Asset.current_team, isouter=True)
        .join(Person, Person.id == DraftPick.person_id, isouter=True)
        .filter(DraftPick.season_id == season_id)
        .order_by(DraftPick.round.asc())
        .order_by(DraftPick.position.asc().nullslast())
        .order_by(creator.abbreviation.asc())
        .with_entities(
            DraftPick.round,
            DraftPick.position,
            DraftPick.id,
            owner.abbreviation,
            creator.abbreviation,
            Person.name,
            Person.id,
        )
        .all()
    )
