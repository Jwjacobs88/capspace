import json
from flask import (
    render_template,
    url_for,
    redirect,
    request,
    flash,
    session,
    abort,
    jsonify,
    current_app,
    g,
)
from flask_login import current_user
from .. import db

from . import (
    bp,
    team,
    person,
    buyout,
    trade,
    draft,
    api,
    free_agency,
    team_season,
    search,
    transaction,
)
from .forms import (
    QualifyingOffer,
    BuyoutForm,
    TradeProposal,
    SaveTradeProposal,
    SelectDraftSeason,
    SearchForm,
)


@bp.before_app_request
def search_form():
    g.search_form = SearchForm()


@bp.route("/index")
@bp.route("/")
def index():
    data = {
        "cap": team_season.homepage_data(),
        "signings": free_agency.get_signings(),
        "free_agents": free_agency.get_free_agents(True, True, True),
    }
    data["show_projected_cap"] = False
    for row in data["cap"]:
        if row["projected_cap"] != row["used_cap"]:
            data["show_projected_cap"] = True
    return render_template("index.html", title="Home", css="homepage.css", data=data)


@bp.route("/about/")
@bp.route("/about")
def about():
    return render_template("about.html", title="About", css="about.css")


@bp.route("/tips/")
@bp.route("/tips")
def tips():
    return render_template("tips.html", title="Tips")


@bp.route("/acknowledgements/")
@bp.route("/acknowledgements")
def thanks():
    return render_template("acknowledgements.html", title="Acknowledgements")


@bp.route("/site-progess/")
@bp.route("/site-progess")
def site_progress():
    return render_template(
        "site_progress.html", title="Site Progress", css="progress.css"
    )


@bp.route("/search")
def search_route():
    text = request.args.get("search")
    data = search.search_data(text)
    if len(data["team"]) == 1 and len(data["person"]) == 0:
        flash("Only one search result found. Redirecting", "search")
        return redirect(url_for("main.team_route", team_id=data["team"][0]["id"]))
    if len(data["team"]) == 0 and len(data["person"]) == 1:
        flash("Only one search result found. Redirecting", "search")
        url_id = f"{data['person'][0]['name'].replace(' ', '-').lower()}-{data['person'][0]['id']}"
        return redirect(url_for("main.person_route", person_id=url_id))
    return render_template(
        "search.html", title=f"Search - {text}", css="search.css", data=data
    )


@bp.route("/teams")
def teams():
    return team.teams_page()


@bp.route("/teams/defunct")
def defunct_teams():
    return team.defunct_teams()


@bp.route("/teams/logos")
def team_logos():
    return team.team_logos()


@bp.route("/team/<team_id>/")
@bp.route("/team/<team_id>")
def team_route(team_id):
    return team.team_page(team_id)


@bp.route("/team/<team_id>/logos")
def team_logo_page(team_id):
    return team.team_logo_page(team_id)


@bp.route("/team/<team_id>/daily-cap/<season>")
@bp.route("/team/<team_id>/daily-cap")
def daily_cap(team_id, season=None):
    data = team_season.daily_cap_data(team_id, season)
    return render_template(
        "daily_cap.html", title="Daily Cap Tracking", css="daily_cap.css", data=data
    )


@bp.route("/person/<person_id>")
def person_route(person_id):
    return person.person_page(person_id)


@bp.route("/buyout", methods=["GET", "POST"])
def buyout_route():
    form = BuyoutForm()
    if form.validate_on_submit():
        # Get data
        person_id = form.person.data
        season_id = form.buyout_after.data
        return redirect(
            url_for(
                "main.buyout_with_results", person_id=person_id, after_season=season_id
            )
        )
    return buyout.buyout_page(form)


@bp.route("/buyout/<person_id>", methods=["GET", "POST"])
def buyout_with_results(person_id):
    season = request.args.get("after_season")
    return buyout.buyout_results(person_id, season)


@bp.route("/qualify")
def qualifying_route():
    form = QualifyingOffer()
    return free_agency.qualifying_offer(form)


@bp.route("/entry-draft", methods=["GET", "POST"])
def entry_draft():
    form = SelectDraftSeason()
    if form.validate_on_submit():
        return redirect(url_for("main.entry_draft_season", year=form.season.data))
    return draft.draft_page()


@bp.route("/entry-draft/<year>", methods=["GET", "POST"])
def entry_draft_season(year):
    form = SelectDraftSeason()
    if form.validate_on_submit():
        return redirect(url_for("main.entry_draft_season", year=form.season.data))
    return draft.draft_page(year)


@bp.route("/trade-proposal", methods=["GET", "POST"])
def trade_proposal():
    form = TradeProposal()
    if form.validate_on_submit():
        trade_data = {
            "team_one": form.team_one.data,
            "team_two": form.team_two.data,
            "picks_one": json.loads(form.picks_one.data),
            "contracts_one": json.loads(form.contracts_one.data),
            "picks_two": json.loads(form.picks_two.data),
            "contracts_two": json.loads(form.contracts_two.data),
            "future_considerations_one": form.future_considerations_one.data,
            "future_considerations_two": form.future_considerations_two.data,
            "future_considerations": form.future_considerations.data,
        }
        session["trade_data"] = trade_data
        return redirect(url_for("main.trade_proposal_display"))
    return trade.trade_proposal(form)


@bp.route("/trade", methods=["GET", "POST"])
def trade_proposal_display():
    data = session.get("trade_data", None)
    save_proposal = None
    apply = None
    if not data:
        flash(
            "Trade proposal data not found. Maybe you saved it? Saved proposals are visible from your user profile"
        )
        return redirect(url_for("main.trade_proposal"))
    if current_user.is_authenticated:
        save_proposal = SaveTradeProposal()
    if save_proposal and save_proposal.validate_on_submit():
        proposal_id = trade.save_proposal(data, current_user.id)
        del session["trade_data"]
        return redirect(url_for("main.saved_proposal", proposal_id=proposal_id))
    return trade.trade_page(data, save_proposal, apply)


@bp.route("/trade-proposal/<proposal_id>", methods=["GET", "POST"])
def saved_proposal(proposal_id):
    transaction = trade.get_transaction_record(proposal_id)
    if not transaction:
        abort(404)
    if transaction.type == "Trade":
        return redirect(url_for("main.trade_display", trade_id=proposal_id))
    return trade.trade_page(proposal_id, None)


@bp.route("/trade/<trade_id>")
def trade_display(trade_id):
    transaction = trade.get_transaction_record(trade_id)
    if not transaction:
        abort(404)
    if transaction.type != "Trade":
        return redirect(url_for("main.saved_proposal", proposal_id=trade_id))
    return trade.trade_page(trade_id)


@bp.route("/trades")
def trades_display():
    from_date = request.args.get("from")
    to_date = request.args.get("to")
    return trade.trades_page(from_date, to_date)


@bp.route("/free-agent")
def free_agents():
    return free_agency.free_agent_page()


@bp.route("/free-agent/RFA")
def restricted_free_agents():
    return free_agency.free_agent_page(ufa=False)


@bp.route("/free-agent/UFA")
def unrestricted_free_agents():
    return free_agency.free_agent_page(rfa=False)


@bp.route("/signings/")
@bp.route("/signings")
def signings():
    from_date = request.args.get("from")
    to_date = request.args.get("to")
    return free_agency.signings_page(from_date, to_date)


@bp.route("/transactions/")
@bp.route("/transactions")
def transactions_page():
    from_date = request.args.get("from")
    to_date = request.args.get("to")
    return transaction.transaction_page(from_date, to_date)
