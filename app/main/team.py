from datetime import date, datetime
from flask import render_template, url_for, redirect, abort, current_app
from sqlalchemy import func, or_, case, column, select
from sqlalchemy.orm import aliased
from sqlalchemy.exc import MultipleResultsFound
from .. import db
from ..models import (
    Team,
    DraftPick,
    Season,
    Asset,
    Person,
    Contract,
    ContractYear,
    Transaction,
    TransactionAsset,
    BuyoutYear,
    RetainedSalary,
    TeamSeason,
    Penalty,
    PenaltyYear,
    SigningRights,
    ProfessionalTryout,
    DailyCap,
    GoalieSeason,
    SkaterSeason,
    Note,
)
from . import team_season


def teams_page():
    teams = Team.query.filter(Team.active == True).order_by("name").all()
    return render_template(
        "teams.html", title="Teams", teams=teams, logos=False, css="teams.css"
    )


def team_logos():
    teams = Team.query.order_by("name").all()
    return render_template(
        "teams.html", title="Teams' Logos", teams=teams, logos=True, css="teams.css"
    )


def defunct_teams():
    teams = Team.query.filter(Team.active == False).order_by("name").all()
    return render_template(
        "teams.html", title="Defunct Teams", teams=teams, logos=False, css="teams.css"
    )


def team_page(team_id):
    team_data = None
    if team_id.isdigit():
        team_data = Team.query.filter_by(id=team_id).one_or_none()
        current_app.logger.debug("/team/ID - Internal ID")
    elif len(team_id) == 3:
        team_data = Team.query.filter_by(abbreviation=team_id.upper()).one_or_none()
        current_app.logger.debug("/team/ABBR - Abbreviation")
    else:
        team_id = team_id.replace("-", " ").replace("_", " ")
        team_id = " ".join([word.capitalize() for word in team_id.split()])
        try:  # If team_id processed to team name exactly, return it
            team_data = Team.query.filter_by(name=team_id).one_or_none()
            if team_data is None:
                # If team_id matches one team name, redirect to team name
                team_data = Team.query.filter(
                    Team.name.like(f"%{team_id}%")
                ).one_or_none()
                current_app.logger.debug(f"/team/name - Matched {team_data.name}")
                return redirect(
                    url_for(
                        "main.team_route",
                        team_id=team_data.name.replace(" ", "-").lower(),
                    ),
                    code=303,
                )
        except MultipleResultsFound:
            abort(
                500, "This URL could refer to multiple teams. Please be more specific"
            )
    if team_data is None:
        abort(404)

    if not team_data.active:
        return defunct_team_page(team_data)

    season_names = get_season_names()
    data = get_data(team_data.id)
    limit_years(data)
    set_titles(data["draft_picks"])
    return render_template(
        "team.html",
        title=team_data.name,
        css="team.css",
        get_age=age_from_birthdate,
        seasons=season_names,
        data=data,
        logo=team_data.logo,
    )


def defunct_team_page(team):
    successor = None
    predecessor = None
    notes = []
    if team.successor:
        successor = Team.query.get(team.successor)
    if team.predecessor:
        predecessor = Team.query.get(team.predecessor)
    notes = (
        Note.query.filter(Note.record_table == "team")
        .filter(Note.record_id == team.id)
        .all()
    )
    return render_template(
        "defunct_team.html",
        title=team.name,
        css="team.css",
        team=team,
        predecessor=predecessor,
        successor=successor,
        notes=notes,
    )


def team_logo_page(team_id):
    if team_id.isdigit():
        team_data = Team.query.filter_by(id=team_id).one_or_none()
    elif len(team_id) == 3:
        team_data = Team.query.filter_by(abbreviation=team_id.upper()).one_or_none()
    else:
        team_data = Team.query.filter(Team.name.like(f"%{team_id}%")).count()
        if team_data != 1:
            abort(
                404,
                f"Team id: {team_id} could indicate one of {team_data} teams. Please be more specific",
            )
        team_data = Team.query.filter(Team.name.like(f"%{team_id}%")).first()

    successors = []
    team = team_data
    while team.successor:
        successors.append(Team.query.get(team.successor))
        team = successors[-1]
    successors.sort(key=lambda team: team.first_year)

    predecessors = []
    team = team_data
    while team.predecessor:
        predecessors.append(Team.query.get(team.predecessor))
        team = predecessors[-1]
    predecessors.sort(key=lambda team: team.first_year)

    print(f"{team_data.name} {successors=} {predecessors=}")
    successors = [get_logo_history(team.id, team.name) for team in successors]
    predecessors = [get_logo_history(team.id, team.name) for team in predecessors]
    current = get_logo_history(team_data.id, team_data.name)
    data = {
        "logos": predecessors + [current] + successors,
        "team": team_data,
    }
    return render_template(
        "logos.html", title=f"{team_data.name} Logo History", css="logos.css", data=data
    )


def get_logo_history(team_id, team_name=None):
    query = (
        TeamSeason.query.join(Season, Season.id == TeamSeason.season_id)
        .filter(TeamSeason.team_id == team_id)
        .group_by(TeamSeason.logo)
        .order_by(Season.draft_date.asc())
        .with_entities(
            func.min(Season.name),
            func.max(Season.name),
            TeamSeason.logo,
        )
        .all()
    )
    data = {}
    data["logos"] = [
        {
            "first_season": team[0],
            "last_season": team[1],
            "logo": team[2],
        }
        for team in query
    ]
    last_season = Season.query.filter(
        Season.name == data["logos"][-1]["last_season"]
    ).first()
    if last_season.draft_date and last_season.draft_date > date.today():
        data["logos"][-1]["last_season"] = "Present"
    if team_name:
        data["name"] = team_name
    return data


def get_data(team_id):
    team_data = Team.query.filter_by(id=team_id).first()
    season = Season.query.get(current_app.config["CURRENT_SEASON"])
    if not team_data:
        current_app.logger.error(f"[/api]/team/team_id - No team with provided ID")
        abort(404)
    api_data = {
        "id": team_data.id,
        "name": team_data.name,
        "abbreviation": team_data.abbreviation,
        "first_year": team_data.first_year,
        "draft_picks": get_draft_picks(team_data.id),
        "roster": get_roster(team_data.id),
        "season": team_season.get_team_season(team_data.id, None),
        "salary_cap": season.salary_cap,
    }
    api_data["injuries"] = sum(
        [
            1
            for player in api_data["roster"]
            if player["ir"] or player["ltir"] or player["soir"]
        ]
    )
    api_data["roster_count"] = sum(
        [1 for player in api_data["roster"] if player["signed"]]
    )
    api_data["cap_hit"] = sum(
        [
            person["years"][0]["cap_hit"]
            for person in api_data["roster"]
            if "years" in person and len(person["years"]) >= 1
        ]
    )
    non_roster = get_non_roster(team_data.id)
    if non_roster:
        api_data["non_roster"] = non_roster
        if "buyouts" in api_data["non_roster"]:
            api_data["cap_hit"] += sum(
                [
                    person["years"][0]["cap_hit"]
                    for person in api_data["non_roster"]["buyouts"]
                    if "years" in person and len(person["years"]) >= 1
                ]
            )
        if "retained" in api_data["non_roster"]:
            api_data["cap_hit"] += sum(
                [
                    person["years"][0]["cap_hit"]
                    for person in api_data["non_roster"]["retained"]
                    if "years" in person and len(person["years"]) >= 1
                ]
            )
        if "non-roster" in api_data["non_roster"]:
            api_data["cap_hit"] += sum(
                [
                    person["years"][0]["cap_hit"]
                    for person in api_data["non_roster"]["non-roster"]
                    if "years" in person and len(person["years"]) >= 1
                ]
            )
    current_app.logger.debug(f"[/api]/team/{team_id} - Gathering data")
    return api_data


def limit_years(data):
    season_limit = current_app.config["DISPLAY_X_SEASONS"]
    if "roster" not in data:
        return
    roster = data["roster"]
    for idx in range(len(roster)):
        if "years" not in roster[idx]:
            continue
        while len(roster[idx]["years"]) > season_limit:
            roster[idx]["display_overflow"] = True
            del roster[idx]["years"][season_limit]
    if "non_roster" not in data:
        return
    non_roster = data["non_roster"]
    for key in non_roster:
        for idx in range(len(non_roster[key])):
            if "years" not in non_roster[key][idx]:
                continue
            while len(non_roster[key][idx]["years"]) > season_limit:
                non_roster[key][idx]["display_overflow"] = True
                del non_roster[key][idx]["years"][season_limit]


def set_titles(picks):
    for idx_s in range(len(picks)):
        for idx_p in range(len(picks[idx_s]["picks"])):
            title = []
            if picks[idx_s]["picks"][idx_p]["traded_away"]:
                title.append("Traded Away")
            if picks[idx_s]["picks"][idx_p]["condition"]:
                title = title + picks[idx_s]["picks"][idx_p]["condition"]
            title = "\n".join(title)
            if title != "":
                picks[idx_s]["picks"][idx_p]["title"] = title


def age_from_birthdate(birthdate):
    birth_date = datetime.strptime(birthdate, current_app.config["DATE_FMT"]).date()
    age_days = (date.today() - birth_date).days
    age_years = age_days // 365
    return age_years


def get_teams():
    raw_teams = query_teams()
    return [tuple(team) for team in raw_teams]


def get_draft_picks(team_id):
    raw_data = query_draft_picks(team_id)

    try:
        season = raw_data[0][5]
    except IndexError:
        return []
    data = [{"season": season, "picks": []}]
    for pick in raw_data:
        if pick[5] != season:
            season = pick[5]
            data.append({"season": season, "picks": []})
        data[-1]["picks"].append(
            {
                "team": pick[0],
                "traded_away": pick[1] != team_id and pick[2] == team_id,
                "traded": pick[1] == team_id and pick[2] != team_id,
                "round": pick[3],
                "position": pick[4],
                "condition": get_pick_conditions(pick[7]),
                "logo": pick[8],
            }
        )
    return data


def get_pick_conditions(pick_id):
    raw_data = query_pick_conditions(pick_id)
    return [condition[0] for condition in raw_data] or None


def get_roster(team_id):
    raw_roster = query_roster(team_id)
    raw_roster += query_professional_tryouts(team_id)
    raw_roster += query_signing_rights(team_id)
    raw_roster += query_pending_ufa(team_id)
    data = [
        {
            "name": person[0],
            "position": person[1],
            "birthdate": person[2].strftime(current_app.config["DATE_FMT"]),
            "expires_as": get_expiration(person[3]),
            "id": person[3],
            "years": get_contract_years(person[3], "SOIR" in person[7].split(",")),
            "retention": get_total_retention(person[3]),
            "nmc": person[4],
            "ntc": person[5],
            "clause_limits": person[6],
            "ir": "IR" in person[7].split(","),
            "ltir": "LTIR" in person[7].split(","),
            "soir": "SOIR" in person[7].split(","),
            "suspended": "Suspended" in person[7],
            "url_id": f"{person[0].lower().replace(' ', '-')}-{person[3]}",
            "signed": (person[8] == "Contract"),
            "pto": (person[8] == "Professional Tryout"),
            "number": person[9],
            "waivers_exempt": person[10],
            "emergency_callup": "Emergency Callup" in person[7].split(","),
        }
        for person in raw_roster
    ]
    return data


def get_future_contracts(team_id):
    current_deals = (
        ContractYear.query.filter(
            ContractYear.season_id == current_app.config["CURRENT_SEASON"]
        )
        .with_entities(ContractYear.contract_id)
        .subquery()
    )
    currently_signed = (
        Contract.query.join(Asset, Asset.id == Contract.id)
        .filter(Contract.id.in_(select(current_deals)))
        .filter(Asset.active == True)
        .with_entities(Contract.person_id)
        .subquery()
    )
    future_deals = (
        Contract.query.join(Person, Person.id == Contract.person_id)
        .join(Asset, Asset.id == Contract.id)
        .filter(Contract.person_id.not_in(select(currently_signed)))
        .filter(Contract.signing_season_id == current_app.config["CURRENT_SEASON"])
        .filter(Person.current_status.like("%Under Contract%"))
        .filter(Asset.type == "Contract")
        .filter(Asset.current_team == team_id)
        .with_entities(
            Person.name,
            Person.position,
            Person.birthdate,
            Person.id,
            Person.current_status,
            Asset.type,
            Person.number,
            Contract.expiration_status,
        )
        .all()
    )
    data = [
        {
            "name": person[0],
            "position": person[1],
            "birthdate": person[2].strftime(current_app.config["DATE_FMT"]),
            "expires_as": person[7],
            "id": person[3],
            "years": get_contract_years(person[3]),
            "url_id": f"{person[0].lower().replace(' ', '-')}-{person[3]}",
            "signed": (person[5] == "Contract"),
            "number": person[6],
        }
        for person in future_deals
    ]
    return data


def get_non_roster(team_id):
    raw_buyout = query_buyouts(team_id)
    raw_retained = query_retained(team_id)
    raw_penalties = query_penalties(team_id)
    raw_non_roster = query_non_roster(team_id)
    raw_non_roster += query_signing_rights(team_id, True)
    raw_non_roster += query_pending_ufa(team_id, True)
    data = {
        "buyouts": [
            {
                "name": person[0],
                "position": person[1],
                "birthdate": person[2].strftime(current_app.config["DATE_FMT"]),
                "id": person[3],
                "years": get_buyout_years(person[4]),  # Contract ID
                "retention": 100 - get_buyout_retention(person[4]),  # Un-retained piece
                "url_id": f"{person[0].lower().replace(' ', '-')}-{person[3]}",
            }
            for person in raw_buyout
        ],
        "retained": [
            {
                "retention": person[0],
                "name": person[1],
                "position": person[2],
                "birthdate": person[3].strftime(current_app.config["DATE_FMT"]),
                "id": person[4],
                "years": get_retained_years(person[5], person[0]),  # Contract ID
                "url_id": f"{person[1].lower().replace(' ', '-')}-{person[4]}",
            }
            for person in raw_retained
        ],
        "penalties": [
            {
                "name": person[0],
                "position": person[1],
                "birthdate": person[2].strftime(current_app.config["DATE_FMT"]),
                "id": person[3],
                "type": person[5],
                "years": get_penalty_years(person[4]),  # Penalty ID
                "url_id": f"{person[0].lower().replace(' ', '-')}-{person[3]}",
            }
            for person in raw_penalties
        ],
        "future": get_future_contracts(team_id),
        "non-roster": [
            {
                "name": person[0],
                "position": person[1],
                "birthdate": person[2].strftime(current_app.config["DATE_FMT"]),
                "id": person[3],
                "years": get_non_roster_years(person[3]),  # Person, Contract IDs
                "retention": get_total_retention(person[3]),
                "expiration_status": get_expiration(person[3]),
                "url_id": f"{person[0].lower().replace(' ', '-')}-{person[3]}",
                "waivers_exempt": person[4],
                "on_loan": person[5],
                "in_juniors": person[6] if type(person[6]).__name__ != "str" else False,
                # Type check because in_juniors' position for pending UFAs is filled with NTC/NMC limits, a string field. We want a boolean
            }
            for person in raw_non_roster
        ],
    }
    retained_buyouts = query_retained_buyouts(team_id)
    for person in retained_buyouts:
        data["buyouts"].append(
            {
                "name": person[0],
                "position": person[1],
                "birthdate": person[2].strftime(current_app.config["DATE_FMT"]),
                "id": person[3],
                "years": get_buyout_years(
                    person[4], person[6]
                ),  # Contract ID, Retention
                "retention": person[6],  # Retention only
            }
        )
    to_delete = []
    for key, value in data.items():
        if value == []:
            to_delete.append(key)
    for key in to_delete:
        del data[key]
    if data == {}:
        return False

    if "retained" in data:
        data["retained"].sort(key=lambda asset: -len(asset["years"]))
    return data


def get_expiration(person_id):
    raw_data = (
        Contract.query.filter(Contract.person_id == person_id)
        .order_by(Contract.signing_date.desc())
        .with_entities(Contract.expiration_status)
        .first()
    )
    return raw_data[0]


def get_contract_years(person_id, soir=False):
    ratio = 1
    if soir:
        ratio = soir_ratio(person_id)
    raw_years = query_contract(person_id)
    data = [
        {
            "cap_hit": int(year[0] * (100 - (year[6] or 0)) / 100),
            "aav": int(year[1] * (100 - (year[6] or 0)) / 100),
            "nhl_salary": int(year[2] * (100 - (year[6] or 0)) / 100),
            "performance_bonuses": int(year[3] * (100 - (year[6] or 0)) / 100),
            "signing_bonus": int(year[4] * (100 - (year[6] or 0)) / 100),
            "season": year[5],
        }
        for year in raw_years
    ]
    if len(data):
        data[0]["cap_hit"] = int(ratio * data[0]["cap_hit"])
    return data


def soir_ratio(person_id):
    # Last season's daily cap and days in season
    last_season = (
        Season.query.filter(
            Season.free_agency_opening
            < (
                Season.query.filter(Season.id == current_app.config["CURRENT_SEASON"])
                .with_entities(Season.free_agency_opening)
                .order_by(Season.free_agency_opening.desc())
                .limit(1)
                .scalar_subquery()
            )
        )
        .with_entities(Season.id)
        .order_by(Season.free_agency_opening.desc())
        .limit(1)
        .scalar_subquery()
    )
    last_season_length = (
        Season.query.filter(Season.id == last_season)
        .with_entities(
            func.julianday(Season.regular_season_end)
            - func.julianday(Season.regular_season_start),
        )
        .first()[0]
    )
    last_season_roster_days = (
        DailyCap.query.filter(DailyCap.season_id == last_season)
        .filter(
            DailyCap.contract_id.in_(
                select(
                    Contract.query.filter(Contract.person_id == person_id)
                    .with_entities(Contract.id)
                    .subquery()
                )
            )
        )
        .filter(DailyCap.non_roster == False)
        .count()
    )

    if last_season_roster_days > 0:
        return last_season_roster_days / last_season_length

    last_season_games = 0
    if Person.query.get(person_id).position == "G":
        query = (
            GoalieSeason.query.filter(GoalieSeason.person_id == person_id)
            .filter(GoalieSeason.season_id == last_season)
            .group_by(GoalieSeason.person_id)
            .with_entities(
                func.sum(GoalieSeason.games_played),
            )
            .first()
        )
        if query:
            last_season_games = query[0]
    else:
        query = (
            SkaterSeason.query.filter(SkaterSeason.person_id == person_id)
            .filter(SkaterSeason.season_id == last_season)
            .group_by(SkaterSeason.person_id)
            .with_entities(
                func.sum(SkaterSeason.games_played),
            )
            .first()
        )
        if query:
            last_season_games = query[0]
    return last_season_games / 82


def get_total_retention(person_id):
    retention_asset = aliased(Asset)
    contract_asset = aliased(Asset)
    raw_ret = (
        RetainedSalary.query.join(Contract, Contract.id == RetainedSalary.contract_id)
        .join(retention_asset, retention_asset.id == RetainedSalary.id)
        .join(contract_asset, contract_asset.id == RetainedSalary.contract_id)
        .filter(Contract.person_id == person_id)
        .filter(retention_asset.active == True)
        .filter(contract_asset.active == True)
        .with_entities(
            func.sum(RetainedSalary.retention),
        )
        .first()
    )
    return raw_ret[0] or 0


def get_buyout_years(contract_id, retention=None):
    raw_years = query_buyout_years(contract_id)
    if not retention:
        retention = 100 - get_buyout_retention(contract_id)
    data = [
        {
            "cap_hit": retention * year[0] // 100,
            "nhl_salary": retention * year[1] // 100,
            "signing_bonus": retention * year[2] // 100,
            "season": year[3],
        }
        for year in raw_years
    ]
    return data


def get_buyout_retention(contract_id):
    raw_ret = (
        RetainedSalary.query.filter(RetainedSalary.contract_id == contract_id)
        .with_entities(
            func.sum(RetainedSalary.retention),
        )
        .first()
    )
    return raw_ret[0] or 0


def get_retained_years(contract_id, retention):
    """
    Per CBA, retained salary reduces AAV, NHL and minors salary, performance bonuses, signing bonus
    """
    raw_years = query_retained_years(contract_id)
    data = [
        {
            "cap_hit": int(year[0] * retention / 100),
            "nhl_salary": int(year[1] * retention / 100),
            "signing_bonus": int(year[2] * retention / 100),
            "season": year[3],
        }
        for year in raw_years
    ]
    return data


def get_non_roster_years(person_id):
    retention = 100 - get_total_retention(person_id)
    raw_years = query_non_roster_years(person_id)
    data = [
        {
            "cap_hit": max([int((100 - (year[5] or 0)) * year[0] / 100) - year[4], 0]),
            "nhl_salary": int((100 - (year[5] or 0)) * year[1] / 100),
            "signing_bonus": int((100 - (year[5] or 0)) * year[2] / 100),
            "season": year[3],
            "amount_buried": min([year[4], year[0]]),
        }
        for year in raw_years
    ]
    return data


def get_penalty_years(penalty_id):
    raw_years = query_penalty_years(penalty_id)
    data = [
        {
            "cap_hit": year[0],
            "season": year[1],
        }
        for year in raw_years
    ]
    return data


def query_teams():
    return (
        Team.query.with_entities(Team.id, Team.name)
        .filter(Team.active == True)
        .order_by(Team.name.asc())
        .all()
    )


def query_draft_picks(team_id):
    draft_season_ids = (
        Season.query.filter(
            Season.draft_date
            >= Season.query.filter(Season.id == current_app.config["DRAFT_SEASON"])
            .with_entities(Season.draft_date)
            .order_by(Season.draft_date.asc())
            .limit(1)
            .scalar_subquery()
        )
        .order_by(Season.draft_date.asc())
        .with_entities(Season.id)
        .limit(4)
        .subquery()
    )
    return (
        DraftPick.query.join(Asset, DraftPick.id == Asset.id)
        .join(Team, Team.id == Asset.originating_team)
        .join(Season, Season.id == DraftPick.season_id)
        .filter(Asset.active == True)
        .filter(DraftPick.season_id.in_(select(draft_season_ids)))
        .filter(or_(Asset.current_team == team_id, Asset.originating_team == team_id))
        .with_entities(
            Team.abbreviation,
            Asset.current_team,
            Asset.originating_team,
            DraftPick.round,
            DraftPick.position,
            Season.name,
            Season.id,
            Asset.id,
            Team.logo,
        )
        .order_by(
            Season.draft_date.asc(),
            Season.id.asc(),
            DraftPick.round.asc(),
            DraftPick.position.asc(),
            Team.abbreviation.asc(),
        )
        .all()
    )


def query_pick_conditions(pick_id):
    return (
        TransactionAsset.query.join(
            Transaction, Transaction.id == TransactionAsset.transaction_id
        )
        .filter(TransactionAsset.asset_id == pick_id)
        .filter(TransactionAsset.condition != None)
        .order_by(Transaction.date.asc())
        .with_entities(TransactionAsset.condition)
        .all()
    )


def query_roster(team_id):
    return (
        Person.query.join(Contract, Contract.person_id == Person.id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .join(Asset, Contract.id == Asset.id)
        .filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .filter(Contract.non_roster == False)
        .with_entities(
            Person.name,
            Person.position,
            Person.birthdate,
            Person.id,
            ContractYear.nmc,
            ContractYear.ntc,
            ContractYear.clause_limits,
            Person.current_status,
            Asset.type,
            Person.number,
            Person.waivers_exempt,
        )
        .order_by(
            case(
                (Person.current_status.like("%LTIR%"), 2),
                (Person.current_status.like("%SOIR%"), 3),
                else_=1,
            ),
            case(
                (Person.position == "D", 2),
                (Person.position == "LD", 2),
                (Person.position == "RD", 2),
                (Person.position == "G", 3),
                else_=1,
            ),
            ContractYear.cap_hit.desc(),
            Contract.total_value.desc(),
            Contract.years.desc(),
        )
        .all()
    )


def query_professional_tryouts(team_id):
    return (
        ProfessionalTryout.query.join(Person, Person.id == ProfessionalTryout.person_id)
        .join(Asset, Asset.id == ProfessionalTryout.id)
        .filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .with_entities(
            Person.name,
            Person.position,
            Person.birthdate,
            Person.id,
            False,
            False,
            None,
            Person.current_status,
            Asset.type,
            Person.number,
            Person.waivers_exempt,
        )
        .order_by(Person.name.asc())
        .all()
    )


def query_signing_rights(team_id, non_roster=False):
    last_season = (
        Season.query.filter(
            Season.free_agency_opening
            < (
                Season.query.filter(Season.id == current_app.config["CURRENT_SEASON"])
                .with_entities(Season.free_agency_opening)
                .scalar_subquery()
            )
        )
        .with_entities(
            Season.id,
        )
        .order_by(
            Season.free_agency_opening.desc(),
        )
        .limit(1)
        .scalar_subquery()
    )
    roster_subset = (
        Contract.query.join(ContractYear, ContractYear.contract_id == Contract.id)
        .join(Asset, Asset.id == Contract.id)
        .filter(Contract.non_roster == non_roster)
        .filter(ContractYear.season_id == last_season)
        .filter(Asset.current_team == team_id)
        .with_entities(
            Contract.person_id,
        )
        .subquery()
    )
    return (
        Person.query.join(SigningRights, SigningRights.person_id == Person.id)
        .join(Asset, Asset.id == SigningRights.id)
        .filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .filter(Person.current_status.like("%RFA%"))
        .filter(Person.id.in_(select(roster_subset)))
        .with_entities(
            Person.name,
            Person.position,
            Person.birthdate,
            Person.id,
            False,
            False,
            None,
            Person.current_status,
            Asset.type,
            Person.number,
            Person.waivers_exempt,
        )
        .order_by(
            case((Person.position == "D", 2), (Person.position == "G", 3), else_=1),
            Person.name,
        )
        .all()
    )


def query_pending_ufa(team_id, non_roster=False):
    free_agency = (
        Season.query.filter(Season.id == current_app.config["CURRENT_SEASON"])
        .first()
        .free_agency_opening
    )
    if free_agency is None:
        current_app.logger.debug("No free agency date")
        return []
    if date.today() > free_agency:
        current_app.logger.debug("Free agency in the past")
        return []
    season_id = (
        Season.query.filter(Season.free_agency_opening < free_agency)
        .order_by(Season.free_agency_opening.desc())
        .with_entities(Season.id)
        .limit(1)
        .scalar_subquery()
    )
    return (
        Person.query.join(Contract, Contract.person_id == Person.id)
        .join(Asset, Asset.id == Contract.id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .filter(Asset.active == False)
        .filter(Asset.current_team == team_id)
        .filter(ContractYear.season_id == season_id)
        .filter(ContractYear.bought_out == False)
        .filter(ContractYear.terminated == False)
        .filter(Contract.non_roster == non_roster)
        .filter(Person.current_status.like("%UFA%"))
        .with_entities(
            Person.name,
            Person.position,
            Person.birthdate,
            Person.id,
            False,
            False,
            ContractYear.clause_limits,
            Person.current_status,
            column("UFA"),
            Person.number,
            Person.waivers_exempt,
        )
        .order_by(
            case((Person.position == "D", 2), (Person.position == "G", 3), else_=1),
            Contract.total_value,
            Person.name,
        )
        .all()
    )


def query_contract(person_id):
    retention_sum = (
        db.session.query(
            RetainedSalary.contract_id.label("contract_id"),
            func.sum(RetainedSalary.retention).label("total_retention"),
        )
        .group_by(RetainedSalary.contract_id)
        .subquery()
    )
    return (
        ContractYear.query.join(Contract, ContractYear.contract_id == Contract.id)
        .join(Season, Season.id == ContractYear.season_id)
        .join(
            retention_sum,
            ContractYear.contract_id == retention_sum.c.contract_id,
            isouter=True,
        )
        .filter(Contract.person_id == person_id)
        .filter(ContractYear.season_id >= current_app.config["CURRENT_SEASON"])
        .filter(ContractYear.terminated == False)
        .filter(ContractYear.bought_out == False)
        .order_by(Season.free_agency_opening, Season.name.asc())
        .with_entities(
            ContractYear.cap_hit,
            ContractYear.aav,
            ContractYear.nhl_salary,
            ContractYear.performance_bonuses,
            ContractYear.signing_bonus,
            Season.name,
            retention_sum.c.total_retention,
        )
        .all()
    )


def query_buyouts(team_id):
    buyout_year_count = (
        db.session.query(
            BuyoutYear.contract_id.label("contract_id"),
            db.func.max(Season.free_agency_opening).label("last_season_free_agency"),
            db.func.max(BuyoutYear.season_id).label("last_season"),
            db.func.count().label("buyout_years"),
        )
        .join(Season, Season.id == BuyoutYear.season_id)
        .group_by(BuyoutYear.contract_id)
        .subquery()
    )
    current_free_agency = (
        Season.query.filter(Season.id == current_app.config["CURRENT_SEASON"])
        .with_entities(Season.free_agency_opening)
        .limit(1)
        .scalar_subquery()
    )
    buyouts = (
        Person.query.join(Contract, Contract.person_id == Person.id)
        .join(Asset, Contract.id == Asset.id)
        .join(buyout_year_count, buyout_year_count.c.contract_id == Contract.id)
        .filter(Asset.active == False)
        .filter(Asset.current_team == team_id)
        .filter(buyout_year_count.c.buyout_years > 0)
        .filter(buyout_year_count.c.last_season_free_agency >= current_free_agency)
        .with_entities(
            Person.name,
            Person.position,
            Person.birthdate,
            Person.id,
            Contract.id,
            buyout_year_count.c.buyout_years,
        )
        .order_by(buyout_year_count.c.buyout_years.desc())
        .all()
    )
    return buyouts


def query_retained_buyouts(team_id):
    buyout_year_count = (
        db.session.query(
            BuyoutYear.contract_id.label("contract_id"),
            db.func.max(BuyoutYear.season_id).label("last_season"),
            db.func.count().label("buyout_years"),
        )
        .group_by(BuyoutYear.contract_id)
        .subquery()
    )
    retained_buyouts = (
        Person.query.join(Contract, Contract.person_id == Person.id)
        .join(RetainedSalary, RetainedSalary.contract_id == Contract.id)
        .join(Asset, RetainedSalary.id == Asset.id)
        .join(buyout_year_count, buyout_year_count.c.contract_id == Contract.id)
        .filter(Asset.active == False)
        .filter(Asset.current_team == team_id)
        .filter(buyout_year_count.c.buyout_years > 0)
        .filter(buyout_year_count.c.last_season >= current_app.config["CURRENT_SEASON"])
        .with_entities(
            Person.name,
            Person.position,
            Person.birthdate,
            Person.id,
            Contract.id,
            buyout_year_count.c.buyout_years,
            RetainedSalary.retention,
        )
        .order_by(buyout_year_count.c.buyout_years.desc())
        .all()
    )
    return retained_buyouts


def query_buyout_years(contract_id):
    return (
        BuyoutYear.query.join(Season, Season.id == BuyoutYear.season_id)
        .filter(BuyoutYear.season_id >= current_app.config["CURRENT_SEASON"])
        .filter(BuyoutYear.contract_id == contract_id)
        .with_entities(
            BuyoutYear.cap_hit,
            BuyoutYear.nhl_salary,
            BuyoutYear.signing_bonus,
            Season.name,
        )
        .order_by(Season.free_agency_opening.asc())
        .all()
    )


def query_retained(team_id):
    return (
        RetainedSalary.query.join(Contract, Contract.id == RetainedSalary.contract_id)
        .join(Asset, Asset.id == Contract.id)
        .join(Person, Person.id == Contract.person_id)
        .filter(Asset.active == True)
        .filter(RetainedSalary.retained_by == team_id)
        .order_by(RetainedSalary.retention.desc())
        .with_entities(
            RetainedSalary.retention,
            Person.name,
            Person.position,
            Person.birthdate,
            Person.id,
            Contract.id,
        )
        .all()
    )


def query_retained_years(contract_id):
    return (
        ContractYear.query.join(Season, Season.id == ContractYear.season_id)
        .filter(ContractYear.season_id >= current_app.config["CURRENT_SEASON"])
        .filter(ContractYear.contract_id == contract_id)
        .with_entities(
            ContractYear.cap_hit,
            ContractYear.nhl_salary,
            ContractYear.signing_bonus,
            Season.name,
        )
        .order_by(Season.free_agency_opening.asc())
        .all()
    )


def query_non_roster(team_id):
    return (
        Person.query.join(Contract, Contract.person_id == Person.id)
        .join(ContractYear, Contract.id == ContractYear.contract_id)
        .join(Asset, Asset.id == Contract.id)
        .filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .filter(Contract.non_roster == True)
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .order_by(
            case((Person.position == "D", 2), (Person.position == "G", 3), else_=1),
            case(
                (Contract.in_juniors == True, 3), (Contract.on_loan == True, 2), else_=1
            ),
            ContractYear.cap_hit.desc(),
        )
        .with_entities(
            Person.name,
            Person.position,
            Person.birthdate,
            Person.id,
            Person.waivers_exempt,
            Contract.on_loan,
            Contract.in_juniors,
        )
        .all()
    )


def query_non_roster_years(person_id):
    retention_sum = (
        db.session.query(
            RetainedSalary.contract_id.label("contract_id"),
            func.sum(RetainedSalary.retention).label("total_retention"),
        )
        .group_by(RetainedSalary.contract_id)
        .subquery()
    )
    return (
        ContractYear.query.join(Season, Season.id == ContractYear.season_id)
        .join(
            retention_sum,
            retention_sum.c.contract_id == ContractYear.contract_id,
            isouter=True,
        )
        .join(Contract, Contract.id == ContractYear.contract_id)
        .filter(ContractYear.season_id >= current_app.config["CURRENT_SEASON"])
        .filter(Contract.person_id == person_id)
        .filter(ContractYear.terminated == False)
        .filter(ContractYear.bought_out == False)
        .with_entities(
            ContractYear.cap_hit,
            ContractYear.nhl_salary,
            ContractYear.signing_bonus,
            Season.name,
            Season.max_buriable_hit,
            retention_sum.c.total_retention,
        )
        .order_by(Season.free_agency_opening)
        .all()
    )


def query_penalties(team_id):
    return (
        Penalty.query.join(Asset, Asset.id == Penalty.id)
        .join(Contract, Contract.id == Penalty.contract_id)
        .join(Person, Person.id == Contract.person_id)
        .join(PenaltyYear, Penalty.id == PenaltyYear.penalty_id)
        .filter(Asset.active == True)
        .filter(Asset.current_team == team_id)
        .filter(PenaltyYear.season_id == current_app.config["CURRENT_SEASON"])
        .order_by(
            PenaltyYear.cap_hit.desc(),
            Person.position,
        )
        .with_entities(
            Person.name,
            Person.position,
            Person.birthdate,
            Person.id,
            Penalty.id,
            Penalty.type,
        )
        .all()
    )


def query_penalty_years(penalty_id):
    return (
        PenaltyYear.query.join(Season, Season.id == PenaltyYear.season_id)
        .filter(PenaltyYear.season_id >= current_app.config["CURRENT_SEASON"])
        .filter(PenaltyYear.penalty_id == penalty_id)
        .with_entities(
            PenaltyYear.cap_hit,
            Season.name,
        )
        .order_by(Season.free_agency_opening)
        .all()
    )


def get_season_names():
    return (
        Season.query.filter(
            Season.free_agency_opening
            >= Season.query.get(
                current_app.config["CURRENT_SEASON"]
            ).free_agency_opening
        )
        .with_entities(Season.name)
        .order_by(Season.free_agency_opening)
        .limit(current_app.config["DISPLAY_X_SEASONS"])
        .all()
    )
