from markupsafe import escape
from flask import current_app

from .. import db
from ..models import Note


def get_notes(table=None, record_id=None):
    query = Note.query
    if table:
        query = query.filter(Note.record_table == table)
    if table and record_id:
        query = query.filter(Note.record_id == record_id)
    raw_notes = query.all()
    return [
        {
            "note_id": note.id,
            "record_table": note.record_table,
            "record_id": note.record_id,
            "note": escape(note.note) if not note.safe else note.note,
        }
        for note in raw_notes
    ]
