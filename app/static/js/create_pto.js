document.getElementById("show-inactive").onclick = selectFromInactive;

async function selectFromInactive() {
    let selector = document.getElementById("person");
    let response = await fetch("/admin/api/inactive");
    if (!response.ok) {
        console.log("Error querying inactive players for PTO");
        return;
    }
    let data = await response.json();
    if (data.length == 0) {
        alert("No inactive players for PTOs");
        return;
    }
    selector.querySelectorAll("option").forEach(el => el.remove());
    data.forEach(row => {
        let option = document.createElement("option");
        option.innerText = row.pop();
        option.value = row.pop();
        selector.appendChild(option);
    });
    document.getElementById("show-inactive").parentElement.remove();
}
