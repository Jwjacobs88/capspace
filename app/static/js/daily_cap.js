document.querySelector("#select").onclick = redirect;

function redirect() {
    let season = document.querySelector("#season").value;
    if (!season) {
        console.log("No season, no redirect");
        return;
    }

    let url = new URL(window.location.href);

    let noSeason = (url.pathname.split("/").length == 4);
    console.log(`url.pathname=${url.pathname} -- noSeason:${noSeason}`);
    if (noSeason) {
        window.location.href = `${url.href}/${season}`;
        return;
    }

    let stub = url.pathname.slice(0, url.pathname.lastIndexOf("/"));
    window.location.href = `${stub}/${season}`;
    return;
}
