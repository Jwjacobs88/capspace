const STATSTABLE = document.querySelector(".table_stats");
setLeagues();
displayAllStats();
document.querySelector("#stats-league").onchange = filterRows;

function getLeagueIndex() {
    let columns = STATSTABLE.querySelector("tr").querySelectorAll("td");
    let idx = 0;
    for (; idx < columns.length; idx++) {
        if (columns[idx].innerText == "League") {
            return idx;
        }
    }
}

function setLeagues() {
    let idx = getLeagueIndex();
    let rows = STATSTABLE.querySelectorAll("tr");
    let leagues = [];
    rows.forEach(row => leagues.push(row.querySelectorAll("td")[idx].innerText));
    leagues = new Set(leagues);
    let selector = document.querySelector("#stats-league");
    for (let league of leagues) {
        if (league == "League") {
            continue;
        } else if (league == "") {
            continue;
        }
        let option = document.createElement("option");
        option.value = league;
        option.innerText = league;
        selector.appendChild(option);
    }
}

function displayAllStats() {
    let idx = 1;
    let rows = STATSTABLE.querySelectorAll("tr");
    for (; idx < rows.length; idx++) {
        rows[idx].style.display = "table-row";
    }
    recolorRows();
}

function filterRows() {
    let selector = document.querySelector("#stats-league");
    if (!selector.value) {
        //Show all stats rows
        displayAllStats();

        //Hide redundant season names
        let idx = 1;
        let previous = "";
        let rows = STATSTABLE.querySelectorAll("tr");
        for (; idx < rows.length; idx++) {
            let season = rows[idx].querySelector("td");
            if (season.innerText == previous) {
                season.innerHTML = "&nbsp;"
            } else {
                previous = season.innerText;
            }
        }
    } else {
        //Set season names
        let idx = 1;
        let previous = "";
        let rows = STATSTABLE.querySelectorAll("tr");
        for (; idx < rows.length; idx++) {
            let season = rows[idx].querySelector("td");
            if (season.innerHTML == "&nbsp;") {
                season.innerText = previous;
            } else {
                previous = season.innerText;
            }
        }

        //Hide irrelevant rows
        let leagueIdx = getLeagueIndex();
        for (let row of rows) {
            let league = row.querySelectorAll("td")[leagueIdx].innerText;
            if (selector.value == league || league == "League") {
                row.style.display = "table-row";
            } else {
                row.style.display = "none";
            }
        }

        //Hide duplicate season names
        let visible = STATSTABLE.querySelectorAll("tr[style*='display: table-row']");
        idx = 1;
        previous = "";
        for (; idx < visible.length; idx++) {
            let season = visible[idx].querySelector("td");
            if (season.innerText == previous) {
                season.innerHTML = "&nbsp;";
            } else {
                previous = season.innerText;
            }
        }
    }

    //Re-set row-coloring classes based on visible
    recolorRows();
    return;
}

function recolorRows() {
    let displayed = STATSTABLE.querySelectorAll("tr[style*='display: table-row']");
    for (let idx = 0; idx < displayed.length; idx++) {
        displayed[idx].classList.remove("tr_stats_odd");
        displayed[idx].classList.remove("tr_stats_even");
        if (idx % 2 == 0) {
            displayed[idx].classList.add("tr_stats_odd");
        } else {
            displayed[idx].classList.add("tr_stats_even");
        }
    }
}
