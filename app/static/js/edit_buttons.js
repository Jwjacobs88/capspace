document.getElementById("edit").href = editPerson();
document.querySelectorAll("[id^='notes-']").forEach(element => element.onclick = editNotes);

fetch(`/admin/update-person`)
    .then(response => {
        if (response.redirected) {
            return;
        }
        document.querySelectorAll(".edit").forEach(element => element.style.display = "block");
    });

function editPerson() {
    return window.location.href.replace("person", "admin/update-person");
}

function editNotes(event) {
    let path = `admin/notes/contract/${event.target.id.slice(6)}`;
    window.location.href = `${window.location.origin}/${path}`;
}
