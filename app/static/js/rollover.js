reshadeRows()
document
    .querySelectorAll(".set-p-bonus")
    .forEach(elem => elem.onclick = setPerformanceBonuses);
document
    .querySelectorAll("[id^='g6']")
    .forEach(elem => elem.onclick = setGroupSix);
document.getElementById("update-stats").onclick = updateStats;

function reshadeRows() {
    document.querySelectorAll("table")
        .forEach(table => {
            table.querySelectorAll("tbody tr")
            .forEach((row, idx) => {
                if (idx % 2 == 0) {
                    row.classList.add("shaded");
                } else {
                    row.classList.remove("shaded");
                }
            });
        });
}

async function updateStats() {
    let url = "/admin/api/update-stats";
    if (confirm("Update stats?. This should take between 15 and 25 minutes.")) {
        let response = await fetch(url, {"method": "POST"});
    }
}

async function setPerformanceBonuses(event) {
    let contractID = event.target.previousElementSibling.id.split("-").pop();
    let performanceBonuses = event.target.previousElementSibling.value;

    if (isNaN(Number(performanceBonuses))) {
        alert("Performance bonus value must be a number");
        event.target.previousElementSibling.value = 0;
        return;
    }

    console.log(`Performance Bonuses: contract=${contractID} bonus=${performanceBonuses}`);
    let url = `/admin/api/set-p-bonuses/${contractID}?earned=${performanceBonuses}`;
    let response = await fetch(url, {"method": "POST"});
    let data = await response.json();
    if (data.error) {
        alert(`Invalid update: ${data.error}`);
        return;
    }
    event.target.parentElement.parentElement.remove();
    reshadeRows();
}

async function setGroupSix(event) {
    let contractID = event.target.id.split("-").pop();
    if (isNaN(Number(contractID))) {
        alert("Contract ID must be a number");
        return;
    }
    let name = event.target.parentElement.parentElement.querySelector("a").innerText;
    if (!confirm(`Set ${name} as a group six free agent?`)) {
        return;
    }
    console.log(`G6 UFA: contract=${contractID}`);
    let url = `/admin/api/set-g6/${contractID}`;
    let response = await fetch(url, {"method": "POST"});
    let data = response.json();
    if (data.error) {
        alert(`Invalid update: ${data.error}`);
        return;
    }
    event.target.parentElement.parentElement.remove();
    reshadeRows();
}
