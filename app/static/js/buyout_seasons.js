let playerSelector = document.getElementById("players");
playerSelector.onchange=(event => updateSeasons(event.srcElement.value));

async function updateSeasons(personId) {
    let buyoutSeasons = await getBuyoutSeasons(personId);
    updateSeasonSelector(buyoutSeasons);
}

async function getBuyoutSeasons(personId) {
    let response = await fetch(`/api/buyout_seasons/${personId}`);
    let seasons = await response.json();
    return seasons;
}

function updateSeasonSelector(seasons) {
    let options = [];
    for (let season of seasons) {
        let option = document.createElement("option");
        option.value = season;
        option.innerText = season;
        options.push(option);
    }
    options.pop();
    document.getElementById("buyout_after").replaceChildren(...options);
}
