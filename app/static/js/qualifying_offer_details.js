document.querySelector("#qualify").onclick = getQualifyingDetails;

async function getQualifyingDetails() {
    let personId = document.querySelector("#players").value;
    let response = await fetch(`/api/qualifying_offer/${personId}`);
    let details = await response.text();
    document.querySelector("#results").innerHTML = details;
    return false;
}
