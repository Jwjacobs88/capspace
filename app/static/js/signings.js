document.getElementById("load_contracts").onclick = loadSignings;
document.getElementById("team-filter").value = "";
document.getElementById("team-filter").onchange = filterTeams;
document.getElementById("filter-dates").onclick = filterDates;
setFilterDates();
reshadeRows();

async function loadSignings() {
    let body = document.getElementById("signings").querySelector("tbody");
    let offset = body.rows.length;
    let loadURL = new URL(window.location.href);
    loadURL.pathname = `/api/load_signings/${offset}`
    let response = await fetch(loadURL);
    let signings = await response.json();
    if (signings.length == 0) {
        document.getElementById("load_contracts").style.display = "none";
    }

    signings.forEach(signing => body.appendChild(createRow(signing)));
    filterTeams();
}

function filterTeams() {
    let body = document.getElementById("signings").querySelector("tbody");
    let teamID = document.getElementById("team-filter").value;
    if (!teamID) {
        body.querySelectorAll("tr").forEach(row => row.style.display = "");
    } else {
        body.querySelectorAll("tr").forEach(row => row.style.display = "none");
        body.querySelectorAll(`.team_${teamID}`).forEach(row => row.style.display = "");
    }
    reshadeRows();
}

function filterDates() {
    let url = new URL(window.location.href);
    url.searchParams.delete("from");
    url.searchParams.delete("to");
    let toDate = document.getElementById("to").value;
    let fromDate = document.getElementById("from").value;
    if (toDate != "") {
        url.searchParams.append("to", toDate);
    }
    if (fromDate != "") {
        url.searchParams.append("from", fromDate);
    }
    window.location.href = url.href;
}

function setFilterDates() {
    let url = new URL(window.location.href);
    let toDate = url.searchParams.get("to");
    let fromDate = url.searchParams.get("from");
    document.getElementById("to").value = toDate;
    document.getElementById("from").value = fromDate;
}

function createRow(data) {
    let row = document.createElement("tr");
    row.classList.add(`team_${data.team_id}`);
    let name = document.createElement("td");
    name.innerHTML = "&nbsp;"
    let nameLink = document.createElement("a");
    nameLink.href = `/person/${data.person_id}`;
    nameLink.innerText = data.person_name;
    name.appendChild(nameLink);
    row.appendChild(name);

    let age = document.createElement("td");
    age.innerText = data.age;
    row.appendChild(age);

    let position = document.createElement("td");
    position.innerText = data.position;
    row.appendChild(position);

    let team = document.createElement("td");
    let teamLink = document.createElement("a");
    teamLink.href = `/team/${data.team.toLowerCase()}`;
    teamLink.innerHTML = `${data.team_logo} ${data.team}`;
    team.appendChild(teamLink);
    row.appendChild(team);

    let contractType = document.createElement("td");
    if (data.entry_level) {
        contractType.innerText = "Entry Level";
    } else if (data.thirty_five_plus) {
        contractType.innerText = "Thirty-Five+";
    } else {
        contractType.innerText = "Standard";
    }
    row.appendChild(contractType);

    let years = document.createElement("td");
    years.innerText = data.years;
    row.appendChild(years);

    let totalValue = document.createElement("td");
    totalValue.classList.add("align-right");
    totalValue.innerText = new Intl.NumberFormat("en-US", {
        "currency": "USD",
        "style": "currency",
        "maximumFractionDigits": 0,
        "minimumFractionDigits": 0,
    }).format(data.value);
    row.appendChild(totalValue);

    let signedOn = document.createElement("td");
    signedOn.innerText = data.signing_date;
    row.appendChild(signedOn);

    return row;
}

function reshadeRows() {
    let rows = document.querySelectorAll("#signings tr[class^='team']");
    rows.forEach(row => row.classList.remove("shaded"));
    let shaded = false;
    rows.forEach(row => {
        if (row.style.display == "none") {
            return;
        }
        if (shaded) {
            row.classList.add("shaded");
        }
        shaded = !shaded;
    });
}
