document.getElementById("team").onchange = clearData;
document.getElementById("season").onchange = clearData;
document.getElementById("load").onclick = renderData;
orderSeasons();

function clearData() {
    document.getElementById("team-season").innerHTML = "";
}

async function renderData() {
    let team = document.getElementById("team").value;
    let season = document.getElementById("season").value;
    let url = `/admin/api/team-season-data/${team}/${season}`;
    let response = await fetch(url);
    let details = await response.text();
    document.getElementById("team-season").innerHTML = details;
    document.getElementById("update-team-season").onclick = updateRecord;
    document.querySelector("#team-season #logo").onchange = updatePreview;
    await updatePreview({"target": document.querySelector("#team-season #logo")});
}

async function updateRecord() {
    let formData = [];
    document
        .querySelectorAll(".page form")
        .forEach(form => formData.push(new FormData(form)));
    let parameters = formData
        .map(data => new URLSearchParams(data))
        .map(params => params.toString())
        .join("&");
    let url = `/admin/api/update-team-season?${parameters}`;
    let response = await fetch(url, {"method": "POST"});
    let message = await response.json();
    let updateMessage = document.createElement("p");
    updateMessage.innerText = message.message;
    if (message.error) {
        updateMessage.classList.add("error");
    }

    let container = document.querySelector("#team-season");
    container.insertBefore(updateMessage, container.children[1]);
}

function remove() {
    document
        .querySelectorAll(".remove")
        .forEach(elem => elem.remove());
}

function orderSeasons() {
    let seasons = document.getElementById("season");
    let year = (new Date()).getFullYear().toString();
    let options = [...seasons.children];
    options
        .sort((a, b) => {
            let textA = a.innerText;
            let textB = b.innerText;
            let stubA = textA.slice(0, 4);
            let stubB = textB.slice(0, 4);
            //Seasons in the past go after current seasons
            if (stubA < year && stubB >= year) {
                return 1;
            } else if (stubB < year && stubA >= year) {
                return 1;
            } else if (textA == textB) {
                return 0;
            } else {
                //Coercing a boolean to an int and mathing on it for lexicographic sorting
                return (textA < textB)*-2 + 1;
            }
        }).forEach(option => seasons.appendChild(option));
}

async function updatePreview(event) {
    console.log(event);
    let preview = document.getElementById("logo-preview");
    preview.innerHTML = "";
    let logoName = event.target.value;
    let title = document.createElement("h2");
    title.innerText = "Preview Logo";
    let logo = document.createElement("img");
    logo.classList.add("logo");
    logo.src = `/static/${logoName}`;
    preview.appendChild(title);
    preview.appendChild(logo);
}
