import sys
from datetime import date, datetime
from flask import current_app
from flask_wtf import FlaskForm
from wtforms import SelectMultipleField, SubmitField, DateField
from wtforms.validators import DataRequired
from sqlalchemy import select, insert, func, case, and_, Integer, Float
from .. import db, scheduler
from ..models import (
    Asset,
    Season,
    Contract,
    BuyoutYear,
    ContractYear,
    PenaltyYear,
    RetainedSalary,
    TeamSeason,
    Person,
    DailyCap,
    Team,
    SkaterSeason,
    GoalieSeason,
)

# TODO: Trade proposal contract count check with this?
# TODO: Trade proposal cap check
# TODO: Add trade tests for cap
# TODO: Reduce cap and salary from contract lines on receiving team page - part of trade.apply_to_database
# TODO: Test teams for retained salary contract limit when proposing a trade
# TODO: Test teams for previously retained contracts when proposing a trade
# TODO: Add all this to team and trade tests


@scheduler.task("interval", id="get_jobs", minutes=30)
def do_it():
    now = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
    with scheduler.app.app_context():
        if current_app.config["DISABLE_SCHEDULED"]:
            print(f"Scheduled jobs disabled: {now}", file=sys.stderr)
            return
    print(f"Monitoring Scheduled Jobs: {now}", file=sys.stderr)
    print(f"    {scheduler.get_job('daily_cap')}", file=sys.stderr)
    print(f"    {scheduler.get_job('monthly_active_user')}", file=sys.stderr)


@scheduler.task("cron", id="daily_cap", hour="17", timezone="America/New_York")
def run_daily_update():
    with scheduler.app.app_context():
        if current_app.config["DISABLE_SCHEDULED"]:
            return
        # Default value for date.today() doesn't change day to day
        # It runs once on runtime and then repeats
        daily_cap_updates(date.today())


def get_team_season(team_id, season_id):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    raw_data = query_team_season(team_id, season_id)
    if not raw_data:
        return {}
    return {
        "cap_hit": raw_data.projected_cap_hit,  # Projected
        "contracts": raw_data.contract_count,
        "retained_contracts": raw_data.retained_contracts,
        "max_ltir_relief": raw_data.max_ltir_relief,
    }


def query_team_season(team_id, season_id):
    return (
        TeamSeason.query.filter(TeamSeason.season_id == season_id)
        .filter(TeamSeason.team_id == team_id)
        .first()
    )


def manually_update_team_season(record, fields):
    for key, value in fields.items():
        if key in ["team", "season", "team_id", "season_id"]:
            continue
        if not hasattr(record, key):
            continue
        setattr(record, key, value)
    db.session.add(record)
    db.session.commit()


def create_team_season(team_id, season_id):
    db.session.add(
        TeamSeason(
            team_id=team_id,
            season_id=season_id,
            contract_count=get_contract_count(team_id, season_id),
            retained_contracts=get_retained_count(team_id, season_id),
            cap_hit=get_cap_hit(team_id, season_id),
            projected_cap_hit=projected_cap_hit(team_id, season_id),
            max_ltir_relief=get_max_ltir_relief(team_id, season_id),
            ltir_contracts=current_ltir_contracts(team_id),
            logo=get_current_logo(team_id),
        )
    )
    db.session.flush()


def get_current_logo(team_id):
    return Team.query.get(team_id).logo


def update_all_teams_seasons(team_id):
    current_free_agency = (
        Season.query.filter(Season.id == current_app.config["CURRENT_SEASON"])
        .with_entities(Season.free_agency_opening)
        .first()
    )
    if not current_free_agency or not current_free_agency[0]:
        last_year = date.today().year - 1
        current_free_agency = date.today().replace(year=last_year)
    else:
        current_free_agency = current_free_agency[0]
    season_ids = (
        Season.query.filter(Season.free_agency_opening >= current_free_agency)
        .with_entities(Season.id)
        .all()
    )
    for season_id in season_ids:
        update_team_season(team_id, season_id[0])


def update_league_records(season_id):
    teams = Team.query.filter(Team.active == True).with_entities(Team.id).all()
    for team in teams:
        update_team_season(team[0], season_id)
    db.session.commit()


def update_team_season(team_id, season_id):
    count = (
        TeamSeason.query.filter(TeamSeason.team_id == team_id)
        .filter(TeamSeason.season_id == season_id)
        .update(
            {
                TeamSeason.contract_count: get_contract_count(team_id, season_id),
                TeamSeason.retained_contracts: get_retained_count(team_id, season_id),
                TeamSeason.cap_hit: get_cap_hit(team_id, season_id),
                TeamSeason.projected_cap_hit: projected_cap_hit(team_id, season_id),
                TeamSeason.max_ltir_relief: get_max_ltir_relief(team_id, season_id),
                TeamSeason.ltir_contracts: current_ltir_contracts(team_id),
            }
        )
    )
    if count == 0:
        create_team_season(team_id, season_id)


def current_ltir_contracts(team_id, day=date.today()):
    return (
        DailyCap.query.filter(DailyCap.date == day)
        .filter(DailyCap.team_id == team_id)
        .filter(DailyCap.ltir == True)
        .count()
    )


def get_contract_count(team_id, season_id):
    return (
        ContractYear.query.join(Asset, ContractYear.contract_id == Asset.id)
        .join(Contract, Contract.id == Asset.id)
        .filter(ContractYear.season_id == season_id)
        .filter(Contract.contract_limit_exempt == False)
        .filter(Asset.current_team == team_id)
        .filter(Asset.active == True)
        .count()
    )


def get_retained_count(team_id, season_id):
    playoffs_start = get_playoffs_start(season_id)
    return (
        RetainedSalary.query.join(
            ContractYear, ContractYear.contract_id == RetainedSalary.contract_id
        )
        .filter(ContractYear.season_id == season_id)
        .filter(RetainedSalary.retained_from < playoffs_start)
        .filter(RetainedSalary.retained_by == team_id)
        .count()
    )


def get_max_ltir_relief(team_id, season_id):
    # CBA 50.5(d)
    # 1. The total replacement player salary + bonuses for those replacing
    # LTIR-ed folks may not exceed the injured players' total salary
    # 2. Total replacement player salary + bonuses are added to the cap until the limit
    # Then the team may exceed the upper limit due to the addition of replacement players
    # 3. Upon returning from injury, including for conditioning loans, the team must
    # reduce their salary to below the limit before the LTIR-ed player may rejoin the team

    # cap_hit = get_offseason_projected_cap_hit(team_id, season_id)
    #   - Remove LTIR from this. Then use
    ltir_hit = (
        ContractYear.query.join(Asset, ContractYear.contract_id == Asset.id)
        .join(Contract, ContractYear.contract_id == Contract.id)
        .join(Person, Contract.person_id == Person.id)
        .filter(ContractYear.season_id == season_id)
        .filter(Asset.current_team == team_id)
        .filter(Person.current_status.like("%LTIR%"))
        .with_entities(func.sum(ContractYear.cap_hit))
        .first()[0]
        or 0
    )
    # Annual cap hit the day after the last day without LTIR minus LTIR
    # Current LTIR cap
    # Current salary cap
    # Current LTIR cap - salary cap + cap hit first LTIR day

    # if ltir_hit == 0:
    #    return 0
    # season = Season.query.get(season_id)
    # current_season = Season.query.get(current_app.config["CURRENT_SEASON"])
    # cap_ceiling = season.salary_cap or 0
    # max_ltir = cap_hit + ltir_hit - cap_ceiling
    # print(f"{cap_hit=} {cap_ceiling=} {ltir_hit=} {max_ltir=}")
    # if max_ltir < 0:
    #    max_ltir = 0
    # if current_season.draft_date < season.draft_date:
    #    max_ltir = 0
    # return max_ltir
    return ltir_hit


def get_cap_hit(team_id, season_id, on_date=date.today()):
    regular_season = (
        Season.query.filter(Season.regular_season_start <= on_date)
        .filter(Season.regular_season_end >= on_date)
        .filter(Season.id == season_id)
        .count()
    )
    if regular_season == 0:
        return get_offseason_projected_cap_hit(team_id, season_id)
    cap_hit = daily_cap_used(team_id, season_id, on_date)
    cap_hit += buyout_cap_hit(team_id, season_id)
    cap_hit += retained_buyout_cap_hit(team_id, season_id)
    cap_hit += penalty_cap_hit(team_id, season_id)
    return round(cap_hit, 2)


def projected_cap_hit(team_id, season_id, on_date=date.today()):
    regular_season = (
        Season.query.filter(Season.regular_season_start >= on_date)
        .filter(Season.regular_season_end <= on_date)
        .count()
    )
    if regular_season == 0:
        return get_offseason_projected_cap_hit(team_id, season_id)
    return get_projected_cap_hit(team_id, season_id)


def get_projected_cap_hit(team_id, season_id, on_date=date.today()):
    season = Season.query.get(season_id)
    daily_cap_today = (
        DailyCap.query.filter(DailyCap.date == on_date)
        .filter(DailyCap.team_id == team_id)
        .filter(DailyCap.season_id == season_id)
        .with_entities(func.sum(DailyCap.cap_hit))
        .first()
    )
    if not daily_cap_today:
        daily_cap_today = 0
    else:
        daily_cap_today = daily_cap_today[0] or 0
    days_remaining = (season.regular_season_end - date.today()).days - 1
    # daily_cap_used counts today's cap. so does the projected_remaining_cap_hit
    # subtract one to correct
    if days_remaining < 0:
        return 0
    projected_remaining_cap_hit = daily_cap_today * days_remaining
    cap_hit = daily_cap_used(team_id, season_id, on_date=on_date)
    cap_hit += buyout_cap_hit(team_id, season_id)
    cap_hit += retained_buyout_cap_hit(team_id, season_id)
    cap_hit += penalty_cap_hit(team_id, season_id)
    cap_hit += projected_remaining_cap_hit
    return round(cap_hit, 2)


def get_offseason_projected_cap_hit(team_id, season_id):
    cap_hit = get_contract_cap_hit(team_id, season_id)
    cap_hit += get_buyout_cap_hit(team_id, season_id)
    cap_hit += get_retained_buyout_cap_hit(team_id, season_id)
    cap_hit += get_buried_cap_penalty(team_id, season_id)
    cap_hit += get_retention_addition(team_id, season_id)
    cap_hit -= get_retention_decrease(team_id, season_id)
    cap_hit += get_penalties(team_id, season_id)
    return cap_hit


def get_contract_cap_hit(team_id, season_id):
    return (
        ContractYear.query.join(Asset, ContractYear.contract_id == Asset.id)
        .join(Contract, ContractYear.contract_id == Contract.id)
        .filter(ContractYear.season_id == season_id)
        .filter(Asset.current_team == team_id)
        .filter(Asset.active == True)
        .filter(Contract.non_roster == False)
        .with_entities(func.sum(ContractYear.cap_hit))
        .first()[0]
        or 0
    )


def get_buried_cap_penalty(team_id, season_id):
    season = Season.query.filter(Season.id == season_id).first()
    max_buriable = 0
    if season:
        max_buriable = season.max_buriable_hit
    data = (
        ContractYear.query.join(Asset, ContractYear.contract_id == Asset.id)
        .join(Contract, ContractYear.contract_id == Contract.id)
        .filter(ContractYear.season_id == season_id)
        .filter(Asset.current_team == team_id)
        .filter(Contract.non_roster == True)
        .filter(ContractYear.cap_hit > max_buriable)
        .with_entities(
            func.sum(ContractYear.cap_hit),
            func.count(),
        )
        .first()
    )
    if data[1] == 0:
        return 0
    penalty = data[0] - data[1] * max_buriable
    return penalty


def get_buyout_cap_hit(team_id, season_id):
    cap_hits = (
        BuyoutYear.query.join(
            RetainedSalary,
            RetainedSalary.contract_id == BuyoutYear.contract_id,
            isouter=True,
        )
        .join(Asset, BuyoutYear.contract_id == Asset.id)
        .filter(Asset.current_team == team_id)
        .filter(BuyoutYear.season_id == season_id)
        .with_entities(
            BuyoutYear.cap_hit,
            func.sum(RetainedSalary.retention),
        )
        .group_by(BuyoutYear.contract_id)
        .all()
    )
    return sum(
        map(
            lambda contract: int(contract[0] * (100 - (contract[1] or 0)) / 100),
            cap_hits,
        )
    )


def get_retained_buyout_cap_hit(team_id, season_id):
    cap_hits = (
        BuyoutYear.query.join(
            RetainedSalary, RetainedSalary.contract_id == BuyoutYear.contract_id
        )
        .filter(RetainedSalary.retained_by == team_id)
        .filter(BuyoutYear.season_id == season_id)
        .with_entities(
            BuyoutYear.cap_hit * RetainedSalary.retention / 100,
        )
        .all()
    )
    return sum([int(hit[0]) for hit in cap_hits])


def get_retention_addition(team_id, season_id):
    playoff_start = get_playoffs_start(season_id)
    retention = (
        RetainedSalary.query.join(
            ContractYear, ContractYear.contract_id == RetainedSalary.contract_id
        )
        .filter(RetainedSalary.retained_from < playoff_start)
        .filter(RetainedSalary.retained_by == team_id)
        .filter(ContractYear.season_id == season_id)
        .with_entities(
            RetainedSalary.retention,
            ContractYear.cap_hit,
        )
        .all()
    )
    adjustment = sum([int(ret[0] * ret[1] / 100) for ret in retention])
    return adjustment


def get_retention_decrease(team_id, season_id):
    playoff_start = get_playoffs_start(season_id)
    retention = (
        RetainedSalary.query.join(
            ContractYear, ContractYear.contract_id == RetainedSalary.contract_id
        )
        .join(Asset, Asset.id == ContractYear.contract_id)
        .filter(RetainedSalary.retained_from < playoff_start)
        .filter(Asset.current_team == team_id)
        .filter(ContractYear.season_id == season_id)
        .with_entities(
            RetainedSalary.retention,
            ContractYear.cap_hit,
        )
        .all()
    )
    adjustment = sum([int(ret[0] * ret[1] / 100) for ret in retention])
    return adjustment


def get_penalties(team_id, season_id):
    return (
        PenaltyYear.query.join(Asset, Asset.id == PenaltyYear.penalty_id)
        .filter(PenaltyYear.season_id == season_id)
        .filter(Asset.current_team == team_id)
        .with_entities(
            func.sum(PenaltyYear.cap_hit),
        )
        .first()[0]
        or 0
    )


def get_playoffs_start(season_id):
    return (
        db.session.query(Season.playoffs_start)
        .filter(Season.id == season_id)
        .scalar_subquery()
    )


"""
Daily Cap Hit records

Offseason Cap Hit (50.5(d)(1)(A))
    - Roster players' daily cap hit
    - Deferred salary and bonuses per 50.2(a) and 50.2(b)
    - Buyout cap hits per 50.9(i)
    - QO and Offer sheet tagging space until the earliest of
        - The RFA signs a contract
        - The QO expires per 10.2
    Two-way QOs tag cap space in accordance with the player's NHL time the prior year
    (e.g. 10 days in the NHL out of 200 would mean you tag 1/20th of their NHL salary) 
    - Two-way contracts w.r.t. to the player's NHL time the prior year
    (e.g. 10 NHL days of 200 means you count 1/20th the NHL salary)
    - Retained salary
    - Cap advantage recapture penalties
    - Dispute settlements that aren't included in a player's contract

Regular Season Cap Hit (50.5(d)(1)(B))
    - Roster players' daily cap hit
    - Deferred salary and bonuses per 50.2(a) and 50.2(b)
    - Buyout cap hits per 50.9(i)
    - Offer Sheet's NHL salary until the RFA signs an SPC
    - 35+ contract salary and bonuses over $100,000 when the player's not on the NHL roster where
        i. The contract's total compensation doesn't increase monotonically OR
        ii. The contract has a signing bonus in the a year other than the first
    - The NHL salary less the maximum buriable amount (NHL minimum + $375k) for non-roster one-way contracts while non-roster
    - The minors salary less the maximum buriable amount (NHL minimum + $375k) for non-roster two-way contracts while non-roster
    - Cap advantage recapture
    - Retained salary
    - Dispute settlements that aren't included in a player's contract
"""


def daily_cap_updates(date=date.today(), team_ids=[]):
    in_season = (
        Season.query.filter(Season.regular_season_start <= date)
        .filter(Season.regular_season_end >= date)
        .with_entities(Season.id)
        .all()
    )
    updates = []
    season_id = current_app.config["CURRENT_SEASON"]
    if len(in_season) > 0:
        current_app.logger.debug(f"Running daily cap roundup for {date}")
        season_id = in_season[0][0]
        updates = create_daily_cap_records(season_id, date, team_ids)
        current_app.logger.debug(f"  created={updates[1]} deleted={updates[0]}")
    update_league_records(season_id)
    return updates


def create_daily_cap_records(season_id, date=date.today(), team_ids=[]):
    deleted = 0
    record_query = DailyCap.query.filter(DailyCap.season_id == season_id).filter(
        DailyCap.date == date
    )
    if len(team_ids) > 0:
        record_query = (
            DailyCap.query.filter(DailyCap.season_id == season_id)
            .filter(DailyCap.date == date)
            .filter(DailyCap.team_id.in_(team_ids))
        )
    if record_query.count() != 0:
        deleted = record_query.delete()
    created = create_nhl_cap_hit(season_id, date, team_ids)
    current_app.logger.debug("  created NHL cap hit records")
    created += create_minors_thirtyfive_cap_hit(season_id, date, team_ids)
    current_app.logger.debug("  created 35+ NHL cap hit records")
    created += create_minors_cap_hit(season_id, date, team_ids)
    current_app.logger.debug("  created minors cap hit records")
    created += create_retained_nhl_cap_hit(season_id, date, team_ids)
    current_app.logger.debug("  created NHL retained cap hit records")
    created += create_retained_minors_thirtyfive_cap_hit(season_id, date, team_ids)
    current_app.logger.debug("  created minors 35+ cap hit records")
    created += create_retained_minors_cap_hit(season_id, date, team_ids)
    current_app.logger.debug("  created minors retained cap hit records")
    update_soir_cap_hits(season_id, date)
    current_app.logger.debug("  updated SOIR cap hits")
    db.session.commit()
    return (deleted, created)


def update_soir_cap_hits(season_id=None, date=date.today()):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    soir_records = (
        DailyCap.query.filter(DailyCap.season_id == season_id)
        .filter(DailyCap.soir == True)
        .filter(DailyCap.date == date)
        .all()
    )
    for record in soir_records:
        record.cap_hit = round(soir_ratio(record) * record.cap_hit, 2)
        db.session.add(record)
    return


def soir_ratio(daily_cap):
    last_season = (
        Season.query.filter(
            Season.free_agency_opening
            < (
                Season.query.filter(Season.id == daily_cap.season_id)
                .with_entities(Season.free_agency_opening)
                .scalar_subquery()
            )
        )
        .with_entities(Season.id)
        .order_by(Season.free_agency_opening.desc())
        .limit(1)
        .scalar_subquery()
    )
    person_id = (
        Contract.query.filter(Contract.id == daily_cap.contract_id)
        .with_entities(Contract.person_id)
        .scalar_subquery()
    )
    contract_ids = (
        Contract.query.filter(Contract.person_id == person_id)
        .with_entities(
            Contract.id,
        )
        .subquery()
    )
    last_season_length = (
        Season.query.filter(Season.id == last_season)
        .with_entities(
            func.julianday(Season.regular_season_end)
            - func.julianday(Season.regular_season_start),
        )
        .first()[0]
    )
    ratio = (
        DailyCap.query.filter(DailyCap.season_id == last_season)
        .filter(DailyCap.non_roster == False)
        .filter(DailyCap.contract_id.in_(select(contract_ids)))
        .count()
        / last_season_length
    )

    if ratio != 0:
        return ratio

    query = (
        SkaterSeason.query.filter(SkaterSeason.person_id == person_id)
        .filter(SkaterSeason.season_id == last_season)
        .group_by(SkaterSeason.person_id)
        .with_entities(func.sum(SkaterSeason.games_played))
        .first()
    )
    if query:
        return query[0] / 82

    query = (
        GoalieSeason.query.filter(GoalieSeason.person_id == person_id)
        .filter(GoalieSeason.season_id == last_season)
        .group_by(GoalieSeason.person_id)
        .with_entities(func.sum(GoalieSeason.games_played))
        .first()
    )
    if query:
        return query[0] / 82

    return 0


def create_nhl_cap_hit(season_id=None, date=date.today(), team_ids=[]):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    daily_cap_data = (
        ContractYear.query.join(Season, Season.id == ContractYear.season_id)
        .join(Contract, Contract.id == ContractYear.contract_id)
        .join(Asset, Asset.id == Contract.id)
        .join(Person, Person.id == Contract.person_id)
        .filter(ContractYear.season_id == season_id)
        .filter(Asset.active == True)
        .filter(Contract.non_roster == False)
    )
    if len(team_ids) > 0:
        daily_cap_data = daily_cap_data.filter(Asset.current_team.in_(team_ids))
    daily_cap_data = daily_cap_data.with_entities(
        Asset.current_team,
        ContractYear.season_id,
        Contract.id,
        func.date(date),
        Contract.non_roster,
        False,
        func.round(
            Contract.cap_hit_scale_factor
            * (
                RetainedSalary.query.filter(RetainedSalary.contract_id == Contract.id)
                .with_entities(
                    100 - func.coalesce(func.sum(RetainedSalary.retention), 0)
                )
                .scalar_subquery()
                / 100.0
            )
            * ContractYear.cap_hit
            / func.cast(
                func.julianday(Season.regular_season_end)
                - func.julianday(Season.regular_season_start),
                Integer,
            ),
            2,
        ),
        case(
            (
                and_(
                    Person.current_status.like("%IR%"),
                    Person.current_status.not_like("%SOIR%"),
                    Person.current_status.not_like("%LTIR%"),
                ),
                True,
            ),
            else_=False,
        ),
        case((Person.current_status.like("%SOIR%"), True), else_=False),
        case((Person.current_status.like("%LTIR%"), True), else_=False),
        Contract.on_loan,
        Contract.in_juniors,
        case((Person.current_status.like("%Suspended%"), True), else_=False),
        case((Person.current_status.like("%Emergency Callup%"), True), else_=False),
    ).subquery()
    create_records = insert(DailyCap).from_select(
        [
            "team_id",
            "season_id",
            "contract_id",
            "date",
            "non_roster",
            "retained",
            "cap_hit",
            "ir",
            "soir",
            "ltir",
            "on_loan",
            "in_juniors",
            "suspended",
            "emergency_loan",
        ],
        select(daily_cap_data),
    )
    result = db.session.execute(create_records)
    return result.rowcount


def create_minors_thirtyfive_cap_hit(season_id=None, date=date.today(), team_ids=[]):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    daily_cap_data = (
        ContractYear.query.join(Season, Season.id == ContractYear.season_id)
        .join(Contract, Contract.id == ContractYear.contract_id)
        .join(Person, Person.id == Contract.person_id)
        .join(Asset, Asset.id == Contract.id)
        .filter(ContractYear.season_id == season_id)
        .filter(Asset.active == True)
        .filter(Contract.non_roster == True)
        .filter(Contract.thirty_five_plus == True)
    )
    if len(team_ids) > 0:
        daily_cap_data = daily_cap_data.filter(Asset.current_team.in_(team_ids))
    daily_cap_data = daily_cap_data.with_entities(
        Asset.current_team,
        ContractYear.season_id,
        Contract.id,
        func.date(date),
        Contract.non_roster,
        False,
        func.round(
            Contract.cap_hit_scale_factor
            * (
                RetainedSalary.query.filter(RetainedSalary.contract_id == Contract.id)
                .with_entities(
                    100 - func.coalesce(func.sum(RetainedSalary.retention), 0)
                )
                .scalar_subquery()
                / 100.0
            )
            * func.max(
                case(
                    (ContractYear.two_way == False, ContractYear.cap_hit),
                    (
                        ContractYear.two_way == True,
                        ContractYear.minors_salary + ContractYear.signing_bonus,
                    ),
                )
                - 100000,
                0,
            )
            / func.cast(
                func.julianday(Season.regular_season_end)
                - func.julianday(Season.regular_season_start),
                Integer,
            ),
            2,
        ),
        case(
            (
                and_(
                    Person.current_status.like("%IR%"),
                    Person.current_status.not_like("%SOIR%"),
                    Person.current_status.not_like("%LTIR%"),
                ),
                True,
            ),
            else_=False,
        ),
        case((Person.current_status.like("%SOIR%"), True), else_=False),
        case((Person.current_status.like("%LTIR%"), True), else_=False),
        Contract.on_loan,
        Contract.in_juniors,
        case((Person.current_status.like("%Suspended%"), True), else_=False),
        case((Person.current_status.like("%Emergency Callup%"), True), else_=False),
    ).subquery()
    create_records = insert(DailyCap).from_select(
        [
            "team_id",
            "season_id",
            "contract_id",
            "date",
            "non_roster",
            "retained",
            "cap_hit",
            "ir",
            "soir",
            "ltir",
            "on_loan",
            "in_juniors",
            "suspended",
            "emergency_loan",
        ],
        select(daily_cap_data),
    )
    result = db.session.execute(create_records)
    return result.rowcount


def create_minors_cap_hit(season_id=None, date=date.today(), team_ids=[]):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    daily_cap_data = (
        ContractYear.query.join(Season, Season.id == ContractYear.season_id)
        .join(Contract, Contract.id == ContractYear.contract_id)
        .join(Person, Person.id == Contract.person_id)
        .join(Asset, Asset.id == Contract.id)
        .filter(ContractYear.season_id == season_id)
        .filter(Asset.active == True)
        .filter(Contract.non_roster == True)
        .filter(Contract.thirty_five_plus == False)
    )
    if len(team_ids) > 0:
        daily_cap_data = daily_cap_data.filter(Asset.current_team.in_(team_ids))
    daily_cap_data = daily_cap_data.with_entities(
        Asset.current_team,
        ContractYear.season_id,
        Contract.id,
        func.date(date),
        Contract.non_roster,
        False,
        func.round(
            Contract.cap_hit_scale_factor
            * (
                RetainedSalary.query.filter(RetainedSalary.contract_id == Contract.id)
                .with_entities(
                    100 - func.coalesce(func.sum(RetainedSalary.retention), 0)
                )
                .scalar_subquery()
                / 100.0
            )
            * func.max(
                case(
                    (ContractYear.two_way == False, ContractYear.cap_hit),
                    (
                        ContractYear.two_way == True,
                        ContractYear.minors_salary + ContractYear.signing_bonus,
                    ),
                )
                - Season.max_buriable_hit,
                0,
            )
            / func.cast(
                func.julianday(Season.regular_season_end)
                - func.julianday(Season.regular_season_start),
                Integer,
            ),
            2,
        ),
        case(
            (
                and_(
                    Person.current_status.like("%IR%"),
                    Person.current_status.not_like("%SOIR%"),
                    Person.current_status.not_like("%LTIR%"),
                ),
                True,
            ),
            else_=False,
        ),
        case((Person.current_status.like("%SOIR%"), True), else_=False),
        case((Person.current_status.like("%LTIR%"), True), else_=False),
        Contract.on_loan,
        Contract.in_juniors,
        case((Person.current_status.like("%Suspended%"), True), else_=False),
        case((Person.current_status.like("%Emergency Callup%"), True), else_=False),
    ).subquery()
    create_records = insert(DailyCap).from_select(
        [
            "team_id",
            "season_id",
            "contract_id",
            "date",
            "non_roster",
            "retained",
            "cap_hit",
            "ir",
            "soir",
            "ltir",
            "on_loan",
            "in_juniors",
            "suspended",
            "emergency_loan",
        ],
        select(daily_cap_data),
    )
    result = db.session.execute(create_records)
    return result.rowcount


def create_retained_nhl_cap_hit(season_id=None, date=date.today(), team_ids=[]):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    daily_cap_data = (
        RetainedSalary.query.join(
            ContractYear, ContractYear.contract_id == RetainedSalary.contract_id
        )
        .join(Season, Season.id == ContractYear.season_id)
        .join(Contract, Contract.id == ContractYear.contract_id)
        .join(Person, Person.id == Contract.person_id)
        .join(Asset, Asset.id == Contract.id)
        .filter(ContractYear.season_id == season_id)
        .filter(Asset.active == True)
        .filter(Contract.non_roster == False)
    )
    if len(team_ids) > 0:
        daily_cap_data = daily_cap_data.filter(RetainedSalary.retained_by.in_(team_ids))
    daily_cap_data = daily_cap_data.with_entities(
        RetainedSalary.retained_by,
        ContractYear.season_id,
        Contract.id,
        func.date(date),
        Contract.non_roster,
        True,
        func.round(
            Contract.cap_hit_scale_factor
            * RetainedSalary.retention
            * ContractYear.cap_hit
            / 100.0
            / func.cast(
                func.julianday(Season.regular_season_end)
                - func.julianday(Season.regular_season_start),
                Integer,
            ),
            2,
        ),
        case(
            (
                and_(
                    Person.current_status.like("%IR%"),
                    Person.current_status.not_like("%SOIR%"),
                    Person.current_status.not_like("%LTIR%"),
                ),
                True,
            ),
            else_=False,
        ),
        case((Person.current_status.like("%SOIR%"), True), else_=False),
        case((Person.current_status.like("%LTIR%"), True), else_=False),
        Contract.on_loan,
        Contract.in_juniors,
        case((Person.current_status.like("%Suspended%"), True), else_=False),
        case((Person.current_status.like("%Emergency Callup%"), True), else_=False),
    ).subquery()
    create_records = insert(DailyCap).from_select(
        [
            "team_id",
            "season_id",
            "contract_id",
            "date",
            "non_roster",
            "retained",
            "cap_hit",
            "ir",
            "soir",
            "ltir",
            "on_loan",
            "in_juniors",
            "suspended",
            "emergency_loan",
        ],
        select(daily_cap_data),
    )
    result = db.session.execute(create_records)
    return result.rowcount


def create_retained_minors_thirtyfive_cap_hit(
    season_id=None, date=date.today(), team_ids=[]
):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    daily_cap_data = (
        RetainedSalary.query.join(
            ContractYear, ContractYear.contract_id == RetainedSalary.contract_id
        )
        .join(Season, Season.id == ContractYear.season_id)
        .join(Contract, Contract.id == ContractYear.contract_id)
        .join(Person, Person.id == Contract.person_id)
        .join(Asset, Asset.id == Contract.id)
        .filter(ContractYear.season_id == season_id)
        .filter(Asset.active == True)
        .filter(Contract.non_roster == True)
        .filter(Contract.thirty_five_plus == True)
    )
    if len(team_ids) > 0:
        daily_cap_data = daily_cap_data.filter(RetainedSalary.retained_by.in_(team_ids))
    daily_cap_data = daily_cap_data.with_entities(
        RetainedSalary.retained_by,
        ContractYear.season_id,
        Contract.id,
        func.date(date),
        Contract.non_roster,
        True,
        func.round(
            Contract.cap_hit_scale_factor
            * RetainedSalary.retention
            * func.max(
                case(
                    (ContractYear.two_way == False, ContractYear.cap_hit),
                    (
                        ContractYear.two_way == True,
                        ContractYear.minors_salary + ContractYear.signing_bonus,
                    ),
                )
                - 100000,
                0,
            )
            / 100.0
            / func.cast(
                func.julianday(Season.regular_season_end)
                - func.julianday(Season.regular_season_start),
                Integer,
            ),
            2,
        ),
        case(
            (
                and_(
                    Person.current_status.like("%IR%"),
                    Person.current_status.not_like("%SOIR%"),
                    Person.current_status.not_like("%LTIR%"),
                ),
                True,
            ),
            else_=False,
        ),
        case((Person.current_status.like("%SOIR%"), True), else_=False),
        case((Person.current_status.like("%LTIR%"), True), else_=False),
        Contract.on_loan,
        Contract.in_juniors,
        case((Person.current_status.like("%Suspended%"), True), else_=False),
        case((Person.current_status.like("%Emergency Callup%"), True), else_=False),
    ).subquery()
    create_records = insert(DailyCap).from_select(
        [
            "team_id",
            "season_id",
            "contract_id",
            "date",
            "non_roster",
            "retained",
            "cap_hit",
            "ir",
            "soir",
            "ltir",
            "on_loan",
            "in_juniors",
            "suspended",
            "emergency_loan",
        ],
        select(daily_cap_data),
    )
    result = db.session.execute(create_records)
    return result.rowcount


def create_retained_minors_cap_hit(season_id=None, date=date.today(), team_ids=[]):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    daily_cap_data = (
        RetainedSalary.query.join(
            ContractYear, ContractYear.contract_id == RetainedSalary.contract_id
        )
        .join(Season, Season.id == ContractYear.season_id)
        .join(Contract, Contract.id == ContractYear.contract_id)
        .join(Person, Person.id == Contract.person_id)
        .join(Asset, Asset.id == Contract.id)
        .filter(ContractYear.season_id == season_id)
        .filter(Asset.active == True)
        .filter(Contract.non_roster == True)
        .filter(Contract.thirty_five_plus == False)
    )
    if len(team_ids) > 0:
        daily_cap_data = daily_cap_data.filter(RetainedSalary.retained_by.in_(team_ids))
    daily_cap_data = daily_cap_data.with_entities(
        RetainedSalary.retained_by,
        ContractYear.season_id,
        Contract.id,
        func.date(date),
        Contract.non_roster,
        True,
        func.round(
            Contract.cap_hit_scale_factor
            * RetainedSalary.retention
            * func.max(
                case(
                    (ContractYear.two_way == False, ContractYear.cap_hit),
                    (
                        ContractYear.two_way == True,
                        ContractYear.minors_salary + ContractYear.signing_bonus,
                    ),
                )
                - Season.max_buriable_hit,
                0,
            )
            / 100.0
            / func.cast(
                func.julianday(Season.regular_season_end)
                - func.julianday(Season.regular_season_start),
                Integer,
            ),
            2,
        ),
        case(
            (
                and_(
                    Person.current_status.like("%IR%"),
                    Person.current_status.not_like("%SOIR%"),
                    Person.current_status.not_like("%LTIR%"),
                ),
                True,
            ),
            else_=False,
        ),
        case((Person.current_status.like("%SOIR%"), True), else_=False),
        case((Person.current_status.like("%LTIR%"), True), else_=False),
        Contract.on_loan,
        Contract.in_juniors,
        case((Person.current_status.like("%Suspended%"), True), else_=False),
        case((Person.current_status.like("%Emergency Callup%"), True), else_=False),
    ).subquery()
    create_records = insert(DailyCap).from_select(
        [
            "team_id",
            "season_id",
            "contract_id",
            "date",
            "non_roster",
            "retained",
            "cap_hit",
            "ir",
            "soir",
            "ltir",
            "on_loan",
            "in_juniors",
            "suspended",
            "emergency_loan",
        ],
        select(daily_cap_data),
    )
    result = db.session.execute(create_records)
    return result.rowcount


def daily_cap_used(team_id, season_id=None, on_date=date.today()):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    cap_hit = (
        DailyCap.query.filter(DailyCap.season_id == season_id)
        .filter(DailyCap.date <= on_date)
        .filter(DailyCap.team_id == team_id)
        .with_entities(func.round(func.sum(DailyCap.cap_hit), 2))
        .first()
    )
    if not cap_hit:
        return 0
    return cap_hit[0] or 0


def buyout_cap_hit(team_id, season_id=None):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    cap_hit = (
        BuyoutYear.query.join(Asset, Asset.id == BuyoutYear.contract_id)
        .filter(Asset.current_team == team_id)
        .filter(BuyoutYear.season_id == season_id)
        .with_entities(
            func.round(
                func.sum(
                    (
                        RetainedSalary.query.filter(
                            RetainedSalary.contract_id == BuyoutYear.contract_id
                        )
                        .with_entities(
                            100 - func.coalesce(func.sum(RetainedSalary.retention), 0)
                        )
                        .scalar_subquery()
                        / 100.0
                    )
                    * BuyoutYear.cap_hit
                ),
                2,
            )
        )
        .first()
    )
    if not cap_hit:
        return 0
    return cap_hit[0] or 0


def retained_buyout_cap_hit(team_id, season_id=None):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    cap_hit = (
        RetainedSalary.query.join(
            BuyoutYear, BuyoutYear.contract_id == RetainedSalary.contract_id
        )
        .filter(RetainedSalary.retained_by == team_id)
        .filter(BuyoutYear.season_id == season_id)
        .with_entities(
            func.round(
                func.sum(1.0 * RetainedSalary.retention * BuyoutYear.cap_hit / 100), 2
            )
        )
        .first()
    )
    if not cap_hit:
        return 0
    return cap_hit[0] or 0


def penalty_cap_hit(team_id, season_id=None):
    if not season_id:
        season_id = current_app.config["CURRENT_SEASON"]
    cap_hit = (
        PenaltyYear.query.join(Asset, Asset.id == PenaltyYear.penalty_id)
        .filter(PenaltyYear.season_id == season_id)
        .filter(Asset.current_team == team_id)
        .with_entities(func.sum(PenaltyYear.cap_hit))
        .first()
    )
    if not cap_hit:
        return 0
    return cap_hit[0] or 0


"""
Offer sheet nhl salary
"""

"""
Tagging two way QOs, contracts
"""

"""
Tagging two way offer sheets
"""

"""
Deferred salary and bonuses per 50.2(a) and 50.2(b)
"""


class RecalculateDailyCap(FlaskForm):
    def __init__(self):
        super().__init__()
        self.team.choices = self.get_teams()

    def get_teams(self):
        raw_teams = (
            Team.query.with_entities(Team.id, Team.name)
            .filter(Team.active == True)
            .order_by(Team.name.asc())
            .all()
        )
        return [tuple(team) for team in raw_teams]

    date = DateField("Date", default="", validators=[DataRequired()], id="date")
    team = SelectMultipleField(
        "Team",
        default="",
        validators=[DataRequired()],
        validate_choice=False,
        id="team",
    )
    submit = SubmitField("Submit")
