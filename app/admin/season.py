from datetime import date, datetime
from flask import current_app, render_template
from flask_wtf import FlaskForm
from wtforms import SubmitField, IntegerField, DateField, HiddenField
from wtforms.validators import Optional
from .. import db
from ..models import Season


def season_ids():
    raw_data = (
        Season.query.order_by(Season.free_agency_opening.asc())
        .with_entities(
            Season.id,
            Season.name,
        )
        .order_by(Season.name.asc())
        .all()
    )
    return [tuple(entry) for entry in raw_data]


def valid_id(season_id):
    record = Season.query.filter(Season.id == season_id).first()
    return record is not None


class SeasonForm(FlaskForm):
    def set_data(self, season_id=None):
        season = Season.query.filter(Season.id == season_id).first()
        self.id.data = season_id
        self.name.data = season.name
        self.salary_cap.data = season.salary_cap
        self.salary_floor.data = season.salary_floor
        self.minimum_salary.data = season.minimum_salary
        self.max_buriable.data = season.max_buriable_hit
        self.bonus_overage.data = season.bonus_overage_limit
        self.draft_date.data = season.draft_date
        self.free_agency_opening.data = season.free_agency_opening
        self.regular_season.data = season.regular_season_start
        self.playoffs.data = season.playoffs_start
        self.max_elc_salary.data = season.max_elc_salary
        self.max_elc_minors.data = season.max_elc_minor_salary
        self.max_elc_performance.data = season.max_elc_performance_bonuses

    def update_season(self):
        season = Season.query.filter(Season.id == self.id.data).first()
        season.salary_cap = self.salary_cap.data
        season.salary_floor = self.salary_floor.data
        season.minimum_salary = self.minimum_salary.data
        season.max_buriable_hit = self.max_buriable.data
        season.bonus_overage_limit = self.bonus_overage.data
        season.draft_date = self.draft_date.data
        season.free_agency_opening = self.free_agency_opening.data
        season.regular_season_start = self.regular_season.data
        season.playoffs_start = self.playoffs.data
        season.max_elc_salary = self.max_elc_salary.data
        season.max_elc_minor_salary = self.max_elc_minors.data
        season.max_elc_performance_bonuses = self.max_elc_performance.data
        db.session.add(season)
        db.session.commit()

    id = HiddenField("id")
    name = HiddenField("name")
    salary_cap = IntegerField("Salary Cap Ceiling", validators=[Optional()])
    salary_floor = IntegerField("Salary Floor", validators=[Optional()])
    minimum_salary = IntegerField("Minimum Salary", validators=[Optional()])
    max_buriable = IntegerField("Maximum Buriable Cap Hit", validators=[Optional()])
    bonus_overage = IntegerField("Maximum Bonus Overage", validators=[Optional()])
    draft_date = DateField("Draft Date", validators=[Optional()])
    free_agency_opening = DateField("Free Agency Opening", validators=[Optional()])
    regular_season = DateField("Regular Season Start", validators=[Optional()])
    playoffs = DateField("Playoffs Start", validators=[Optional()])
    max_elc_salary = IntegerField("Maximum Entry Level Salary", validators=[Optional()])
    max_elc_minors = IntegerField(
        "Maximum Entry Level AHL Salary", validators=[Optional()]
    )
    max_elc_performance = IntegerField(
        "Maximum Entry Level Performance Bonuses", validators=[Optional()]
    )
    submit = SubmitField("Update Season")
