from datetime import date, datetime, timedelta
from flask import render_template, url_for, redirect, abort, current_app
from sqlalchemy import or_, select, func
from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField
from wtforms.validators import DataRequired

from .. import db
from ..models import (
    Person,
    Contract,
    ContractYear,
    Asset,
    Team,
    Season,
    SkaterSeason,
    GoalieSeason,
    SigningRights,
    Transaction,
    TransactionAsset,
)


def qualifying_offer(form, apply=None):
    return render_template(
        "admin/qualifying_offer.html",
        title="Qualifying Offer",
        css="qualify.css",
        form=form,
        apply=apply,
    )


def get_pending_rfas(team_id=None):
    next_season_id = (
        Season.query.filter(
            Season.free_agency_opening
            > Season.query.get(current_app.config["CURRENT_SEASON"]).free_agency_opening
        )
        .order_by(Season.free_agency_opening.asc())
        .with_entities(Season.id)
        .scalar_subquery()
    )
    active_signing_rights = (
        SigningRights.query.join(Asset, Asset.id == SigningRights.id)
        .filter(Asset.active == True)
        .with_entities(SigningRights.person_id)
        .subquery()
    )
    results = (
        Person.query.join(Contract, Contract.person_id == Person.id)
        .join(ContractYear, ContractYear.contract_id == Contract.id)
        .filter(Person.active == True)
        .filter(ContractYear.season_id == current_app.config["CURRENT_SEASON"])
        .filter(Contract.expiration_status == "RFA")
        .filter(Contract.person_id.not_in(select(active_signing_rights)))
        .filter(
            Contract.id.not_in(
                ContractYear.query.filter(ContractYear.season_id == next_season_id)
                .with_entities(ContractYear.contract_id)
                .scalar_subquery()
            )
        )
        .with_entities(
            Person.id,
            Person.name,
        )
        .order_by(Person.name.asc())
    )
    if team_id:
        results = results.join(Asset, Asset.id == Contract.id).filter(
            Asset.current_team == team_id
        )
    return [tuple(result) for result in results.all()]


def calculate_qualifying_offer(person_id):
    contract_data = (
        ContractYear.query.join(Contract, Contract.id == ContractYear.contract_id)
        .join(Season, Season.id == ContractYear.season_id)
        .join(Asset, Asset.id == Contract.id)
        .filter(Contract.person_id == person_id)
        .order_by(Season.free_agency_opening.desc())
        .with_entities(
            Contract.signing_date,
            ContractYear.nhl_salary,
            ContractYear.aav,
            Asset.current_team,
            ContractYear.minors_salary,
        )
        .first()
    )

    player = Person.query.get(person_id)
    if player.position == "G":
        stats = (
            GoalieSeason.query.join(Season, Season.id == GoalieSeason.season_id)
            .filter(GoalieSeason.person_id == person_id)
            .order_by(Season.free_agency_opening.desc())
            .group_by(GoalieSeason.season_id)
            .limit(3)
            .with_entities(
                func.max(Season.name),
                func.sum(GoalieSeason.games_played),
                func.sum(GoalieSeason.wins),
                func.sum(GoalieSeason.losses),
                func.sum(GoalieSeason.shutouts),
                func.sum(GoalieSeason.shots_against),
                func.sum(GoalieSeason.goals_against),
            )
            .all()
        )
        stat_names = [
            "games_played",
            "wins",
            "losses",
            "shutouts",
            "shots_against",
            "goals_against",
        ]
    else:
        stats = (
            SkaterSeason.query.join(Season, Season.id == SkaterSeason.season_id)
            .filter(SkaterSeason.person_id == person_id)
            .order_by(Season.free_agency_opening.desc())
            .group_by(SkaterSeason.season_id)
            .limit(3)
            .with_entities(
                func.max(Season.name),
                func.sum(SkaterSeason.games_played),
                func.sum(SkaterSeason.goals),
                func.sum(SkaterSeason.assists),
                func.sum(SkaterSeason.points),
                func.sum(SkaterSeason.shots),
                func.sum(SkaterSeason.hits),
            )
            .all()
        )
        stat_names = ["games_played", "goals", "assists", "points", "shots", "hits"]

    qualifying_salary = contract_data[1]
    if qualifying_salary <= 660000:
        qualifying_salary *= 1.1
    elif qualifying_salary < 1000000:
        qualifying_salary *= 1.05

    if qualifying_salary > 1.2 * contract_data[2] and contract_data[0] >= date(
        year=2020, month=7, day=10
    ):
        qualifying_salary = 1.2 * contract_data[2]

    one_way = False
    waived_since = Season.query.filter(Season.regular_season_start != None).filter(
        Season.regular_season_start
        < (Season.query.get(current_app.config["CURRENT_SEASON"]).regular_season_start)
    ).order_by(
        Season.regular_season_start.desc()
    ).first().regular_season_start - timedelta(
        days=12
    )
    waivers = (
        Transaction.query.join(
            TransactionAsset, Transaction.id == TransactionAsset.transaction_id
        )
        .join(Contract, Contract.id == TransactionAsset.asset_id)
        .filter(Transaction.type == "Waivers")
        .filter(Transaction.date >= waived_since)
        .filter(TransactionAsset.new_team == TransactionAsset.former_team)
        .filter(Contract.person_id == person_id)
        .count()
    )
    if (
        len(stats) > 0
        and stats[0][1] >= 60
        and sum([val[1] for val in stats]) >= 180
        and waivers == 0
    ):
        one_way = True

    data = {
        "name": player.name,
        "id": player.id,
        "team_id": contract_data[3],
        "last_salary": contract_data[1],
        "last_minors_salary": contract_data[4],
        "last_aav": contract_data[2],
        "contract_signing": contract_data[0],
        "qualifying_salary": int(qualifying_salary),
        "one_way": one_way,
        "stat_names": stat_names,
        "stats": [list(stat) for stat in stats],
    }
    return data


def get_qualifying_offer_details(person_id):
    data = calculate_qualifying_offer(person_id)
    return render_template("admin/_qualifying_details.html", data=data, apply=apply)


def qualify_player(person_id):
    data = calculate_qualifying_offer(person_id)
    asset = Asset(
        current_team=data["team_id"],
        originating_team=data["team_id"],
        active=True,
        type="Signing Rights",
    )
    db.session.add(asset)
    db.session.flush()

    signing_rights = SigningRights(
        id=asset.id,
        person_id=person_id,
        expiration_date=date.today().replace(month=7, day=15),
        qualifying_offer=True,
        qualifying_salary=data["qualifying_salary"],
        qualifying_minors=data["qualifying_salary"]
        if data["one_way"]
        else data["last_minors_salary"],
    )
    db.session.add(signing_rights)
    db.session.commit()
    return


def get_teams():
    query = (
        Team.query.with_entities(Team.id, Team.name)
        .filter(Team.active == True)
        .order_by(Team.name.asc())
        .all()
    )
    return [tuple(result) for result in query]


class QualifyingOffer(FlaskForm):
    def __init__(self):
        super().__init__()
        self.team.choices = [("", "All")] + get_teams()
        self.person.choices = get_pending_rfas()

    team = SelectField("Team", default="", id="team")
    person = SelectField("Player", validators=[DataRequired()], id="players")
    submit = SubmitField("Apply To Site")
