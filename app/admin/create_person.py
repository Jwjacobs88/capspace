import urllib.request
from flask import current_app
from flask_wtf import FlaskForm
from sqlalchemy import or_, and_
from wtforms import (
    StringField,
    DateField,
    IntegerField,
    SelectField,
    SelectMultipleField,
    BooleanField,
    SubmitField,
)
from wtforms.validators import DataRequired, NumberRange, Length, Optional

from .. import db
from ..models import Person, Alias


class CreatePerson(FlaskForm):
    # Biographical
    name = StringField("Name", validators=[DataRequired()])
    birthdate = DateField("Birthdate", validators=[Optional()])
    birthplace = StringField("Birthplace")
    nationality = StringField(
        "Nationality (3 chars)", validators=[Length(min=3, max=3), Optional()]
    )
    nhl_id = IntegerField("NHL ID", validators=[Optional()])
    ep_id = IntegerField("EliteProspects ID", validators=[Optional()])

    # Physical
    feet = IntegerField("Feet", validators=[NumberRange(min=4, max=9), Optional()])
    inches = IntegerField("Inches", validators=[NumberRange(min=0, max=11), Optional()])
    weight = IntegerField(
        "Weight (lbs)", validators=[NumberRange(min=48, max=400), Optional()]
    )

    # Hockey data
    number = IntegerField("Number", validators=[NumberRange(min=1, max=98), Optional()])
    position = SelectMultipleField(
        "Position",
        choices=["F", "C", "RW", "LW", "D", "LD", "RD", "G"],
        validators=[DataRequired()],
    )
    shoots = SelectField(
        "Shoots", choices=[("L", "Left"), ("R", "Right")], validators=[Optional()]
    )
    arbitration = BooleanField("Arbitration Eligible", default=False)
    waivers = BooleanField("Waivers Exempt", default=True)
    active = BooleanField("Active", default=True)
    rookie = BooleanField("Rookie", default=True)
    captain = BooleanField("Captain", default=False)
    alternate_captain = BooleanField("Alternate Captain", default=False)
    non_roster = BooleanField("Non-roster player", default=True)
    aliases = SelectMultipleField(
        "Aliases", choices=[""], validators=[Optional()], validate_choice=False
    )
    submit = SubmitField("Create User")

    # Only forward or only defense or only goalie
    def validate_position(self, position):
        if not position.data:
            return True
        selections = set(position.data)
        forward_positions = {"C", "RW", "LW"}
        left_defense_position = {"LD"}
        right_defense_position = {"RD"}
        general_defense = {"D"}
        goalie_position = {"G"}
        is_forward = selections.issubset(forward_positions)
        is_defense = (
            selections.issubset(left_defense_position)
            or selections.issubset(right_defense_position)
            or selections.issubset(general_defense)
        )
        is_goalie = selections.issubset(goalie_position)
        current_app.logger.debug(
            f"Position check: {is_forward=} {is_defense=} {is_goalie=}"
        )
        return is_forward or is_defense or is_goalie

    def validate_nhl_id(self, nhl_id):
        if not nhl_id.data:
            return True
        try:
            request = urllib.request.urlopen(
                urllib.request.Request(
                    f"https://api-web.nhle.com/v1/player/{nhl_id.data}/landing",
                    data=None,
                    headers={
                        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0"
                    },
                )
            )
        except urllib.error.HTTPError:
            nhl_id.errors.append("Error validating ID on api-web.nhle.com")
            current_app.logger.error(f"NHL ID check: not found through stats API")
            return False
        return True

    def validate_ep_id(self, ep_id):
        if ep_id.data is None:
            return True
        person = Person.query.filter(Person.ep_id == ep_id.data).count()
        if person == 0:
            return True
        ep_id.errors.append("EliteProspects ID already found in database")
        return False

    def duplicate_person_exists(self, person):
        duplicate_count = Person.query.filter(
            or_(
                and_(Person.ep_id != None, Person.ep_id == person.ep_id),
                and_(Person.nhl_id != None, Person.nhl_id == person.nhl_id),
                and_(
                    Person.name == person.name,
                    Person.birthdate == person.birthdate,
                    Person.birthplace == Person.birthplace,
                ),
            )
        ).count()
        return duplicate_count > 0

    def create_person(self):
        new_person = Person(
            name=self.name.data,
            position=",".join(self.position.data),
            shoots=self.shoots.data,
            arbitration=self.arbitration.data,
            waivers_exempt=self.waivers.data,
            active=self.active.data,
            rookie=self.rookie.data,
            captain=self.captain.data,
            alternate_captain=self.alternate_captain.data,
            non_roster=self.non_roster.data,
        )
        if self.feet.data and self.inches.data:
            new_person.height = 12 * self.feet.data + self.inches.data
        if self.weight.data:
            new_person.weight = self.weight.data
        if self.nhl_id.data:
            new_person.nhl_id = self.nhl_id.data
        if self.birthdate.data:
            new_person.birthdate = self.birthdate.data
        if self.birthplace.data:
            new_person.birthplace = self.birthplace.data
        if self.nationality.data:
            new_person.nationality = self.nationality.data
        if self.number.data:
            new_person.number = self.number.data
        if self.ep_id.data:
            new_person.ep_id = self.ep_id.data

        if self.duplicate_person_exists(new_person):
            current_app.logger.error(
                f"Duplicate person (name={self.name.data} or nhl_id={self.nhl_id.data})"
            )
            self.submit.errors.append(
                "Submission aborted: person already exists in database"
            )
            return None

        db.session.add(new_person)
        db.session.flush()
        if not self.aliases.data:
            self.aliases.data = []
        for value in self.aliases.data:
            db.session.add(
                Alias(
                    person_id=new_person.id,
                    value=value,
                )
            )

        normalize_map = {
            "Á": "A",
            "À": "A",
            "Â": "A",
            "Ä": "A",
            "Ã": "A",
            "Å": "A",
            "Ā": "A",
            "Ç": "C",
            "Č": "C",
            "Ð": "D",
            "Ď": "D",
            "É": "E",
            "È": "E",
            "Ë": "E",
            "Ê": "E",
            "Í": "I",
            "Ì": "I",
            "Ï": "I",
            "Î": "I",
            "Ī": "I",
            "Ĺ": "L",
            "Ļ": "L",
            "Ñ": "N",
            "Ň": "N",
            "Ń": "N",
            "Ņ": "N",
            "Ó": "O",
            "Ò": "O",
            "Ö": "O",
            "Ô": "O",
            "Õ": "O",
            "Ø": "O",
            "Ř": "R",
            "Ŕ": "R",
            "Š": "S",
            "Ť": "T",
            "Ú": "U",
            "Ù": "U",
            "Ü": "U",
            "Û": "U",
            "Ů": "U",
            "Ū": "U",
            "Ý": "Y",
            "Ÿ": "Y",
            "Ž": "Z",
            "Ź": "Z",
            "Ż": "Z",
            "á": "a",
            "à": "a",
            "â": "a",
            "ä": "a",
            "ã": "a",
            "å": "a",
            "ā": "a",
            "ç": "c",
            "č": "c",
            "ð": "d",
            "ď": "d",
            "é": "e",
            "è": "e",
            "ë": "e",
            "ê": "e",
            "í": "i",
            "ì": "i",
            "ï": "i",
            "î": "i",
            "ī": "i",
            "ĺ": "l",
            "ļ": "l",
            "ñ": "n",
            "ň": "n",
            "ń": "n",
            "ņ": "n",
            "ó": "o",
            "ò": "o",
            "ö": "o",
            "ô": "o",
            "õ": "o",
            "ø": "o",
            "ř": "r",
            "ŕ": "r",
            "š": "s",
            "ť": "t",
            "ú": "u",
            "ù": "u",
            "ü": "u",
            "û": "u",
            "ů": "u",
            "ū": "u",
            "ý": "y",
            "ÿ": "y",
            "ž": "z",
            "ź": "z",
            "ż": "z",
            "Æ": "AE",
            "æ": "ae",
            "Œ": "OE",
            "œ": "oe",
            "Þ": "P",
            "þ": "p",
            "ß": "ss",
        }
        normalize = str.maketrans(normalize_map)
        db.session.add(
            Alias(
                person_id=new_person.id,
                value=new_person.name,
                search=True,
            )
        )
        if new_person.name != new_person.name.translate(normalize):
            db.session.add(
                Alias(
                    person_id=new_person.id,
                    value=new_person.name.translate(normalize),
                    search=True,
                )
            )

        db.session.commit()
        return new_person.id
