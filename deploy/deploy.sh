#!/usr/bin/env bash
PORT="8000"

start() {
    if [[ "${PWD}" != "${VIRTUAL_ENV}" ]]; then
        DEACTIVATE="TRUE"
        source bin/activate
    fi
    python -mgunicorn -w 2 -b localhost:"${PORT}" capspace:app > log/output.log 2> log/error.log &
    PID="$!"
    echo "Gunicorn PID=${PID}"
    echo "${PID}" > capspace.pid
}

stop() {
    if [[ -f capspace.pid ]]; then
        PID="$(cat capspace.pid)"
        kill "${PID}"
    elif [[ "${PORT}" == "" ]]; then
        echo "Cannot accurately kill process. Please try manually"
        exit 1
    else
        ps aux | grep gunicorn | grep "${PORT}" | awk '{print $2}' | xargs kill
    fi
}

case "$1" in
    start)
        start
    ;;
    stop)
        stop
    ;;
    restart)
        stop
        start
    ;;
    *)
        echo "Usage (from repository root): $0 {start|stop|restart}"
    ;;
esac
