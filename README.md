# capspace - an NHL cap tracking website

a player and team salary cap site for the nhl  
built with flask and sqlalchemy  
something i've wanted to do since i first saw capgeek in high school

## running

python 3.10 is required - the site uses 3.10's match-case syntax
from the repository root directory

    python -mvenv .
    source bin/activate
    python -mpip install -r requirements.txt
    git config --local core.hooksPath .hooks
    flask db upgrade
    flask run #It's a dev server, not for production

the site will be empty - there's no data  
the sql dump (and the test data I've been mangling) is out of date. sorry  
it's still somewhere to start from. just unzip and `.read` it into sqlite.
maybe run `flask db upgrade` to get the right schema  

if you'd like to fix your own data up, there's a database diagram in misc/ that loads very nicely in Ondřej Žára's [sql designer](https://sql.toad.cz/)

to access the admin menu

    sqlite3 app.db
    update user set admin=1 where username = '<your_user>';

## testing

there are tests  
the quality is debatable, but they've made some updates much simpler

    coverage run [&& coverage report && coverage html]

## view the site

Visit [CapSpace](https://cap-space.com)  
Data may be a few days to a week-ish out of date  
Some images of the site throughout its development are available at the [site progress page](https://cap-space.com/site-progress)

## todo

1. BUGFIX: trade check for contract count includes signing rights. should ignore those
1. Make /transactions reorderable client side
1. Text for inactive teams
    - Detroit Cougars
    - Detroit Falcons
    - Hamilton Tigers
    - Hartford Whalers
    - Kansas City Scouts
    - Minnesota North Stars
    - Montreal Maroons
    - Montreal Wanderers
    - New York Americans
    - Oakland Seals
    - Ottawa Senators
    - Philadelphia Quakers
    - Phoenix Coyotes
    - Pittsburgh Pirates
    - Quebec Bulldogs
    - Quebec Nordiques
    - St. Louis Eagles
    - Toronto Arenas
    - Toronto St. Patricks
    - Winnipeg Jets
1. Use TeamSeason for trade proposal checks where possible
    - Cap check (include LTIR)
1. Update TeamSeason for ACSL, LTIR cap pool, and LTIR PB pool, bonus overage
1. Add ad-hoc transactions to roster update pages (to let you note down players being made non-roster without going to the AHL)
    - Get ad-hoc transactions from juniors not to unset non-roster
    - Add test for ad-hoc transactions
1. Release v4.3
1. Text for active teams (and a /team/&lt;abbr&gt;/about link for it)
1. Update notes + notes editing (on-page edits?)
1. Offseason cap tracking
    - Implement offseason QO tracking (two-way and one-way)
    - Implement offseason offer sheet tracking
    - Implement offseason two-way contract tracking. Fall back to non\_roster == True : minors ? nhl
1. Awards data
    - Awards table: name, reason, webpage link
    - Award recipient table: person\_id, year
    - Admin award pages like notes.html and note.html
1. Unscheduled mode
    - Wrap all apscheduler jobs
1. Static mode
    - "STATIC" config value
    - Wrap admin menu in config check
    - Wrap search
    - Wrap login/register/user
    - Wrap interactive
    - Wrap draft year selector
    - Wrap edit person button
    - Wrap edit contract notes button
    - Wrap person notes button
    - Wrap trade and transaction "Load more" buttons
    - Wrap trade and transaction date filters
    - Wrap apscheduler jobs
    - Prevent EP stats from updating
    - "STATIC_LIMIT" date config
    - Limit trades from STATIC_LIMIT onwards. Otherwise load all/load limit
    - Limit transactions from STATIC_LIMIT onwards. Otherwise load all/load limit
1. Create /team/&lt;abbr&gr;/about for active teams. Show defunct page
    - Redirect defunct teams to /team/&lt;abbr&gr;
    - Create text for each team
1. Release site v5
1. Dark mode
1. Figure out copying database from localhost to VPS + auto-backups
1. Restrict and document API
1. Add trade lookup + display for draft picks
    - Ability to display trades around a draft pick or player
    - Slimmed down template that can just be rendered on draft notes
1. Create daily\_cap\_summary table
1. Periodically poll NHL API standings for TeamSeason (once it's dug up :/ )
    - Order draft picks by standings
1. More admin things
    - edit links check for /admin/pagename access before becoming visible
    - "Edit" link on contracts (team and person)
    - "Edit" link on people bio -> Inherit create person form
    - "Edit" link on team bio
    - "Edit" link on draft
    - "Edit" link on team draft picks
    - Find some way to set a banner
1. Once have daily tracking, show rosters from a specific date and team (for any date with daily tracking)
1. Update create\_contract.py - create and check a 35+ contract
1. Update team.py tests
    - retained salary -> inactive vs active retention
    - retaining only active contract, not extension
    - fix generally (maybe with a static team?)
    - buyout with retention. buying out team + team retaining
    - get\_buyout\_retention single function test
    - update get\_non\_roster test for buyout + buyout with retention
    - getting penalty data
    - missing tests
1. Update person.py tests for penalties
1. Buyouts with retained salary need to deactivate retained salary records too
1. Update team\_season tests (get\_penalties)
1. Update draft picks on team.html
    - One color for picks traded away
    - One color for picks traded to
1. Remove flask-apscheduler?
1. mark trades stale if assets inactive or traded away from former\_team
1. Armchair GM
1. UFA and RFA filters
1. CSV exports
1. Trade trees
1. Look at separating templates and static files into each blueprint (https://exploreflask.com/en/latest/blueprints.html#refactoring-small-apps-to-use-blueprints)
