import sys
import random
import unittest
from datetime import datetime, date
from sqlalchemy.orm.exc import MultipleResultsFound
from werkzeug.exceptions import InternalServerError, NotFound

from app import create_app, db
from app.models import (
    Team,
    Season,
    Person,
    Asset,
    DraftPick,
    Contract,
    ContractYear,
    BuyoutYear,
    Transaction,
    TransactionAsset,
)

from app.admin import input_buyout as buyout


class InputBuyoutTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def create_cba1(self):
        person = Person(
            id=4000,
            name="CBA Test 1",
            birthdate=datetime.strptime(
                f"1990-{random.randint(1,12):02}-{random.randint(1,28):02}",
                self.app.config["DATE_FMT"],
            ).date(),
        )
        team = Team(id=1, name="Test One", logo="test")
        seasons = [
            Season(name="AAAAA", free_agency_opening=date(year=2020, month=6, day=1)),
            Season(name="BBBBB", free_agency_opening=date(year=2020, month=6, day=2)),
            Season(name="CCCCC", free_agency_opening=date(year=2020, month=6, day=3)),
            Season(name="DDDDD", free_agency_opening=date(year=2020, month=6, day=4)),
            Season(name="EEEEE", free_agency_opening=date(year=2020, month=6, day=5)),
            Season(name="FFFFF", free_agency_opening=date(year=2020, month=6, day=6)),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.add(person)
        db.session.add(team)
        db.session.commit()
        asset = Asset(current_team=team.id, originating_team=team.id, active=True)
        db.session.add(asset)
        db.session.commit()

        contract = Contract(
            person_id=person.id, id=asset.id, years=3, total_value=10800000
        )
        db.session.add(contract)
        db.session.commit()

        contract_years = [
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[0].id,
                aav=3600000,
                nhl_salary=4000000,
                minors_salary=4000000,
                cap_hit=3600000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[1].id,
                aav=3600000,
                nhl_salary=3600000,
                minors_salary=3600000,
                cap_hit=3600000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[2].id,
                aav=3600000,
                nhl_salary=3200000,
                minors_salary=3200000,
                cap_hit=3600000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        return person.id, seasons[0].name

    def create_cba2(self):
        person = Person(
            id=5000,
            name="CBA Test 2",
            birthdate=datetime.strptime(
                f"1990-{random.randint(1,12):02}-{random.randint(1,28):02}",
                self.app.config["DATE_FMT"],
            ).date(),
        )
        team = Team(name="Test Two")
        seasons = [
            Season(name="AAAAA", free_agency_opening=date(year=2020, month=6, day=1)),
            Season(name="BBBBB", free_agency_opening=date(year=2020, month=6, day=2)),
            Season(name="CCCCC", free_agency_opening=date(year=2020, month=6, day=3)),
            Season(name="DDDDD", free_agency_opening=date(year=2020, month=6, day=4)),
            Season(name="EEEEE", free_agency_opening=date(year=2020, month=6, day=5)),
            Season(name="FFFFF", free_agency_opening=date(year=2020, month=6, day=6)),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.add(person)
        db.session.add(team)
        db.session.commit()
        asset = Asset(current_team=team.id, originating_team=team.id, active=True)
        db.session.add(asset)
        db.session.commit()

        contract = Contract(
            person_id=person.id, id=asset.id, years=3, total_value=19500000
        )
        db.session.add(contract)
        db.session.commit()

        contract_years = [
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[0].id,
                aav=6500000,
                cap_hit=6500000,
                nhl_salary=6000000,
                minors_salary=6000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[1].id,
                aav=6500000,
                cap_hit=6500000,
                nhl_salary=6500000,
                minors_salary=6500000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[2].id,
                aav=6500000,
                cap_hit=6500000,
                nhl_salary=7000000,
                minors_salary=7000000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        return person.id, seasons[0].name

    def create_cba3(self):
        person = Person(
            id=6000,
            name="CBA Test 3",
            birthdate=datetime.strptime(
                f"1990-{random.randint(1,12):02}-{random.randint(1,28):02}",
                self.app.config["DATE_FMT"],
            ).date(),
        )
        team = Team(name="Test Three")
        seasons = [
            Season(name="AAAAA", free_agency_opening=date(year=2020, month=6, day=1)),
            Season(name="BBBBB", free_agency_opening=date(year=2020, month=6, day=2)),
            Season(name="CCCCC", free_agency_opening=date(year=2020, month=6, day=3)),
            Season(name="DDDDD", free_agency_opening=date(year=2020, month=6, day=4)),
            Season(name="EEEEE", free_agency_opening=date(year=2020, month=6, day=5)),
            Season(name="FFFFF", free_agency_opening=date(year=2020, month=6, day=6)),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.add(person)
        db.session.add(team)
        db.session.commit()
        asset = Asset(current_team=team.id, originating_team=team.id, active=True)
        db.session.add(asset)
        db.session.commit()

        contract = Contract(
            person_id=person.id, id=asset.id, years=3, total_value=13500000
        )
        db.session.add(contract)
        db.session.commit()

        contract_years = [
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[0].id,
                aav=4500000,
                cap_hit=4500000,
                nhl_salary=5000000,
                minors_salary=5000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[1].id,
                aav=4500000,
                cap_hit=4500000,
                nhl_salary=4500000,
                minors_salary=4500000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[2].id,
                aav=4500000,
                cap_hit=4500000,
                nhl_salary=4000000,
                minors_salary=4000000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        return person.id, seasons[0].name

    def create_jared_cowen(self):
        birthdate = date.today()
        birthdate = birthdate.replace(year=birthdate.year - 24)
        person = Person(id=6000, name="Jared Cowen", birthdate=birthdate)
        team = Team(
            name="Ottawa Senators",
        )
        seasons = [
            Season(
                name="2013-2014", free_agency_opening=date(year=2013, month=7, day=1)
            ),
            Season(
                name="2014-2015", free_agency_opening=date(year=2014, month=7, day=1)
            ),
            Season(
                name="2015-2016", free_agency_opening=date(year=2015, month=7, day=1)
            ),
            Season(
                name="2016-2017", free_agency_opening=date(year=2016, month=7, day=1)
            ),
            Season(
                name="2017-2018", free_agency_opening=date(year=2017, month=7, day=1)
            ),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.add(person)
        db.session.add(team)
        db.session.commit()
        asset = Asset(current_team=team.id, originating_team=team.id, active=True)
        db.session.add(asset)
        db.session.commit()

        contract = Contract(
            person_id=person.id, id=asset.id, years=4, total_value=12400000
        )
        db.session.add(contract)
        db.session.commit()
        contract_years = [
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[0].id,
                aav=3100000,
                cap_hit=3100000,
                nhl_salary=1500000,
                minors_salary=1500000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[1].id,
                aav=3100000,
                cap_hit=3100000,
                nhl_salary=2700000,
                minors_salary=2700000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[2].id,
                aav=3100000,
                cap_hit=3100000,
                nhl_salary=3700000,
                minors_salary=3700000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[3].id,
                aav=3100000,
                cap_hit=3100000,
                nhl_salary=4500000,
                minors_salary=4500000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        return person.id, seasons[2].name

    def create_zach_parise(self):
        birthdate = date.today()
        birthdate = birthdate.replace(year=birthdate.year - 36)
        person = Person(id=7000, name="Zach Parise", birthdate=birthdate)
        team = Team(
            name="Minnesota Wild",
        )
        seasons = [
            Season(
                name="2012-2013", free_agency_opening=date(year=2012, month=7, day=1)
            ),
            Season(
                name="2013-2014", free_agency_opening=date(year=2013, month=7, day=1)
            ),
            Season(
                name="2014-2015", free_agency_opening=date(year=2014, month=7, day=1)
            ),
            Season(
                name="2015-2016", free_agency_opening=date(year=2015, month=7, day=1)
            ),
            Season(
                name="2016-2017", free_agency_opening=date(year=2016, month=7, day=1)
            ),
            Season(
                name="2017-2018", free_agency_opening=date(year=2017, month=7, day=1)
            ),
            Season(
                name="2018-2019", free_agency_opening=date(year=2018, month=7, day=1)
            ),
            Season(
                name="2019-2020", free_agency_opening=date(year=2019, month=7, day=1)
            ),
            Season(
                name="2020-2021", free_agency_opening=date(year=2020, month=10, day=9)
            ),
            Season(
                name="2021-2022", free_agency_opening=date(year=2021, month=7, day=1)
            ),
            Season(
                name="2022-2023", free_agency_opening=date(year=2022, month=7, day=1)
            ),
            Season(
                name="2023-2024", free_agency_opening=date(year=2023, month=7, day=1)
            ),
            Season(
                name="2024-2025", free_agency_opening=date(year=2024, month=7, day=1)
            ),
            Season(
                name="2025-2026", free_agency_opening=date(year=2025, month=7, day=1)
            ),
            Season(
                name="2026-2027", free_agency_opening=date(year=2026, month=7, day=1)
            ),
            Season(
                name="2027-2028", free_agency_opening=date(year=2027, month=7, day=1)
            ),
            Season(
                name="2028-2029", free_agency_opening=date(year=2028, month=7, day=1)
            ),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.add(person)
        db.session.add(team)
        db.session.commit()
        asset = Asset(current_team=team.id, originating_team=team.id, active=True)
        db.session.add(asset)
        db.session.commit()

        contract = Contract(
            person_id=person.id, id=asset.id, years=13, total_value=98000000
        )
        db.session.add(contract)
        db.session.commit()
        contract_years = [
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[0].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=2000000,
                minors_salary=12000000,
                signing_bonus=10000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[1].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=2000000,
                minors_salary=12000000,
                signing_bonus=10000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[2].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=6000000,
                minors_salary=11000000,
                signing_bonus=5000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[3].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=9000000,
                minors_salary=9000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[4].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=9000000,
                minors_salary=9000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[5].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=9000000,
                minors_salary=9000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[6].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=9000000,
                minors_salary=9000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[7].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=9000000,
                minors_salary=9000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[8].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=8000000,
                minors_salary=8000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[9].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=6000000,
                minors_salary=6000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[10].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=2000000,
                minors_salary=2000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[11].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=1000000,
                minors_salary=1000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[12].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=1000000,
                minors_salary=1000000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        return person.id, seasons[8].name

    def test_get_data_CBA1(self):
        person, season = self.create_cba1()
        buyout_data = buyout.get_data(person, season)
        self.assertTrue(buyout_data["name"] == "CBA Test 1")
        self.assertTrue(buyout_data["buyout"]["buyout_amount"] == 4533333)
        self.assertTrue(buyout_data["buyout"]["buyout_ratio"] == 0.67)
        self.assertTrue(buyout_data["buyout"]["remaining_salary"] == 6800000)
        self.assertTrue(buyout_data["buyout"]["remaining_seasons"] == 2)
        self.assertTrue(buyout_data["buyout"]["remaining_signing_bonuses"] == 0)
        self.assertTrue(buyout_data["buyout"]["yearly_buyout_amount"] == 1133333)

        # Check buyout seasons
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_amount"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_cap_hit"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_pay"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_savings"] == 2466667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["original_cap_hit"] == 3600000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["original_salary"] == 3600000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][0]["season"] == "BBBBB")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][0]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_amount"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_cap_hit"] == 1533333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_pay"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_savings"] == 2066667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["original_cap_hit"] == 3600000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["original_salary"] == 3200000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][1]["season"] == "CCCCC")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][1]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_amount"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_cap_hit"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_pay"] == 1133333
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["buyout_savings"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["original_cap_hit"] == 0
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["original_salary"] == 0
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["season"] == "DDDDD")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_amount"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_cap_hit"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_pay"] == 1133333
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["buyout_savings"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["original_cap_hit"] == 0
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["original_salary"] == 0
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["season"] == "EEEEE")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["signing_bonus"] == 0)

    def test_get_data_CBA2(self):
        person, season = self.create_cba2()
        buyout_data = buyout.get_data(person, season)
        self.assertTrue(buyout_data["name"] == "CBA Test 2")
        self.assertTrue(buyout_data["buyout"]["buyout_amount"] == 9000000)
        self.assertTrue(buyout_data["buyout"]["buyout_ratio"] == 0.67)
        self.assertTrue(buyout_data["buyout"]["remaining_salary"] == 13500000)
        self.assertTrue(buyout_data["buyout"]["remaining_seasons"] == 2)
        self.assertTrue(buyout_data["buyout"]["remaining_signing_bonuses"] == 0)
        self.assertTrue(buyout_data["buyout"]["yearly_buyout_amount"] == 2250000)

        # Check buyout seasons
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_amount"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_cap_hit"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_pay"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_savings"] == 4250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["original_cap_hit"] == 6500000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["original_salary"] == 6500000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][0]["season"] == "BBBBB")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][0]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_amount"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_cap_hit"] == 1750000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_pay"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_savings"] == 4750000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["original_cap_hit"] == 6500000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["original_salary"] == 7000000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][1]["season"] == "CCCCC")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][1]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_amount"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_cap_hit"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_pay"] == 2250000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["buyout_savings"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["original_cap_hit"] == 0
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["original_salary"] == 0
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["season"] == "DDDDD")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_amount"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_cap_hit"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_pay"] == 2250000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["buyout_savings"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["original_cap_hit"] == 0
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["original_salary"] == 0
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["season"] == "EEEEE")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["signing_bonus"] == 0)

    def test_get_data_CBA3(self):
        person, season = self.create_cba3()
        buyout_data = buyout.get_data(person, season)
        self.assertTrue(buyout_data["name"] == "CBA Test 3")
        self.assertTrue(buyout_data["buyout"]["buyout_amount"] == 5666667)
        self.assertTrue(buyout_data["buyout"]["buyout_ratio"] == 0.67)
        self.assertTrue(buyout_data["buyout"]["remaining_salary"] == 8500000)
        self.assertTrue(buyout_data["buyout"]["remaining_seasons"] == 2)
        self.assertTrue(buyout_data["buyout"]["remaining_signing_bonuses"] == 0)
        self.assertTrue(buyout_data["buyout"]["yearly_buyout_amount"] == 1416667)

        # Check buyout seasons
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_amount"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_cap_hit"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_pay"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_savings"] == 3083333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["original_cap_hit"] == 4500000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["original_salary"] == 4500000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][0]["season"] == "BBBBB")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][0]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_amount"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_cap_hit"] == 1916667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_pay"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_savings"] == 2583333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["original_cap_hit"] == 4500000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["original_salary"] == 4000000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][1]["season"] == "CCCCC")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][1]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_amount"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_cap_hit"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_pay"] == 1416667
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["buyout_savings"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["original_cap_hit"] == 0
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["original_salary"] == 0
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["season"] == "DDDDD")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_amount"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_cap_hit"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_pay"] == 1416667
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["buyout_savings"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["original_cap_hit"] == 0
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["original_salary"] == 0
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["season"] == "EEEEE")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["signing_bonus"] == 0)

    def test_apply_cba1_buyout(self):
        person, season = self.create_cba1()
        buyout_data = buyout.get_data(person, season)
        buyout.apply_buyout(str(person))
        contract = Contract.query.filter(Contract.person_id == person).first()

        #   Buy out contract years
        contract_years = (
            ContractYear.query.filter(ContractYear.contract_id == contract.id)
            .order_by(ContractYear.season_id)
            .all()
        )
        self.assertIs(contract_years[0].bought_out, False)
        self.assertIs(contract_years[1].bought_out, True)
        self.assertIs(contract_years[2].bought_out, True)

        #   Create buyout year records
        buyout_years = (
            BuyoutYear.query.filter(BuyoutYear.contract_id == contract.id)
            .order_by(BuyoutYear.season_id)
            .all()
        )
        self.assertTrue(buyout_years[0].cap_hit == 1133333)
        self.assertTrue(buyout_years[0].nhl_salary == 1133333)
        self.assertTrue(buyout_years[0].signing_bonus == 0)
        self.assertTrue(buyout_years[1].cap_hit == 1533333)
        self.assertTrue(buyout_years[1].nhl_salary == 1133333)
        self.assertTrue(buyout_years[1].signing_bonus == 0)
        self.assertTrue(buyout_years[2].cap_hit == 1133333)
        self.assertTrue(buyout_years[2].nhl_salary == 1133333)
        self.assertTrue(buyout_years[2].signing_bonus == 0)
        self.assertTrue(buyout_years[3].cap_hit == 1133333)
        self.assertTrue(buyout_years[3].nhl_salary == 1133333)
        self.assertTrue(buyout_years[3].signing_bonus == 0)

        self.assertEqual(Transaction.query.count(), 1)
        self.assertEqual(TransactionAsset.query.count(), 1)
        transaction = Transaction.query.get(1)
        tr_asset = TransactionAsset.query.get(1)
        contract = Contract.query.filter(Contract.person_id == person).first()
        asset = Asset.query.get(contract.id)
        self.assertEqual(transaction.type, "Bought out")
        self.assertEqual(tr_asset.asset_id, contract.id)
        self.assertEqual(tr_asset.new_team, asset.current_team)

    def test_apply_cba2_buyout(self):
        person, season = self.create_cba2()
        buyout_data = buyout.get_data(person, season)
        buyout.apply_buyout(str(person))
        contract = Contract.query.filter(Contract.person_id == person).first()

        contract_years = (
            ContractYear.query.filter(ContractYear.contract_id == contract.id)
            .order_by(ContractYear.season_id)
            .all()
        )
        self.assertIs(contract_years[0].bought_out, False)
        self.assertIs(contract_years[1].bought_out, True)
        self.assertIs(contract_years[2].bought_out, True)

        buyout_years = (
            BuyoutYear.query.filter(BuyoutYear.contract_id == contract.id)
            .order_by(BuyoutYear.season_id)
            .all()
        )
        self.assertTrue(buyout_years[0].cap_hit == 2250000)
        self.assertTrue(buyout_years[0].nhl_salary == 2250000)
        self.assertTrue(buyout_years[0].signing_bonus == 0)
        self.assertTrue(buyout_years[1].cap_hit == 1750000)
        self.assertTrue(buyout_years[1].nhl_salary == 2250000)
        self.assertTrue(buyout_years[1].signing_bonus == 0)
        self.assertTrue(buyout_years[2].cap_hit == 2250000)
        self.assertTrue(buyout_years[2].nhl_salary == 2250000)
        self.assertTrue(buyout_years[2].signing_bonus == 0)
        self.assertTrue(buyout_years[3].cap_hit == 2250000)
        self.assertTrue(buyout_years[3].nhl_salary == 2250000)
        self.assertTrue(buyout_years[3].signing_bonus == 0)

        self.assertEqual(Transaction.query.count(), 1)
        self.assertEqual(TransactionAsset.query.count(), 1)
        transaction = Transaction.query.get(1)
        tr_asset = TransactionAsset.query.get(1)
        contract = Contract.query.filter(Contract.person_id == person).first()
        asset = Asset.query.get(contract.id)
        self.assertEqual(transaction.type, "Bought out")
        self.assertEqual(tr_asset.asset_id, contract.id)
        self.assertEqual(tr_asset.new_team, asset.current_team)

    def test_apply_cba3_buyout(self):
        BUYOUT_DATE = date(year=2021, month=7, day=1)
        person, season = self.create_cba3()
        buyout_data = buyout.get_data(person, season)
        buyout.apply_buyout(str(person), season, BUYOUT_DATE)
        contract = Contract.query.filter(Contract.person_id == person).first()
        self.assertEqual(contract.buyout_date, BUYOUT_DATE)

        contract_years = (
            ContractYear.query.filter(ContractYear.contract_id == contract.id)
            .order_by(ContractYear.season_id)
            .all()
        )
        self.assertIs(contract_years[0].bought_out, False)
        self.assertIs(contract_years[1].bought_out, True)
        self.assertIs(contract_years[2].bought_out, True)

        buyout_years = (
            BuyoutYear.query.filter(BuyoutYear.contract_id == contract.id)
            .order_by(BuyoutYear.season_id)
            .all()
        )
        self.assertTrue(buyout_years[0].cap_hit == 1416667)
        self.assertTrue(buyout_years[0].nhl_salary == 1416667)
        self.assertTrue(buyout_years[0].signing_bonus == 0)
        self.assertTrue(buyout_years[1].cap_hit == 1916667)
        self.assertTrue(buyout_years[1].nhl_salary == 1416667)
        self.assertTrue(buyout_years[1].signing_bonus == 0)
        self.assertTrue(buyout_years[2].cap_hit == 1416667)
        self.assertTrue(buyout_years[2].nhl_salary == 1416667)
        self.assertTrue(buyout_years[2].signing_bonus == 0)
        self.assertTrue(buyout_years[3].cap_hit == 1416667)
        self.assertTrue(buyout_years[3].nhl_salary == 1416667)
        self.assertTrue(buyout_years[3].signing_bonus == 0)

        self.assertEqual(Transaction.query.count(), 1)
        self.assertEqual(TransactionAsset.query.count(), 1)
        transaction = Transaction.query.get(1)
        tr_asset = TransactionAsset.query.get(1)
        contract = Contract.query.filter(Contract.person_id == person).first()
        asset = Asset.query.get(contract.id)
        self.assertEqual(transaction.type, "Bought out")
        self.assertEqual(transaction.date.date(), BUYOUT_DATE)
        self.assertEqual(tr_asset.asset_id, contract.id)
        self.assertEqual(tr_asset.new_team, asset.current_team)

    def test_apply_jared_cowen_buyout(self):
        person, season = self.create_jared_cowen()
        buyout_data = buyout.get_data(person, season)
        buyout.apply_buyout(str(person), season)
        contract = Contract.query.filter(Contract.person_id == person).first()

        contract_years = (
            ContractYear.query.filter(ContractYear.contract_id == contract.id)
            .order_by(ContractYear.season_id)
            .all()
        )
        self.assertIs(contract_years[0].bought_out, False)
        self.assertIs(contract_years[1].bought_out, False)
        self.assertIs(contract_years[2].bought_out, False)
        self.assertIs(contract_years[3].bought_out, True)

        buyout_years = (
            BuyoutYear.query.filter(BuyoutYear.contract_id == contract.id)
            .order_by(BuyoutYear.season_id)
            .all()
        )
        self.assertTrue(buyout_years[0].cap_hit == -650000)
        self.assertTrue(buyout_years[0].nhl_salary == 750000)
        self.assertTrue(buyout_years[0].signing_bonus == 0)
        self.assertTrue(buyout_years[1].cap_hit == 750000)
        self.assertTrue(buyout_years[1].nhl_salary == 750000)
        self.assertTrue(buyout_years[1].signing_bonus == 0)

        self.assertEqual(Transaction.query.count(), 1)
        self.assertEqual(TransactionAsset.query.count(), 1)
        transaction = Transaction.query.get(1)
        tr_asset = TransactionAsset.query.get(1)
        contract = Contract.query.filter(Contract.person_id == person).first()
        asset = Asset.query.get(contract.id)
        self.assertEqual(transaction.type, "Bought out")
        self.assertEqual(tr_asset.asset_id, contract.id)
        self.assertEqual(tr_asset.new_team, asset.current_team)

    def test_apply_zach_parise_buyout(self):
        person, season = self.create_zach_parise()
        buyout_data = buyout.get_data(person, season)
        buyout.apply_buyout(str(person), season)
        contract = Contract.query.filter(Contract.person_id == person).first()

        contract_years = (
            ContractYear.query.filter(ContractYear.contract_id == contract.id)
            .order_by(ContractYear.season_id)
            .all()
        )
        self.assertIs(contract_years[0].bought_out, False)
        self.assertIs(contract_years[1].bought_out, False)
        self.assertIs(contract_years[2].bought_out, False)
        self.assertIs(contract_years[3].bought_out, False)
        self.assertIs(contract_years[4].bought_out, False)
        self.assertIs(contract_years[5].bought_out, False)
        self.assertIs(contract_years[6].bought_out, False)
        self.assertIs(contract_years[7].bought_out, False)
        self.assertIs(contract_years[8].bought_out, False)
        self.assertIs(contract_years[9].bought_out, True)
        self.assertIs(contract_years[10].bought_out, True)
        self.assertIs(contract_years[11].bought_out, True)
        self.assertIs(contract_years[12].bought_out, True)

        buyout_years = (
            BuyoutYear.query.filter(BuyoutYear.contract_id == contract.id)
            .order_by(BuyoutYear.season_id)
            .all()
        )
        self.assertTrue(buyout_years[0].cap_hit - 2371794 in [1, 0, -1])
        self.assertTrue(buyout_years[0].nhl_salary == 833333)
        self.assertTrue(buyout_years[0].signing_bonus == 0)
        self.assertTrue(buyout_years[1].cap_hit - 6371794 in [1, 0, -1])
        self.assertTrue(buyout_years[1].nhl_salary == 833333)
        self.assertTrue(buyout_years[1].signing_bonus == 0)
        self.assertTrue(buyout_years[2].cap_hit - 7371794 in [1, 0, -1])
        self.assertTrue(buyout_years[2].nhl_salary == 833333)
        self.assertTrue(buyout_years[2].signing_bonus == 0)
        self.assertTrue(buyout_years[3].cap_hit - 7371794 in [1, 0, -1])
        self.assertTrue(buyout_years[3].nhl_salary == 833333)
        self.assertTrue(buyout_years[3].signing_bonus == 0)
        self.assertTrue(buyout_years[4].cap_hit == 833333)
        self.assertTrue(buyout_years[4].nhl_salary == 833333)
        self.assertTrue(buyout_years[4].signing_bonus == 0)
        self.assertTrue(buyout_years[5].cap_hit == 833333)
        self.assertTrue(buyout_years[5].nhl_salary == 833333)
        self.assertTrue(buyout_years[5].signing_bonus == 0)
        self.assertTrue(buyout_years[6].cap_hit == 833333)
        self.assertTrue(buyout_years[6].nhl_salary == 833333)
        self.assertTrue(buyout_years[6].signing_bonus == 0)
        self.assertTrue(buyout_years[7].cap_hit == 833333)
        self.assertTrue(buyout_years[7].nhl_salary == 833333)
        self.assertTrue(buyout_years[7].signing_bonus == 0)

        self.assertEqual(Transaction.query.count(), 1)
        self.assertEqual(TransactionAsset.query.count(), 1)
        transaction = Transaction.query.get(1)
        tr_asset = TransactionAsset.query.get(1)
        contract = Contract.query.filter(Contract.person_id == person).first()
        asset = Asset.query.get(contract.id)
        self.assertEqual(transaction.type, "Bought out")
        self.assertEqual(tr_asset.asset_id, contract.id)
        self.assertEqual(tr_asset.new_team, asset.current_team)

    def test_invalid_contract(self):
        self.assertFalse(buyout.set_buyout_date("not_an_integer", date.today()))
        self.assertFalse(buyout.set_buyout_date("not_an_integer"))

    def test_expired_buyout(self):
        person = Person(
            id=8000,
            name="Test Expired Contract",
            birthdate=datetime.strptime(
                f"1990-{random.randint(1,12):02}-{random.randint(1,28):02}",
                self.app.config["DATE_FMT"],
            ).date(),
        )
        team = Team(name="Expired")
        seasons = [
            Season(name="AAAAA"),
            Season(name="BBBBB"),
            Season(name="CCCCC"),
            Season(name="DDDDD"),
            Season(name="EEEEE"),
            Season(name="FFFFF"),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.add(person)
        db.session.add(team)
        db.session.commit()
        asset = Asset(
            current_team=random.randint(0, 32), originating_team=team.id, active=True
        )
        db.session.add(asset)
        db.session.commit()

        contract = Contract(
            person_id=person.id, id=asset.id, years=3, total_value=13500000
        )
        db.session.add(contract)
        db.session.commit()

        contract_years = [
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[0].id,
                aav=4500000,
                cap_hit=4500000,
                nhl_salary=5000000,
                minors_salary=5000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[1].id,
                aav=4500000,
                cap_hit=4500000,
                nhl_salary=4500000,
                minors_salary=4500000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[2].id,
                aav=4500000,
                cap_hit=4500000,
                nhl_salary=4000000,
                minors_salary=4000000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        buyout_data = buyout.get_data(person.id, seasons[3].name)
        keys = set(buyout_data.keys())
        self.assertTrue(keys == {"name", "error"})
        self.assertTrue(buyout_data["name"] == person.name)
        self.assertTrue(buyout_data["error"] == "Player's contract already expired")

    def test_buyout_form_seasons(self):
        current = self.app.config["CURRENT_SEASON"]
        seasons = [
            Season(
                id=current,
                name="AAAAA",
                free_agency_opening=date(year=2020, month=6, day=1),
            ),
            Season(
                id=current + 1,
                name="BBBBB",
                free_agency_opening=date(year=2020, month=6, day=2),
            ),
            Season(
                id=current + 2,
                name="CCCCC",
                free_agency_opening=date(year=2020, month=6, day=3),
            ),
            Season(
                id=current + 3,
                name="DDDDD",
                free_agency_opening=date(year=2020, month=6, day=4),
            ),
            Season(
                id=current + 4,
                name="EEEEE",
                free_agency_opening=date(year=2020, month=6, day=5),
            ),
            Season(
                id=current + 5,
                name="FFFFF",
                free_agency_opening=date(year=2020, month=6, day=6),
            ),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.commit()
        query = buyout.query_buyout_form_seasons()
        self.assertEqual(len(query), 6)
        self.assertEqual(query[0][0], "AAAAA")
        self.assertEqual(query[1][0], "BBBBB")
        self.assertEqual(query[2][0], "CCCCC")
        self.assertEqual(query[3][0], "DDDDD")
        self.assertEqual(query[4][0], "EEEEE")
        self.assertEqual(query[5][0], "FFFFF")
        query = buyout.get_buyout_form_seasons()
        self.assertEqual(len(query), 6)
        self.assertEqual(query, ["AAAAA", "BBBBB", "CCCCC", "DDDDD", "EEEEE", "FFFFF"])

        seasons = [
            Season(
                id=current + 6,
                name="IIIII",
                free_agency_opening=date(year=2020, month=6, day=7),
            ),
            Season(
                id=current + 7,
                name="HHHHH",
                free_agency_opening=date(year=2020, month=6, day=8),
            ),
            Season(
                id=current + 8,
                name="GGGGG",
                free_agency_opening=date(year=2020, month=6, day=9),
            ),
            Season(
                id=current + 9,
                name="ABABA",
                free_agency_opening=date(year=2020, month=6, day=10),
            ),
            Season(
                id=current + 10,
                name="BABAB",
                free_agency_opening=date(year=2020, month=6, day=15),
            ),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.commit()
        query = buyout.query_buyout_form_seasons()
        self.assertEqual(len(query), 9)
        self.assertEqual(query[0][0], "AAAAA")
        self.assertEqual(query[1][0], "BBBBB")
        self.assertEqual(query[2][0], "CCCCC")
        self.assertEqual(query[3][0], "DDDDD")
        self.assertEqual(query[4][0], "EEEEE")
        self.assertEqual(query[5][0], "FFFFF")
        self.assertEqual(query[6][0], "IIIII")
        self.assertEqual(query[7][0], "HHHHH")
        self.assertEqual(query[8][0], "GGGGG")
        query = buyout.get_buyout_form_seasons()
        self.assertEqual(len(query), 9)
        self.assertEqual(
            query,
            [
                "AAAAA",
                "BBBBB",
                "CCCCC",
                "DDDDD",
                "EEEEE",
                "FFFFF",
                "IIIII",
                "HHHHH",
                "GGGGG",
            ],
        )

    def test_buyout_candidates(self):
        person_one = Person(name="Test One")
        person_two = Person(name="Test Two")
        db.session.add(person_one)
        db.session.add(person_two)
        db.session.flush()
        asset_one = Asset(current_team=5)
        asset_two = Asset(current_team=7)
        db.session.add(asset_one)
        db.session.add(asset_two)
        db.session.flush()
        contract_one = Contract(id=asset_one.id, person_id=person_one.id)
        contract_two = Contract(id=asset_two.id, person_id=person_two.id)
        db.session.add(contract_one)
        db.session.add(contract_two)
        db.session.commit()

        query = buyout.query_buyout_candidates()
        self.assertEqual(len(query), 2)
        self.assertEqual(query[0][0], person_one.id)
        self.assertEqual(query[0][1], "Test One")
        self.assertEqual(query[1][0], person_two.id)
        self.assertEqual(query[1][1], "Test Two")
        query = buyout.query_buyout_candidates(team_id=5)
        self.assertEqual(len(query), 1)
        self.assertEqual(query[0][0], person_one.id)
        self.assertEqual(query[0][1], "Test One")

        query = buyout.get_buyout_candidates()
        self.assertEqual(len(query), 2)
        self.assertEqual(query[0][0], str(person_one.id))
        self.assertEqual(query[0][1], "Test One")
        self.assertEqual(query[1][0], str(person_two.id))
        self.assertEqual(query[1][1], "Test Two")
        query = buyout.get_buyout_candidates(team_id=5)
        self.assertEqual(len(query), 1)
        self.assertEqual(len(query[0]), 1)
        self.assertEqual(query[0][0], str(person_one.id))

    def test_get_contract_id(self):
        person = Person(name="Test")
        asset = Asset()
        db.session.add(person)
        db.session.add(asset)
        db.session.commit()
        self.assertIsNone(buyout.get_contract_id(person.id))

        contract = Contract(person_id=person.id, id=asset.id)
        db.session.add(contract)
        db.session.commit()
        self.assertEqual(buyout.get_contract_id(person.id), contract.id)

    def test_get_seasons(self):
        asset = Asset(id=4)
        contract = Contract(id=asset.id)
        db.session.add(asset)
        db.session.add(contract)
        seasons = [
            Season(id=self.app.config["CURRENT_SEASON"], name="Test One"),
            Season(id=self.app.config["CURRENT_SEASON"] + 1, name="Test Two"),
            Season(id=self.app.config["CURRENT_SEASON"] + 2, name="Test Three"),
            Season(id=self.app.config["CURRENT_SEASON"] + 3, name="Test Four"),
            Season(id=self.app.config["CURRENT_SEASON"] + 4, name="Test Five"),
            Season(id=self.app.config["CURRENT_SEASON"] + 5, name="Test Six"),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.flush()
        contract_years = [
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[0].id,
                aav=1,
                cap_hit=2,
                nhl_salary=3,
                minors_salary=4,
                signing_bonus=5,
                performance_bonuses=6,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[1].id,
                aav=2,
                cap_hit=3,
                nhl_salary=4,
                minors_salary=5,
                signing_bonus=6,
                performance_bonuses=7,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[2].id,
                aav=3,
                cap_hit=4,
                nhl_salary=5,
                minors_salary=6,
                signing_bonus=7,
                performance_bonuses=8,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[3].id,
                aav=4,
                cap_hit=5,
                nhl_salary=6,
                minors_salary=7,
                signing_bonus=8,
                performance_bonuses=9,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[4].id,
                aav=5,
                cap_hit=6,
                nhl_salary=7,
                minors_salary=8,
                signing_bonus=9,
                performance_bonuses=0,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[5].id,
                aav=6,
                cap_hit=7,
                nhl_salary=8,
                minors_salary=9,
                signing_bonus=0,
                performance_bonuses=1,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()

        query = buyout.get_seasons(self.app.config["CURRENT_SEASON"] + 1, contract.id)
        self.assertEqual(len(query), 4)
        self.assertEqual(query[0]["season"], seasons[2].name)
        self.assertEqual(query[0]["season_id"], seasons[2].id)
        self.assertEqual(query[0]["id"], contract_years[2].id)
        self.assertEqual(query[1]["season"], seasons[3].name)
        self.assertEqual(query[1]["season_id"], seasons[3].id)
        self.assertEqual(query[1]["id"], contract_years[3].id)
        self.assertEqual(query[2]["season"], seasons[4].name)
        self.assertEqual(query[2]["season_id"], seasons[4].id)
        self.assertEqual(query[2]["id"], contract_years[4].id)
        self.assertEqual(query[3]["season"], seasons[5].name)
        self.assertEqual(query[3]["season_id"], seasons[5].id)
        self.assertEqual(query[3]["id"], contract_years[5].id)

        self.assertEqual(query[0]["cap_hit"], 4)
        self.assertEqual(query[1]["cap_hit"], 5)
        self.assertEqual(query[2]["cap_hit"], 6)
        self.assertEqual(query[3]["cap_hit"], 7)
        self.assertEqual(query[0]["aav"], 3)
        self.assertEqual(query[1]["aav"], 4)
        self.assertEqual(query[2]["aav"], 5)
        self.assertEqual(query[3]["aav"], 6)
        self.assertEqual(query[0]["nhl_salary"], 5)
        self.assertEqual(query[1]["nhl_salary"], 6)
        self.assertEqual(query[2]["nhl_salary"], 7)
        self.assertEqual(query[3]["nhl_salary"], 8)
        self.assertEqual(query[0]["minors_salary"], 6)
        self.assertEqual(query[1]["minors_salary"], 7)
        self.assertEqual(query[2]["minors_salary"], 8)
        self.assertEqual(query[3]["minors_salary"], 9)
        self.assertEqual(query[0]["signing_bonus"], 7)
        self.assertEqual(query[1]["signing_bonus"], 8)
        self.assertEqual(query[2]["signing_bonus"], 9)
        self.assertEqual(query[3]["signing_bonus"], 0)
        self.assertEqual(query[0]["performance_bonuses"], 8)
        self.assertEqual(query[1]["performance_bonuses"], 9)
        self.assertEqual(query[2]["performance_bonuses"], 0)
        self.assertEqual(query[3]["performance_bonuses"], 1)

    def test_get_season_id(self):
        self.assertEqual(buyout.get_season_id(None), self.app.config["CURRENT_SEASON"])
        season = Season(name="Test", id=5)
        db.session.add(season)
        db.session.commit()
        self.assertEqual(buyout.get_season_id("Test"), 5)
        self.assertEqual(buyout.get_season_id(5), self.app.config["CURRENT_SEASON"])

    def test_buyout_season_info(self):
        seasons = [
            Season(name="One", free_agency_opening=date(year=2020, month=1, day=1)),
            Season(name="Two", free_agency_opening=date(year=2020, month=2, day=1)),
            Season(name="Three", free_agency_opening=date(year=2020, month=3, day=1)),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.commit()
        query = buyout.query_buyout_seasons("Two", 7)
        self.assertEqual(len(query), 1)
        self.assertEqual(query[0][0], "Three")
        self.assertEqual(query[0][1], seasons[2].id)

        query = buyout.get_buyout_season_info("Two", 7)
        self.assertEqual(len(query), 1)
        self.assertEqual(query[0]["name"], "Three")
        self.assertEqual(query[0]["id"], seasons[2].id)

    def test_get_url_person(self):
        person = Person(id=7, nhl_id=8765432, name="Test One")
        other_person = Person(name="Test Two")
        db.session.add(person)
        db.session.add(other_person)
        db.session.commit()
        data = buyout.get_url_person("8765432")
        self.assertEqual(data[1], "NHLID")
        self.assertEqual(data[0], person)

        data = buyout.get_url_person("7")
        self.assertEqual(data[1], "ID")
        self.assertEqual(data[0], person)

        data = buyout.get_url_person("One")
        self.assertEqual(data[1], "NAMESTUB")
        self.assertEqual(data[0], person)

        data = buyout.get_url_person("Test One")
        self.assertEqual(data[1], "NAME")
        self.assertEqual(data[0], person)

        try:
            buyout.get_url_person("Test")
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(
                e.description,
                "This URL could refer to multiple people. Please be more specific",
            )

        try:
            buyout.get_url_person("no existe")
        except NotFound as e:
            self.assertEqual(e.code, 404)

    def test_deactivate_asset(self):
        asset = Asset()
        db.session.add(asset)
        db.session.add(Asset())
        db.session.add(Asset())
        db.session.commit()

        self.assertTrue(buyout.deactivate_asset(asset.id))
        self.assertFalse(asset.active)
        db.session.commit()

    def test_set_buyout_date(self):
        db.session.add(Asset(id=1))
        db.session.add(Asset(id=2))
        db.session.add(Asset(id=3))
        db.session.add(Contract(id=1))
        contract = Contract(id=2)
        db.session.add(contract)
        db.session.add(Contract(id=3))
        db.session.commit()
        self.assertTrue(buyout.set_buyout_date(contract.id))
        db.session.commit()
        self.assertEqual(contract.buyout_date, date.today())

        y2k = date(year=1999, month=12, day=31)
        self.assertTrue(buyout.set_buyout_date(contract.id, y2k))
        db.session.commit()
        self.assertEqual(contract.buyout_date, y2k)

    def test_mark_years_bought_out(self):
        asset = Asset(id=3)
        contract = Contract(id=3)
        db.session.add(asset)
        db.session.add(contract)
        contract_years = [
            ContractYear(contract_id=3),
            ContractYear(contract_id=3),
            ContractYear(contract_id=3),
            ContractYear(contract_id=3),
            ContractYear(contract_id=3),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        ids = [contract_years[0].id, contract_years[2].id, contract_years[3].id]
        self.assertEqual(buyout.mark_years_bought_out(ids), 3)
        db.session.commit()
        self.assertTrue(contract_years[0].bought_out)
        self.assertFalse(contract_years[1].bought_out)
        self.assertTrue(contract_years[2].bought_out)
        self.assertTrue(contract_years[3].bought_out)
        self.assertFalse(contract_years[4].bought_out)

    def test_create_buyout_years(self):
        asset = Asset(id=8)
        contract = Contract(id=8)
        db.session.add(asset)
        db.session.add(contract)
        db.session.commit()
        buyout_seasons = [
            {
                "season_id": 1,
                "signing_bonus": 9,
                "buyout_pay": 14,
                "buyout_cap_hit": 73,
            },
            {
                "season_id": 2,
                "signing_bonus": 8,
                "buyout_pay": 25,
                "buyout_cap_hit": 84,
            },
            {
                "season_id": 3,
                "signing_bonus": 7,
                "buyout_pay": 36,
                "buyout_cap_hit": 95,
            },
        ]
        buyout.create_buyout_years(contract.id, buyout_seasons)
        db.session.commit()
        buyout_years = BuyoutYear.query.order_by(BuyoutYear.season_id).all()
        self.assertEqual(buyout_years[0].contract_id, 8)
        self.assertEqual(buyout_years[1].contract_id, 8)
        self.assertEqual(buyout_years[2].contract_id, 8)
        self.assertEqual(buyout_years[0].season_id, 1)
        self.assertEqual(buyout_years[1].season_id, 2)
        self.assertEqual(buyout_years[2].season_id, 3)
        self.assertEqual(buyout_years[0].signing_bonus, 9)
        self.assertEqual(buyout_years[1].signing_bonus, 8)
        self.assertEqual(buyout_years[2].signing_bonus, 7)
        self.assertEqual(buyout_years[0].nhl_salary, 14)
        self.assertEqual(buyout_years[1].nhl_salary, 25)
        self.assertEqual(buyout_years[2].nhl_salary, 36)
        self.assertEqual(buyout_years[0].cap_hit, 73)
        self.assertEqual(buyout_years[1].cap_hit, 84)
        self.assertEqual(buyout_years[2].cap_hit, 95)


if __name__ == "__main__":
    unittest.main(verbosity=2)
