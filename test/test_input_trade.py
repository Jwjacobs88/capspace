import sys
import json
import random
import unittest
from datetime import datetime, date
from werkzeug.exceptions import InternalServerError
from app import create_app, db
from app.models import (
    Team,
    Season,
    Person,
    Asset,
    DraftPick,
    Contract,
    ContractYear,
    Transaction,
    TransactionAsset,
    RetainedSalary,
    TeamSeason,
    SigningRights,
    Note,
)

from app.admin import input_trade as trade
from app.admin.input_trade import TradeProposal

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class InputTradeTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def create_dummy_assets(self, trade_data):
        """
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "future_considerations_one": False,
            "future_considerations_two": False,
            "contracts_one": [{"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(1, 10)],
            "contracts_two": [{"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(11, 20)],
            "picks_one": [{"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)],
            "picks_two": [{"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)],
        }
        """
        season_id = self.app.config["CURRENT_SEASON"]
        db.session.add(Season(id=season_id, salary_cap=1000, salary_floor=10))
        db.session.add(
            Team(
                id=trade_data["team_one"],
                name="Test One",
                abbreviation="T01",
                logo="lol",
            )
        )
        db.session.add(
            Team(
                id=trade_data["team_two"],
                name="Test Two",
                abbreviation="T02",
                logo="lol2",
            )
        )
        db.session.add(
            TeamSeason(
                team_id=trade_data["team_one"],
                season_id=season_id,
                cap_hit=10,
                projected_cap_hit=10,
            )
        )
        db.session.add(
            TeamSeason(
                team_id=trade_data["team_two"],
                season_id=season_id,
                cap_hit=10,
                projected_cap_hit=10,
            )
        )
        db.session.add(Season(id=0, name="Test Szn", draft_date=date.today()))
        db.session.add(Person(id=0, name="Test Hmn"))
        for pick in trade_data["picks_one"]:
            db.session.add(
                Asset(
                    id=pick["id"],
                    originating_team=pick["fmr"],
                    current_team=pick["fmr"],
                )
            )
            db.session.add(DraftPick(id=pick["id"], season_id=0, round=5))
        for pick in trade_data["picks_two"]:
            db.session.add(
                Asset(
                    id=pick["id"],
                    originating_team=pick["fmr"],
                    current_team=pick["fmr"],
                )
            )
            db.session.add(DraftPick(id=pick["id"], season_id=0, round=8))
        for contract in trade_data["contracts_one"]:
            db.session.add(
                Asset(
                    id=contract["id"],
                    originating_team=contract["fmr"],
                    current_team=contract["fmr"],
                )
            )
            db.session.add(Contract(id=contract["id"], person_id=0))
            db.session.add(
                ContractYear(
                    contract_id=contract["id"],
                    season_id=season_id,
                    aav=12,
                    cap_hit=13,
                    nhl_salary=14,
                )
            )
        for contract in trade_data["contracts_two"]:
            db.session.add(
                Asset(
                    id=contract["id"],
                    originating_team=contract["fmr"],
                    current_team=contract["fmr"],
                )
            )
            db.session.add(Contract(id=contract["id"], person_id=0))
            db.session.add(
                ContractYear(
                    contract_id=contract["id"],
                    season_id=season_id,
                    aav=121,
                    cap_hit=113,
                    nhl_salary=142,
                )
            )
        db.session.commit()

    def create_team_and_assets(
        self, team_name, team_abbreviation, contracts=True, picks=True
    ):
        seasons = [
            Season(name="AAAAA"),
            Season(name="BBBBB"),
            Season(name="CCCCC"),
            Season(name="DDDDD"),
            Season(name="EEEEE"),
            Season(name="FFFFF"),
            Season(name="GGGGG"),
            Season(name="HHHHH"),
        ]
        for season in seasons:
            db.session.add(season)

        team = Team(name=team_name, abbreviation=team_abbreviation)
        db.session.add(team)
        db.session.commit()

        data = {
            "team": team.id,
        }
        if contracts:
            data["contracts"] = [
                self.create_team_contract(team.id) for _ in range(random.randint(5, 20))
            ]
            data["contracts"].sort(key=lambda contract: contract["name"])
        if picks:
            draft_year = 2000
            for season in seasons:
                draft_year = random.randint(draft_year, 100 + draft_year)
                season.draft_date = date(
                    draft_year,
                    random.randint(
                        1,
                        12,
                    ),
                    random.randint(1, 28),
                )
                db.session.add(season)
            db.session.commit()
            data["picks"] = [
                self.create_team_pick(
                    team.id, team_abbreviation, seasons[idx % len(seasons)]
                )
                for idx in range(random.randint(5, 20))
            ]
            data["picks"].sort(key=lambda pick: (pick["year"], pick["round"]))

        return data

    def create_team_contract(self, team_id):
        # Create person
        person_name = "".join(random.choices(LETTERS, k=16))
        person = Person(name=person_name)
        db.session.add(person)
        asset = Asset(
            originating_team=team_id,
            current_team=team_id,
            type="Contract",
        )
        db.session.add(asset)
        db.session.commit()

        # Create contract
        #   person_id, team_id, expires_as
        contract = Contract(
            id=asset.id,
            person_id=person.id,
            expiration_status=random.choice(["UFA", "RFA"]),
        )
        db.session.add(contract)
        db.session.commit()

        # Create contract years
        #   contract_id, cap_hit, nhl_salary, season_id
        years_remaining = random.randint(1, 8)
        cap_hit = random.randint(750000, 10000000)
        nhl_salary = random.randint(750000, 10000000)
        aav = random.randint(750000, 10000000)
        for season_id in range(1, years_remaining + 1):
            contract_year = ContractYear(
                contract_id=contract.id,
                cap_hit=cap_hit,
                nhl_salary=nhl_salary,
                season_id=season_id,
                aav=aav,
            )
            db.session.add(contract_year)
        db.session.commit()

        data = {
            "id": asset.id,
            "name": person.name,
            "expires_as": contract.expiration_status,
            "years": years_remaining,
            "cap_hit": cap_hit,
            "nhl_salary": nhl_salary,
            "aav": aav,
        }
        return data

    def create_team_pick(self, team_id, team_abb, season):
        draft_round = random.randint(1, 7)
        position = random.choice([True, False, False, False, False, False])
        draft_position = None
        if position:
            draft_position = random.randint(1, 32) + draft_round * 32

        asset = Asset(originating_team=team_id, current_team=team_id)
        db.session.add(asset)
        db.session.commit()

        draft_pick = DraftPick(
            id=asset.id, season_id=season.id, round=draft_round, position=draft_position
        )
        db.session.add(draft_pick)
        db.session.commit()

        return {
            "id": asset.id,
            "team_id": team_id,
            "year": season.draft_date.year,
            "round": draft_round,
            "position": draft_position,
            "team": team_abb,
        }

    # Form checks
    def test_valid_json_valid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()
        form.contracts_one.data = json.dumps({})
        form.contracts_two.data = json.dumps({"test_key": "test_value"})
        form.picks_one.data = json.dumps({5: "number key"})
        form.picks_two.data = json.dumps({"text_key": 5})

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)
        form.picks_one.errors = list(form.picks_one.errors)
        form.picks_two.errors = list(form.picks_two.errors)

        self.assertTrue(form.valid_json(form.contracts_one))
        self.assertTrue(form.valid_json(form.contracts_two))
        self.assertTrue(form.valid_json(form.picks_one))
        self.assertTrue(form.valid_json(form.picks_two))
        self.assertTrue(form.contracts_one.errors == [])
        self.assertTrue(form.contracts_two.errors == [])
        self.assertTrue(form.picks_one.errors == [])
        self.assertTrue(form.picks_two.errors == [])

        ctx.pop()

    def test_valid_json_invalid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()
        form.contracts_one.data = "{'test:' 1}"
        form.contracts_two.data = "{\"test\": '5'}"
        form.picks_one.data = "[one, two]"
        form.picks_two.data = "[None]"

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)
        form.picks_one.errors = list(form.picks_one.errors)
        form.picks_two.errors = list(form.picks_two.errors)

        self.assertTrue(not form.valid_json(form.contracts_one))
        self.assertTrue(not form.valid_json(form.contracts_two))
        self.assertTrue(not form.valid_json(form.picks_one))
        self.assertTrue(not form.valid_json(form.picks_two))
        self.assertTrue(form.contracts_one.errors == ["Incorrectly formed JSON object"])
        self.assertTrue(form.contracts_two.errors == ["Incorrectly formed JSON object"])
        self.assertTrue(form.picks_one.errors == ["Incorrectly formed JSON object"])
        self.assertTrue(form.picks_two.errors == ["Incorrectly formed JSON object"])

        form2 = TradeProposal()
        form2.contracts_one.data = 5
        form2.contracts_two.data = ("An", "Tuple")
        form2.picks_one.data = json.loads
        form2.picks_two.data = form

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form2.contracts_one.errors = list(form2.contracts_one.errors)
        form2.contracts_two.errors = list(form2.contracts_two.errors)
        form2.picks_one.errors = list(form2.picks_one.errors)
        form2.picks_two.errors = list(form2.picks_two.errors)

        self.assertFalse(form2.valid_json(form2.contracts_one))
        self.assertFalse(form2.valid_json(form2.contracts_two))
        self.assertFalse(form2.valid_json(form2.picks_one))
        self.assertFalse(form2.valid_json(form2.picks_two))
        self.assertTrue(
            form2.contracts_one.errors
            == ["JSON variable type must be str, bytes or bytearray"]
        )
        self.assertTrue(
            form2.contracts_two.errors
            == ["JSON variable type must be str, bytes or bytearray"]
        )
        self.assertTrue(
            form2.picks_one.errors
            == ["JSON variable type must be str, bytes or bytearray"]
        )
        self.assertTrue(
            form2.picks_two.errors
            == ["JSON variable type must be str, bytes or bytearray"]
        )

        ctx.pop()

    def test_check_contracts_valid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        assets = self.create_team_and_assets("Test Team", "TST")
        contract_ids = [{"id": contract["id"]} for contract in assets["contracts"]]
        trade_contracts_one = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        trade_contracts_two = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )

        # Set form.contracts_{one,two}
        form.contracts_one.data = json.dumps(trade_contracts_one)
        form.contracts_two.data = json.dumps(trade_contracts_two)

        self.assertTrue(form.check_contracts(assets["team"], form.contracts_one))
        self.assertTrue(form.check_contracts(assets["team"], form.contracts_two))

        ctx.pop()

    def test_check_contracts_invalid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        assets = self.create_team_and_assets("Test Team", "TST")
        contract_ids = [{"id": contract["id"]} for contract in assets["contracts"]]
        trade_contracts_one = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        trade_contracts_two = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        trade_contracts_two.append({"id": 10000})
        trade_contracts_one.sort(key=lambda contract: str(contract["id"]))
        trade_contracts_two.sort(key=lambda contract: str(contract["id"]))

        # Set form.contracts_{one,two}
        form.contracts_one.data = json.dumps(trade_contracts_one)
        form.contracts_two.data = json.dumps(trade_contracts_two)

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)

        self.assertFalse(form.check_contracts(2, form.contracts_one))
        self.assertFalse(form.check_contracts(assets["team"], form.contracts_two))

        form.contracts_one.errors.sort()
        form.contracts_two.errors.sort()
        for idx in range(len(trade_contracts_one)):
            error = form.contracts_one.errors[idx]
            message = f"Contract {trade_contracts_one[idx]['id']} not owned by team 2"
            self.assertTrue(error == message)
        self.assertTrue(
            form.contracts_two.errors == ["Contract 10000 not owned by team 1"]
        )

        ctx.pop()

    def test_check_picks_valid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        assets = self.create_team_and_assets("Test Team", "TST")
        pick_ids = [{"id": pick["id"]} for pick in assets["picks"]]
        trade_picks_one = random.sample(pick_ids, random.randint(1, len(pick_ids)))
        trade_picks_two = random.sample(pick_ids, random.randint(1, len(pick_ids)))

        # Set form.picks_{one,two}
        form.picks_one.data = json.dumps(trade_picks_one)
        form.picks_two.data = json.dumps(trade_picks_two)

        self.assertTrue(form.check_picks(assets["team"], form.picks_one))
        self.assertTrue(form.check_picks(assets["team"], form.picks_two))

        ctx.pop()

    def test_check_picks_invalid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        assets = self.create_team_and_assets("Test Team", "TST")
        pick_ids = [{"id": pick["id"]} for pick in assets["picks"]]
        trade_picks_one = random.sample(pick_ids, random.randint(1, len(pick_ids)))
        trade_picks_two = random.sample(pick_ids, random.randint(1, len(pick_ids)))
        trade_picks_two.append({"id": 10000})
        trade_picks_one.sort(key=lambda pick: str(pick["id"]))
        trade_picks_two.sort(key=lambda pick: str(pick["id"]))

        # Set form.picks_{one,two}
        form.picks_one.data = json.dumps(trade_picks_one)
        form.picks_two.data = json.dumps(trade_picks_two)

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.picks_one.errors = list(form.picks_one.errors)
        form.picks_two.errors = list(form.picks_two.errors)

        self.assertFalse(form.check_picks(2, form.picks_one))
        self.assertFalse(form.check_picks(assets["team"], form.picks_two))

        form.picks_one.errors.sort()
        form.picks_two.errors.sort()
        for idx in range(len(trade_picks_one)):
            error = form.picks_one.errors[idx]
            message = f"Pick {trade_picks_one[idx]['id']} not owned by team 2"
            self.assertTrue(error == message)
        self.assertTrue(form.picks_two.errors == ["Pick 10000 not owned by team 1"])

        ctx.pop()

    def test_check_contract_count_valid_below_limit(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        data = self.create_team_and_assets("Test Team 1", "TT1")
        ts = TeamSeason(
            season_id=self.app.config["CURRENT_SEASON"],
            team_id=data["team"],
            contract_count=30,
        )
        db.session.add(ts)
        db.session.commit()

        # Set contracts to trade
        contract_ids = [{"id": contract["id"]} for contract in data["contracts"]]
        trade_contracts_one = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        trade_contracts_two = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        form.contracts_one.data = json.dumps(trade_contracts_one)
        form.contracts_two.data = json.dumps(trade_contracts_two)
        form.team_one.data = data["team"]
        form.team_two.data = data["team"]

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)

        self.assertTrue(form.check_contract_count())
        self.assertTrue(form.contracts_one.errors == [])
        self.assertTrue(form.contracts_two.errors == [])

        ctx.pop()

    def test_check_contract_count_valid_at_limit(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        data = self.create_team_and_assets("Test Team", "TST")
        while len(data["contracts"]) < 50:
            data["contracts"].append(self.create_team_contract(data["team"]))
        data["contracts"].sort(key=lambda contract: contract["name"])

        # Contracts to trade
        contract_ids = [{"id": contract["id"]} for contract in data["contracts"]]
        sample_size = random.randint(1, len(contract_ids))
        contracts_one = random.sample(contract_ids, sample_size)
        contracts_two = random.sample(contract_ids, sample_size)
        form.contracts_one.data = json.dumps(contracts_one)
        form.contracts_two.data = json.dumps(contracts_two)
        form.team_one.data = data["team"]
        form.team_two.data = data["team"]

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)

        self.assertTrue(form.check_contract_count())
        self.assertTrue(form.contracts_one.errors == [])
        self.assertTrue(form.contracts_two.errors == [])

        ctx.pop()

    def test_check_contract_count_invalid(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        data = self.create_team_and_assets("Test Team", "TST")
        while len(data["contracts"]) < 50:
            data["contracts"].append(self.create_team_contract(data["team"]))
        data["contracts"].sort(key=lambda contract: contract["name"])
        ts = TeamSeason(
            season_id=self.app.config["CURRENT_SEASON"],
            team_id=data["team"],
            contract_count=len(data["contracts"]),
        )
        db.session.add(ts)
        db.session.commit()

        # Contracts to trade
        contract_ids = [{"id": contract["id"]} for contract in data["contracts"]]
        sample_size = random.randint(1, len(contract_ids) - 2)
        contracts_one = random.sample(contract_ids, sample_size)
        contracts_two = random.sample(contract_ids, sample_size + 2)

        form.team_one.data = data["team"]
        form.team_two.data = data["team"]
        form.contracts_one.data = json.dumps(contracts_one)
        form.contracts_two.data = json.dumps(contracts_two)

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)

        self.assertFalse(form.check_contract_count())
        self.assertTrue(form.contracts_two.errors == [])
        self.assertTrue(
            form.contracts_one.errors
            == ["Exceeded 50 contract count. More contracts need to be traded away"]
        )

        # two forms. each side exceeds 50 at one point or another
        form2 = TradeProposal()
        form2.team_one.data = data["team"]
        form2.team_two.data = data["team"]
        form2.contracts_two.data = json.dumps(contracts_one)
        form2.contracts_one.data = json.dumps(contracts_two)

        # WTForms Field.validate() starts with the line
        #   self.errors = list(self.process_errors)
        form2.contracts_one.errors = list(form2.contracts_one.errors)
        form2.contracts_two.errors = list(form2.contracts_two.errors)

        self.assertFalse(form2.check_contract_count())
        self.assertTrue(form2.contracts_one.errors == [])
        self.assertTrue(
            form2.contracts_two.errors
            == ["Exceeded 50 contract count. More contracts need to be traded away"]
        )

        ctx.pop()

    def test_check_retained(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()

        team_one = 7
        season_id = self.app.config["CURRENT_SEASON"]

        db.session.add(
            TeamSeason(
                team_id=team_one,
                season_id=season_id,
                retained_contracts=2,
            )
        )

        form.team_one.data = str(team_one)
        form.contracts_one.data = json.dumps(
            [{"id": 444, "retention": "0"}, {"id": 234, "retention": "33"}]
        )
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.check_retained(form.contracts_one, form.team_one)
        self.assertEqual(form.contracts_one.errors, [])

        form.contracts_one.data = json.dumps(
            [
                {"id": 444, "retention": 0},
                {"id": 234, "retention": 33},
                {"id": 1, "retention": 3},
            ]
        )
        form.check_retained(form.contracts_one, form.team_one)
        self.assertEqual(
            form.contracts_one.errors,
            ["Team 7 cannot retain more than three contracts"],
        )

        ctx.pop()

    def test_check_previously_retained_form(self):
        team_one = 5
        team_two = 8
        assets = [2, 3, 4, 5, 6, 7]

        ctx = self.app.test_request_context()
        ctx.push()
        form = TradeProposal()
        form.team_one.data = str(team_one)
        form.team_two.data = str(team_two)
        form.contracts_one.data = json.dumps([{"id": 2}, {"id": 3}, {"id": 4}])
        form.contracts_two.data = json.dumps([{"id": 5}, {"id": 6}, {"id": 7}])
        form.contracts_one.errors = list(form.contracts_one.errors)
        form.contracts_two.errors = list(form.contracts_two.errors)

        self.assertTrue(form.check_previously_retained())
        db.session.add(Asset(id=1))
        db.session.add(Asset(id=2))
        db.session.add(Asset(id=3))
        db.session.add(Asset(id=4))
        db.session.add(Asset(id=5))
        db.session.add(Asset(id=6))
        db.session.add(
            RetainedSalary(
                id=1,
                retained_by=team_two,
                contract_id=assets[0],
            )
        )
        db.session.add(
            RetainedSalary(
                id=2,
                retained_by=team_two,
                contract_id=assets[1],
            )
        )
        db.session.add(
            RetainedSalary(
                id=3,
                retained_by=team_two,
                contract_id=assets[2],
            )
        )
        db.session.add(
            RetainedSalary(
                id=4,
                retained_by=team_one,
                contract_id=assets[3],
            )
        )
        db.session.add(
            RetainedSalary(
                id=5,
                retained_by=team_one,
                contract_id=assets[4],
            )
        )
        db.session.add(
            RetainedSalary(
                id=6,
                retained_by=team_one,
                contract_id=assets[5],
            )
        )
        db.session.commit()
        self.assertFalse(form.check_previously_retained())
        self.assertEqual(len(form.contracts_one.errors), 1)
        self.assertEqual(
            form.contracts_one.errors[0],
            f"Team {team_one} cannot reacquire a previously retained contract",
        )
        self.assertEqual(len(form.contracts_two.errors), 1)
        self.assertEqual(
            form.contracts_two.errors[0],
            f"Team {team_two} cannot reacquire a previously retained contract",
        )

        ctx.pop()

    # Form checks just from trade.py
    def test_inner_check_contracts_valid(self):
        data = self.create_team_and_assets("Test Team", "TST")
        contract_ids = [{"id": contract["id"]} for contract in data["contracts"]]
        contracts_one = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        contracts_two = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        self.assertTrue(trade.check_contracts(data["team"], contracts_one) == [])
        self.assertTrue(trade.check_contracts(data["team"], contracts_two) == [])

    def test_inner_check_contracts_invalid(self):
        data = self.create_team_and_assets("Test Team", "TST")
        contract_ids = [{"id": contract["id"]} for contract in data["contracts"]]
        contracts_one = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )
        contracts_two = random.sample(
            contract_ids, random.randint(1, len(contract_ids))
        )

        contracts_one.append({"id": 120})
        contracts_two.append({"id": 104})
        contracts_two.append({"id": 107})
        contracts_one_errors = [f"Contract 120 not owned by team {data['team']}"]
        contracts_two_errors = [
            f"Contract 104 not owned by team {data['team']}",
            f"Contract 107 not owned by team {data['team']}",
        ]
        self.assertTrue(
            sorted(trade.check_contracts(data["team"], contracts_one))
            == contracts_one_errors
        )
        self.assertTrue(
            sorted(trade.check_contracts(data["team"], contracts_two))
            == contracts_two_errors
        )

    def test_inner_check_picks_valid(self):
        data = self.create_team_and_assets("Test Team", "TST")
        pick_ids = [{"id": pick["id"]} for pick in data["picks"]]
        picks_one = random.sample(pick_ids, random.randint(1, len(pick_ids)))
        picks_two = random.sample(pick_ids, random.randint(1, len(pick_ids)))
        self.assertTrue(trade.check_picks(data["team"], picks_one) == [])
        self.assertTrue(trade.check_picks(data["team"], picks_two) == [])

    def test_inner_check_picks_invalid(self):
        data = self.create_team_and_assets("Test Team", "TST")
        pick_ids = [{"id": pick["id"]} for pick in data["picks"]]
        picks_one = random.sample(pick_ids, random.randint(1, len(pick_ids)))
        picks_two = random.sample(pick_ids, random.randint(1, len(pick_ids)))

        picks_one.append({"id": 120})
        picks_two.append({"id": 104})
        picks_two.append({"id": 107})
        picks_one_errors = [f"Pick 120 not owned by team {data['team']}"]
        picks_two_errors = [
            f"Pick 104 not owned by team {data['team']}",
            f"Pick 107 not owned by team {data['team']}",
        ]
        self.assertTrue(
            sorted(trade.check_picks(data["team"], picks_one)) == picks_one_errors
        )
        self.assertTrue(
            sorted(trade.check_picks(data["team"], picks_two)) == picks_two_errors
        )

    def test_create_transaction_picks(self):
        TRANSACTION_ID = 7
        TEAM_ONE = 1
        TEAM_TWO = 2
        picks_one = [
            {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)
        ]
        picks_two = [
            {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)
        ]
        for idx in range(len(picks_one)):
            if random.choice([True, False, False, False]):
                picks_one[idx]["condition"] = "".join(random.choices(LETTERS, k=16))
                picks_two[idx]["condition"] = "".join(random.choices(LETTERS, k=16))
        trade.create_transaction_picks(TRANSACTION_ID, picks_one, TEAM_ONE, TEAM_TWO)
        trade.create_transaction_picks(TRANSACTION_ID, picks_two, TEAM_TWO, TEAM_ONE)
        # Create dummy data around picks
        queried_one = (
            TransactionAsset.query.filter(
                TransactionAsset.transaction_id == TRANSACTION_ID
            )
            .filter(TransactionAsset.former_team == TEAM_ONE)
            .order_by(TransactionAsset.asset_id)
            .all()
        )
        queried_two = (
            TransactionAsset.query.filter(
                TransactionAsset.transaction_id == TRANSACTION_ID
            )
            .filter(TransactionAsset.former_team == TEAM_TWO)
            .order_by(TransactionAsset.asset_id)
            .all()
        )
        for idx in range(len(picks_one)):
            self.assertEqual(picks_one[idx]["id"], queried_one[idx].asset_id)
            self.assertEqual(picks_one[idx]["fmr"], queried_one[idx].former_team)
            self.assertEqual(picks_one[idx]["new"], queried_one[idx].new_team)
            self.assertEqual(TRANSACTION_ID, queried_one[idx].transaction_id)
            if "condition" in picks_one[idx]:
                self.assertEqual(
                    picks_one[idx]["condition"], queried_one[idx].condition
                )
        for idx in range(len(picks_two)):
            self.assertEqual(picks_two[idx]["id"], queried_two[idx].asset_id)
            self.assertEqual(picks_two[idx]["fmr"], queried_two[idx].former_team)
            self.assertEqual(picks_two[idx]["new"], queried_two[idx].new_team)
            self.assertEqual(TRANSACTION_ID, queried_two[idx].transaction_id)
            if "condition" in picks_two[idx]:
                self.assertEqual(
                    picks_two[idx]["condition"], queried_two[idx].condition
                )

    def test_create_transaction_assets_contracts(self):
        TRANSACTION_ID = 7
        TEAM_ONE = 1
        TEAM_TWO = 2
        picks_one = [
            {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)
        ]
        picks_two = [
            {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)
        ]
        for idx in range(len(picks_one)):
            if random.choice([True, False, False, False]):
                picks_one[idx]["retention"] = random.randint(1, 50)
                picks_two[idx]["retention"] = random.randint(1, 50)
        trade.create_transaction_contracts(
            TRANSACTION_ID, picks_one, TEAM_ONE, TEAM_TWO
        )
        trade.create_transaction_contracts(
            TRANSACTION_ID, picks_two, TEAM_TWO, TEAM_ONE
        )
        # Create dummy data around picks
        queried_one = (
            TransactionAsset.query.filter(
                TransactionAsset.transaction_id == TRANSACTION_ID
            )
            .filter(TransactionAsset.former_team == TEAM_ONE)
            .order_by(TransactionAsset.asset_id)
            .all()
        )
        queried_two = (
            TransactionAsset.query.filter(
                TransactionAsset.transaction_id == TRANSACTION_ID
            )
            .filter(TransactionAsset.former_team == TEAM_TWO)
            .order_by(TransactionAsset.asset_id)
            .all()
        )
        for idx in range(len(picks_one)):
            self.assertEqual(picks_one[idx]["id"], queried_one[idx].asset_id)
            self.assertEqual(picks_one[idx]["fmr"], queried_one[idx].former_team)
            self.assertEqual(picks_one[idx]["new"], queried_one[idx].new_team)
            self.assertEqual(TRANSACTION_ID, queried_one[idx].transaction_id)
            if "retention" in picks_one[idx]:
                self.assertEqual(
                    f"Retention: {picks_one[idx]['retention']}%",
                    queried_one[idx].condition,
                )
        for idx in range(len(picks_two)):
            self.assertEqual(picks_two[idx]["id"], queried_two[idx].asset_id)
            self.assertEqual(picks_two[idx]["fmr"], queried_two[idx].former_team)
            self.assertEqual(picks_two[idx]["new"], queried_two[idx].new_team)
            self.assertEqual(TRANSACTION_ID, queried_two[idx].transaction_id)
            if "retention" in picks_two[idx]:
                self.assertEqual(
                    f"Retention: {picks_two[idx]['retention']}%",
                    queried_two[idx].condition,
                )

    def test_create_transaction_assets_future_considerations(self):
        TRANSACTION_ID = 7
        TEAM_ONE = 1
        TEAM_TWO = 2
        fut_con = "Random condition nobody cares about"

        trade.create_future_considerations(TRANSACTION_ID, fut_con, TEAM_ONE, TEAM_TWO)
        trade.create_future_considerations(TRANSACTION_ID, fut_con, TEAM_TWO, TEAM_ONE)
        trade.create_future_considerations(TRANSACTION_ID, "", TEAM_ONE, TEAM_TWO)
        trade.create_future_considerations(TRANSACTION_ID, "", TEAM_TWO, TEAM_ONE)

        queried = (
            TransactionAsset.query.filter(
                TransactionAsset.transaction_id == TRANSACTION_ID
            )
            .order_by(TransactionAsset.id)
            .all()
        )

        self.assertEqual(queried[0].transaction_id, TRANSACTION_ID)
        self.assertEqual(queried[1].transaction_id, TRANSACTION_ID)
        self.assertEqual(queried[2].transaction_id, TRANSACTION_ID)
        self.assertEqual(queried[3].transaction_id, TRANSACTION_ID)
        self.assertEqual(queried[0].former_team, TEAM_ONE)
        self.assertEqual(queried[1].former_team, TEAM_TWO)
        self.assertEqual(queried[2].former_team, TEAM_ONE)
        self.assertEqual(queried[3].former_team, TEAM_TWO)
        self.assertEqual(queried[0].new_team, TEAM_TWO)
        self.assertEqual(queried[1].new_team, TEAM_ONE)
        self.assertEqual(queried[2].new_team, TEAM_TWO)
        self.assertEqual(queried[3].new_team, TEAM_ONE)
        self.assertEqual(queried[0].condition, f"Future Considerations:\n{fut_con}")
        self.assertEqual(queried[1].condition, f"Future Considerations:\n{fut_con}")
        self.assertEqual(queried[2].condition, "Future Considerations")
        self.assertEqual(queried[3].condition, "Future Considerations")

    def test_check_data(self):
        trade_data = {}
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(
                e.description,
                "Missing team one. Please make sure two teams are selected to trade",
            )
        trade_data["team_one"] = 1
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(
                e.description,
                "Missing team two. Please make sure two teams are selected to trade",
            )
        trade_data["team_two"] = 2
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing team one picks data")
        trade_data["picks_one"] = "asdf"
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing team two picks data")
        trade_data["picks_two"] = "asdf"
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing pick ID for team one. Pick a")
        trade_data["picks_one"] = []
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing pick ID for team two. Pick a")
        trade_data["picks_one"] = [{"id": "idk"}]
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing pick ID for team two. Pick a")
        trade_data["picks_two"] = []
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing team one contracts data")
        trade_data["picks_two"] = [{"id": "idk"}]
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing team one contracts data")
        trade_data["contracts_one"] = "asdf"
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing team two contracts data")
        trade_data["contracts_two"] = "asdf"
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing contract ID for team one. Pick a")
        trade_data["contracts_one"] = []
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing contract ID for team two. Pick a")
        trade_data["contracts_one"] = [{"id": "asdf"}]
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(e.description, "Missing contract ID for team two. Pick a")
        trade_data["contracts_two"] = []
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(
                e.description, "Future considerations not indicated for team one"
            )
        trade_data["contracts_two"] = [{"id": "asdf"}]
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(
                e.description, "Future considerations not indicated for team one"
            )
        trade_data["future_considerations_one"] = False
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(
                e.description, "Future considerations not indicated for team two"
            )
        trade_data["future_considerations_two"] = True
        try:
            trade.check_data(trade_data)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertEqual(
                e.description, "Future considerations details not provided"
            )
        trade_data["future_considerations"] = ""
        self.assertIsNone(trade.check_data(trade_data))

    def test_save_proposal_valid(self):
        TEAM_ONE = 1
        TEAM_TWO = 2
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "picks_one": [
                {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)
            ],
            "picks_two": [
                {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)
            ],
            "contracts_one": [
                {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(41, 50)
            ],
            "contracts_two": [
                {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(51, 60)
            ],
            "future_considerations_one": True,
            "future_considerations_two": True,
            "future_considerations": "Test condition",
            "date": date.today(),
        }
        season = Season(
            id=self.app.config["CURRENT_SEASON"], free_agency_opening=date.today()
        )
        db.session.add(season)
        db.session.add(Team(id=TEAM_ONE, name="Team One", logo="test"))
        db.session.add(Team(id=TEAM_TWO, name="Team Two", logo="test"))
        db.session.commit()
        transaction_id = trade.save_proposal(trade_data)

        transaction = Transaction.query.filter(Transaction.id == transaction_id).first()
        self.assertEqual(transaction.id, transaction_id)
        queried_assets = (
            TransactionAsset.query.filter(
                TransactionAsset.transaction_id == transaction_id
            )
            .order_by(TransactionAsset.id)
            .all()
        )
        created_assets = (
            trade_data["picks_one"]
            + trade_data["picks_two"]
            + trade_data["contracts_one"]
            + trade_data["contracts_two"]
        )
        created_assets.sort(key=lambda asset: asset["id"])
        for idx in range(len(created_assets)):
            self.assertEqual(queried_assets[idx].transaction_id, transaction_id)
            self.assertEqual(queried_assets[idx].asset_id, created_assets[idx]["id"])
            self.assertEqual(
                queried_assets[idx].former_team, created_assets[idx]["fmr"]
            )
            self.assertEqual(queried_assets[idx].new_team, created_assets[idx]["new"])
        # Future considerations
        self.assertEqual(queried_assets[-2].transaction_id, transaction_id)
        self.assertEqual(queried_assets[-2].former_team, TEAM_ONE)
        self.assertEqual(queried_assets[-2].new_team, TEAM_TWO)
        self.assertEqual(
            queried_assets[-2].condition, f"Future Considerations:\nTest condition"
        )
        self.assertEqual(queried_assets[-1].transaction_id, transaction_id)
        self.assertEqual(queried_assets[-1].new_team, TEAM_ONE)
        self.assertEqual(queried_assets[-1].former_team, TEAM_TWO)
        self.assertEqual(
            queried_assets[-1].condition, f"Future Considerations:\nTest condition"
        )

    def test_get_form_team(self):
        team = Team(id=5, name="Test Team", abbreviation="TT", logo="no")
        db.session.add(team)
        db.session.commit()
        queried = trade.get_form_team(team.id)
        self.assertEqual(team.name, queried["name"])
        self.assertEqual(team.logo, queried["logo"])

    def test_get_form_picks(self):
        TEAM_ONE = 1
        TEAM_TWO = 2
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "picks_one": [
                {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)
            ],
            "picks_two": [
                {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)
            ],
            "contracts_one": [],
            "contracts_two": [],
            "future_considerations_one": True,
            "future_considerations_two": True,
            "future_considerations": "Test condition",
        }
        self.create_dummy_assets(trade_data)
        picks_one = trade.get_form_picks(trade_data["picks_one"])
        picks_two = trade.get_form_picks(trade_data["picks_two"])
        picks_three = trade.get_form_picks(trade_data["contracts_one"])
        self.assertEqual(picks_three, [])
        for idx in range(len(picks_one)):
            self.assertEqual(picks_one[idx]["id"], trade_data["picks_one"][idx]["id"])
            self.assertEqual(picks_one[idx]["originating_team"], "T01")
            self.assertEqual(picks_one[idx]["round"], 5)
            self.assertEqual(picks_one[idx]["season"], date.today().year)
        for idx in range(len(picks_two)):
            self.assertEqual(picks_two[idx]["id"], trade_data["picks_two"][idx]["id"])
            self.assertEqual(picks_two[idx]["originating_team"], "T02")
            self.assertEqual(picks_two[idx]["round"], 8)
            self.assertEqual(picks_two[idx]["season"], date.today().year)

    def test_get_form_contracts(self):
        TEAM_ONE = 1
        TEAM_TWO = 2
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "picks_one": [],
            "picks_two": [],
            "contracts_one": [
                {"id": idx, "retention": "", "fmr": TEAM_ONE, "new": TEAM_TWO}
                for idx in range(41, 50)
            ],
            "contracts_two": [
                {
                    "id": idx,
                    "retention": random.randint(1, 25),
                    "fmr": TEAM_TWO,
                    "new": TEAM_ONE,
                }
                for idx in range(51, 60)
            ],
            "future_considerations_one": True,
            "future_considerations_two": True,
            "future_considerations": "Test condition",
        }
        self.create_dummy_assets(trade_data)
        contracts_one = trade.get_form_contracts(trade_data["contracts_one"])
        contracts_two = trade.get_form_contracts(trade_data["contracts_two"])
        contracts_three = trade.get_form_contracts(trade_data["picks_one"])
        self.assertEqual(contracts_three, [])
        for idx in range(len(contracts_one)):
            self.assertEqual(contracts_one[idx]["id"], 0)
            self.assertEqual(contracts_one[idx]["name"], "Test Hmn")
            self.assertEqual(contracts_one[idx]["nhl_salary"], 14)
            self.assertEqual(contracts_one[idx]["cap_hit"], 13)
        for idx in range(len(contracts_two)):
            self.assertEqual(contracts_two[idx]["id"], 0)
            self.assertEqual(contracts_two[idx]["name"], "Test Hmn")
            self.assertEqual(contracts_two[idx]["nhl_salary"], 142)
            self.assertEqual(contracts_two[idx]["cap_hit"], 113)

    def test_get_form_details(self):
        TEAM_ONE = 1
        TEAM_TWO = 2
        trade_data = {
            "team_one": TEAM_ONE,
            "team_two": TEAM_TWO,
            "picks_one": [
                {"id": idx, "fmr": TEAM_ONE, "new": TEAM_TWO} for idx in range(21, 30)
            ],
            "picks_two": [
                {"id": idx, "fmr": TEAM_TWO, "new": TEAM_ONE} for idx in range(31, 40)
            ],
            "contracts_one": [
                {"id": idx, "retention": "", "fmr": TEAM_ONE, "new": TEAM_TWO}
                for idx in range(41, 50)
            ],
            "contracts_two": [
                {
                    "id": idx,
                    "retention": random.randint(1, 25),
                    "fmr": TEAM_TWO,
                    "new": TEAM_ONE,
                }
                for idx in range(51, 60)
            ],
            "future_considerations_one": True,
            "future_considerations_two": True,
            "future_considerations": "Test condition",
        }
        self.create_dummy_assets(trade_data)
        queried_data = trade.get_form_details(trade_data)
        self.assertEqual(queried_data["clause_warning"], [])
        self.assertEqual(queried_data["team_one"]["name"], "Test One")
        self.assertEqual(queried_data["team_one"]["logo"], "lol")
        self.assertEqual(queried_data["team_two"]["name"], "Test Two")
        self.assertEqual(queried_data["team_two"]["logo"], "lol2")
        contracts_one = queried_data["contracts_one"]
        contracts_two = queried_data["contracts_two"]
        for idx in range(len(contracts_one)):
            self.assertEqual(contracts_one[idx]["id"], 0)
            self.assertEqual(contracts_one[idx]["name"], "Test Hmn")
            self.assertEqual(contracts_one[idx]["nhl_salary"], 14)
            self.assertEqual(contracts_one[idx]["cap_hit"], 13)
        for idx in range(len(contracts_two)):
            self.assertEqual(contracts_two[idx]["id"], 0)
            self.assertEqual(contracts_two[idx]["name"], "Test Hmn")
            self.assertEqual(contracts_two[idx]["nhl_salary"], 142)
            self.assertEqual(contracts_two[idx]["cap_hit"], 113)
        picks_one = queried_data["picks_one"]
        picks_two = queried_data["picks_two"]
        for idx in range(len(picks_one)):
            self.assertEqual(picks_one[idx]["id"], trade_data["picks_one"][idx]["id"])
            self.assertEqual(picks_one[idx]["originating_team"], "T01")
            self.assertEqual(picks_one[idx]["round"], 5)
            self.assertEqual(picks_one[idx]["season"], date.today().year)
        for idx in range(len(picks_two)):
            self.assertEqual(picks_two[idx]["id"], trade_data["picks_two"][idx]["id"])
            self.assertEqual(picks_two[idx]["originating_team"], "T02")
            self.assertEqual(picks_two[idx]["round"], 8)
            self.assertEqual(picks_two[idx]["season"], date.today().year)
        ContractYear.query.filter(
            ContractYear.contract_id == trade_data["contracts_one"][0]["id"]
        ).filter(ContractYear.season_id == self.app.config["CURRENT_SEASON"]).update(
            {"ntc": True}
        )
        queried_data = trade.get_form_details(trade_data)
        self.assertEqual(queried_data["clause_warning"][0], "Test Hmn: NTC")

    def test_query_transaction_team_ids(self):
        transaction_id = 7
        team_one = 9
        team_two = 42
        team_three = 3

        empty = trade.query_transaction_team_ids(transaction_id)
        self.assertEqual(empty, set())

        transaction_asset_one = TransactionAsset(
            transaction_id=transaction_id,
            asset_id=0,
            former_team=team_one,
            new_team=team_two,
        )
        transaction_asset_two = TransactionAsset(
            transaction_id=transaction_id,
            asset_id=1,
            former_team=team_two,
            new_team=team_one,
        )
        db.session.add(transaction_asset_one)
        db.session.add(transaction_asset_two)
        db.session.commit()
        simple_trade = trade.query_transaction_team_ids(transaction_id)
        simple_trade = trade.query_transaction_team_ids(transaction_id)
        self.assertEqual(simple_trade, {team_one, team_two})

        transaction_asset_three = TransactionAsset(
            transaction_id=transaction_id,
            asset_id=2,
            former_team=team_three,
            new_team=team_one,
        )
        db.session.add(transaction_asset_three)
        db.session.commit()
        three_way_trade = trade.query_transaction_team_ids(transaction_id)
        self.assertEqual(three_way_trade, {team_one, team_two, team_three})

    def test_query_retained_assets(self):
        transaction_id = 7
        empty = trade.query_retained_assets(transaction_id, 1)
        self.assertEqual([], empty)

        retained = "Retention: 25%"
        other = "Some other Retention condition"
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=8,
                former_team=1,
                new_team=2,
            )
        )
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=6,
                former_team=1,
                new_team=2,
                condition=retained,
            )
        )
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=4,
                former_team=1,
                new_team=2,
                condition=other,
            )
        )
        queried = trade.query_retained_assets(transaction_id, 1)
        self.assertEqual(1, len(queried))
        self.assertEqual(6, queried[0][0])
        self.assertEqual(retained, queried[0][1])

    def test_get_retained_assets(self):
        transaction_id = 7
        empty = trade.get_retained_assets(transaction_id, 1)
        self.assertEqual([], empty)

        retained = "Retention: 25%"
        other = "Some other Retention condition"
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=8,
                former_team=1,
                new_team=2,
            )
        )
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=6,
                former_team=1,
                new_team=2,
                condition=retained,
            )
        )
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=4,
                former_team=1,
                new_team=2,
                condition=other,
            )
        )
        queried = trade.get_retained_assets(transaction_id, 1)
        self.assertEqual(1, len(queried))
        self.assertEqual(6, queried[0]["id"])
        self.assertEqual(retained, queried[0]["retention"])

    def test_create_retained_salary(self):
        transaction_id = 7
        retained = "Retention: 25%"
        other = "Some other Retention condition"
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=8,
                former_team=1,
                new_team=2,
            )
        )
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=6,
                former_team=1,
                new_team=2,
                condition=retained,
            )
        )
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=4,
                former_team=1,
                new_team=2,
                condition=other,
            )
        )
        db.session.commit()
        trade.create_retained_salary(
            transaction_id, 1, datetime.now().strftime(self.app.config["DATE_FMT"])
        )
        db.session.commit()

        retained = RetainedSalary.query.all()
        self.assertEqual(len(retained), 1)
        self.assertEqual(retained[0].contract_id, 6)
        self.assertEqual(retained[0].retention, 25)
        self.assertEqual(retained[0].retained_by, 1)
        asset = Asset.query.filter(Asset.id == retained[0].id).first()
        self.assertIsNotNone(asset)
        self.assertEqual(asset.type, "Retained Salary")

    def test_query_get_asset_data(self):
        transaction_id = 7
        assets = [
            {
                "id": random.randint(0, 2000),
                "current_team": random.randint(0, 32),
                "originating_team": random.randint(0, 32),
                "active": True,
            }
            for _ in range(3)
        ]
        transaction_assets = [
            {
                "transaction_id": transaction_id,
                "asset_id": asset["id"],
                "former_team": asset["current_team"],
                "new_team": random.randint(0, 32),
                "condition": "".join(random.choices(LETTERS, k=16)),
            }
            for asset in assets
        ]
        for asset in assets:
            db.session.add(Asset(**asset))
        for transaction_asset in transaction_assets:
            db.session.add(TransactionAsset(**transaction_asset))
        db.session.commit()
        assets.sort(key=lambda x: x["id"])
        transaction_assets.sort(key=lambda x: x["asset_id"])

        queried = trade.query_asset_data(transaction_id)
        self.assertEqual(len(queried), 3)
        for idx in range(len(assets)):
            self.assertEqual(assets[idx]["id"], queried[idx][0])
            self.assertEqual(transaction_assets[idx]["asset_id"], queried[idx][0])
            self.assertEqual(transaction_assets[idx]["former_team"], queried[idx][1])
            self.assertEqual(transaction_assets[idx]["new_team"], queried[idx][2])
            self.assertEqual(assets[idx]["current_team"], queried[idx][3])
            self.assertEqual(assets[idx]["active"], queried[idx][4])
            self.assertEqual(transaction_assets[idx]["condition"], queried[idx][5])

        queried = trade.get_asset_data(transaction_id)
        self.assertEqual(len(queried), 3)
        for idx in range(len(assets)):
            self.assertEqual(assets[idx]["id"], queried[idx]["id"])
            self.assertEqual(transaction_assets[idx]["asset_id"], queried[idx]["id"])
            self.assertEqual(
                transaction_assets[idx]["former_team"], queried[idx]["former_team"]
            )
            self.assertEqual(
                transaction_assets[idx]["new_team"], queried[idx]["new_team"]
            )
            self.assertEqual(assets[idx]["current_team"], queried[idx]["current_team"])
            self.assertEqual(assets[idx]["active"], queried[idx]["active"])
            self.assertEqual(
                transaction_assets[idx]["condition"], queried[idx]["condition"]
            )

    def test_update_assets(self):
        transaction_id = 7
        assets = [
            {
                "id": random.randint(0, 2000),
                "current_team": random.randint(0, 32),
                "originating_team": random.randint(0, 32),
                "active": True,
            }
            for _ in range(3)
        ]
        transaction_assets = [
            {
                "transaction_id": transaction_id,
                "asset_id": asset["id"],
                "former_team": asset["current_team"],
                "new_team": random.randint(0, 32),
                "condition": "".join(random.choices(LETTERS, k=16)),
            }
            for asset in assets
        ]
        for asset in assets:
            db.session.add(Asset(**asset))
        for transaction_asset in transaction_assets:
            db.session.add(TransactionAsset(**transaction_asset))
        db.session.commit()
        assets.sort(key=lambda x: x["id"])
        transaction_assets.sort(key=lambda x: x["asset_id"])

        for asset in transaction_assets:
            trade.update_assets(transaction_id, asset["new_team"])
        db.session.commit()
        queried = trade.get_asset_data(transaction_id)
        self.assertEqual(len(queried), 3)
        for idx in range(len(assets)):
            self.assertEqual(
                transaction_assets[idx]["new_team"], queried[idx]["new_team"]
            )
            self.assertEqual(
                transaction_assets[idx]["new_team"], queried[idx]["current_team"]
            )

    def test_check_transaction_assets_unowned_assets(self):
        asset = Asset(current_team=1, originating_team=2, id=9)
        transaction_asset = TransactionAsset(
            transaction_id=7, asset_id=9, former_team=3, new_team=4
        )
        db.session.add(asset)
        db.session.add(transaction_asset)
        db.session.commit()
        try:
            trade.check_transaction_assets(7)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertTrue(
                "Transaction asset no longer belongs to trading team" in e.description
            )

    def test_check_transaction_assets_inactive_assets(self):
        asset = Asset(
            current_team=1,
            originating_team=2,
            id=9,
            active=False,
        )
        transaction_asset = TransactionAsset(
            transaction_id=7, asset_id=9, former_team=1, new_team=4
        )
        db.session.add(asset)
        db.session.add(transaction_asset)
        db.session.commit()
        try:
            trade.check_transaction_assets(7)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertTrue("Transaction asset no longer active" in e.description)

    def test_check_transaction_assets_too_much_retention(self):
        asset = Asset(
            current_team=1,
            originating_team=2,
            id=9,
            active=True,
        )
        transaction_asset = TransactionAsset(
            transaction_id=7,
            asset_id=9,
            former_team=1,
            new_team=4,
            condition="Retention 51%",
        )
        db.session.add(asset)
        db.session.add(transaction_asset)
        db.session.commit()
        try:
            trade.check_transaction_assets(7)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertTrue("Greater than 50% retention on asset" in e.description)

    def test_check_transaction_assets_too_little_retention(self):
        asset = Asset(
            current_team=1,
            originating_team=2,
            id=9,
            active=True,
        )
        transaction_asset = TransactionAsset(
            transaction_id=7,
            asset_id=9,
            former_team=1,
            new_team=4,
            condition="Retention 0%",
        )
        db.session.add(asset)
        db.session.add(transaction_asset)
        db.session.commit()
        try:
            trade.check_transaction_assets(7)
        except InternalServerError as e:
            self.assertEqual(e.code, 500)
            self.assertTrue("0% or less retention on asset" in e.description)

    def test_check_transaction_assets_sucess(self):
        asset = Asset(
            current_team=1,
            originating_team=2,
            id=9,
            active=True,
        )
        transaction_asset = TransactionAsset(
            transaction_id=7,
            asset_id=9,
            former_team=1,
            new_team=4,
        )
        db.session.add(asset)
        db.session.add(transaction_asset)
        db.session.commit()
        self.assertTrue(trade.check_transaction_assets(7))

    def test_apply_to_database(self):
        asset_one = Asset(
            id=1, originating_team=1, current_team=1, active=True, type="Contract"
        )
        asset_two = Asset(id=2, originating_team=2, current_team=2, active=True)
        transaction = Transaction(
            id=1,
            date=datetime.now(),
            type="Trade Proposal",
            season_id=1,
        )
        transaction_asset_one = TransactionAsset(
            transaction_id=1,
            asset_id=1,
            former_team=1,
            new_team=2,
            condition="Retention: 25%",
        )
        transaction_asset_two = TransactionAsset(
            transaction_id=1,
            asset_id=2,
            former_team=2,
            new_team=1,
        )
        contract = Contract(
            id=1,
            person_id=674,
        )
        person = Person(
            id=674,
            name="Test Person",
            captain=True,
            alternate_captain=True,
        )
        db.session.add(asset_one)
        db.session.add(asset_two)
        db.session.add(contract)
        db.session.add(person)
        db.session.add(transaction)
        db.session.add(transaction_asset_one)
        db.session.add(transaction_asset_two)
        db.session.commit()
        trade.apply_to_database(1, date.today().strftime(self.app.config["DATE_FMT"]))
        retained = RetainedSalary.query.first()
        self.assertEqual(asset_one.current_team, 2)
        self.assertEqual(asset_two.current_team, 1)
        self.assertIsNone(transaction.user_id)
        self.assertIsNotNone(retained)
        self.assertEqual(retained.retention, 25)
        self.assertEqual(retained.retained_by, 1)
        self.assertEqual(retained.contract_id, 1)
        asset = Asset.query.filter(Asset.id == retained.id).first()
        self.assertIsNotNone(asset)
        self.assertEqual(asset.type, "Retained Salary")
        self.assertFalse(person.captain)
        self.assertFalse(person.alternate_captain)

    def test_save_proposal_and_apply(self):
        """
        {
            'team_one': '7',
            'team_two': '16',
            'picks_one': [
                {'id': '441', 'condition': 'idk'},
                {'id': '508'}
            ],
            'contracts_one': [],
            'picks_two': [
                {'id': '449'}
            ],
            'contracts_two': [
                {'id': '670', 'retention': '0'}
            ],
            'future_considerations_one': False,
            'future_considerations_two': False,
            'future_considerations': ''
        }
        """
        team_one = Team(id=7, abbreviation="T01", name="Test Team 1")
        team_two = Team(id=16, abbreviation="T02", name="Test Team 2")
        season = Season(name="Test Season", id=1)
        person_one = Person(id=1, name="Test Person 1")
        asset_one = Asset(id=1, originating_team=7, current_team=7)
        contract_one = Contract(id=1, person_id=1, years=1)
        contract_year_one = ContractYear(
            id=1, contract_id=1, season_id=1, aav=1000000, nhl_salary=1000000
        )
        person_two = Person(id=2, name="Test Person 2")
        asset_two = Asset(id=2, originating_team=16, current_team=16)
        contract_two = Contract(id=2, person_id=2, years=1)
        contract_year_two = ContractYear(
            id=2, contract_id=2, season_id=1, aav=1000000, nhl_salary=1000000
        )
        asset_three = Asset(id=3, originating_team=7, current_team=7)
        draft_pick_three = DraftPick(id=3, season_id=1, round=1)
        asset_four = Asset(id=4, originating_team=16, current_team=16)
        draft_pick_four = DraftPick(id=4, season_id=1, round=1)
        db.session.add(team_one)
        db.session.add(team_two)
        db.session.add(season)
        db.session.add(person_one)
        db.session.add(asset_one)
        db.session.add(contract_one)
        db.session.add(contract_year_one)
        db.session.add(person_two)
        db.session.add(asset_two)
        db.session.add(contract_two)
        db.session.add(contract_year_two)
        db.session.add(asset_three)
        db.session.add(draft_pick_three)
        db.session.add(asset_four)
        db.session.add(draft_pick_four)
        db.session.commit()
        test_data = {
            "team_one": 7,
            "team_two": 16,
            "picks_one": [{"id": "3"}],
            "picks_two": [{"id": "4", "condition": "idk"}],
            "contracts_one": [{"id": 1, "retention": "34"}],
            "contracts_two": [{"id": 2}],
            "future_considerations_one": True,
            "future_considerations_two": False,
            "future_considerations": "Zilch",
            "date": date.today().strftime(self.app.config["DATE_FMT"]),
        }
        trade.save_proposal(test_data)
        trade_record = Transaction.query.first()
        self.assertIsNotNone(trade_record)
        self.assertEqual(trade_record.type, "Trade")
        self.assertEqual(trade_record.season_id, 1)
        self.assertIsNone(trade_record.user_id)
        trade_asset_one = TransactionAsset.query.filter(
            TransactionAsset.asset_id == 1
        ).first()
        self.assertIsNotNone(trade_asset_one)
        self.assertEqual(trade_asset_one.transaction_id, trade_record.id)
        self.assertEqual(trade_asset_one.former_team, 7)
        self.assertEqual(trade_asset_one.new_team, 16)
        self.assertEqual(trade_asset_one.condition, "Retention: 34%")
        self.assertEqual(asset_one.current_team, 16)
        trade_asset_two = TransactionAsset.query.filter(
            TransactionAsset.asset_id == 2
        ).first()
        self.assertIsNotNone(trade_asset_two)
        self.assertEqual(trade_asset_two.transaction_id, trade_record.id)
        self.assertEqual(trade_asset_two.former_team, 16)
        self.assertEqual(trade_asset_two.new_team, 7)
        self.assertIsNone(trade_asset_two.condition)
        self.assertEqual(asset_two.current_team, 7)
        trade_asset_three = TransactionAsset.query.filter(
            TransactionAsset.asset_id == 3
        ).first()
        self.assertIsNotNone(trade_asset_three)
        self.assertEqual(trade_asset_three.transaction_id, trade_record.id)
        self.assertEqual(trade_asset_three.former_team, 7)
        self.assertEqual(trade_asset_three.new_team, 16)
        self.assertIsNone(trade_asset_three.condition)
        self.assertEqual(asset_three.current_team, 16)
        trade_asset_four = TransactionAsset.query.filter(
            TransactionAsset.asset_id == 4
        ).first()
        self.assertIsNotNone(trade_asset_four)
        self.assertEqual(trade_asset_four.transaction_id, trade_record.id)
        self.assertEqual(trade_asset_four.former_team, 16)
        self.assertEqual(trade_asset_four.new_team, 7)
        self.assertEqual(trade_asset_four.condition, "idk")
        self.assertEqual(asset_four.current_team, 7)
        retained_salary = RetainedSalary.query.first()
        asset_five = Asset.query.filter(Asset.id == 5).first()
        self.assertIsNotNone(retained_salary)
        self.assertEqual(retained_salary.contract_id, 1)
        self.assertEqual(retained_salary.retained_by, 7)
        self.assertEqual(retained_salary.retention, 34)
        self.assertEqual(retained_salary.id, asset_five.id)
        self.assertEqual(asset_five.current_team, 7)
        self.assertEqual(asset_five.originating_team, 7)
        self.assertEqual(asset_five.type, "Retained Salary")

    def test_get_total_retention(self):
        #   No past retention
        transaction_id = 7
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=6,
                former_team=1,
                new_team=2,
                condition="Retention: 20%",
            )
        )
        db.session.flush()
        trade.create_retained_salary(
            transaction_id, 1, datetime.now().strftime(self.app.config["DATE_FMT"])
        )
        db.session.commit()

        retained = RetainedSalary.query.order_by(RetainedSalary.id.asc()).all()
        self.assertEqual(len(retained), 1)
        self.assertEqual(retained[0].contract_id, 6)
        self.assertEqual(retained[0].retention, 20)
        self.assertEqual(retained[0].retained_by, 1)

        self.assertEqual(trade.get_total_retention(6), 20)

        #   One past retention
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id,
                asset_id=6,
                former_team=2,
                new_team=3,
                condition="Retention: 50%",
            )
        )
        db.session.flush()
        trade.create_retained_salary(
            transaction_id, 2, datetime.now().strftime(self.app.config["DATE_FMT"])
        )
        db.session.commit()

        retained = RetainedSalary.query.order_by(RetainedSalary.id.asc()).all()
        self.assertEqual(len(retained), 2)
        self.assertEqual(retained[1].contract_id, 6)
        self.assertEqual(retained[1].retention, 40)
        self.assertEqual(retained[1].retained_by, 2)

        self.assertEqual(trade.get_total_retention(6), 60)

        #   Two past retentions
        db.session.add(
            TransactionAsset(
                transaction_id=transaction_id + 1,
                asset_id=6,
                former_team=3,
                new_team=4,
                condition="Retention: 25%",
            )
        )
        db.session.flush()
        trade.create_retained_salary(
            transaction_id + 1, 3, datetime.now().strftime(self.app.config["DATE_FMT"])
        )
        db.session.commit()

        retained = RetainedSalary.query.order_by(RetainedSalary.id.asc()).all()
        self.assertEqual(len(retained), 3)
        self.assertEqual(retained[2].contract_id, 6)
        self.assertEqual(retained[2].retention, 10)
        self.assertEqual(retained[2].retained_by, 3)

        self.assertEqual(trade.get_total_retention(6), 70)

    def test_get_form_cap_warning(self):
        season_id = self.app.config["CURRENT_SEASON"]
        db.session.add(
            Season(
                id=season_id,
                salary_cap=1000,
                salary_floor=100,
            )
        )
        team_one = 4
        team_two = 5
        db.session.add(Team(id=team_one, name="Test Team One"))
        db.session.add(Team(id=team_two, name="Test Team Two"))

        team_season_one = TeamSeason(
            team_id=team_one,
            season_id=season_id,
            cap_hit=1,
            projected_cap_hit=1,
        )
        team_season_two = TeamSeason(
            team_id=team_two,
            season_id=season_id,
            cap_hit=999,
            projected_cap_hit=999,
        )
        db.session.add(team_season_one)
        db.session.add(team_season_two)

        # asset, person, contract, contract_year
        db.session.add(Asset(id=6, active=True))
        db.session.add(Person(id=7, name="Test One"))
        db.session.add(Contract(id=6, person_id=7))
        contract_year_one = ContractYear(
            contract_id=6, season_id=season_id, cap_hit=5, nhl_salary=5
        )
        db.session.add(contract_year_one)

        db.session.add(Asset(id=8, active=True))
        db.session.add(Person(id=9, name="Test Two"))
        db.session.add(Contract(id=8, person_id=9))
        contract_year_two = ContractYear(
            contract_id=8, season_id=season_id, cap_hit=1, nhl_salary=5
        )
        db.session.add(contract_year_two)
        db.session.commit()

        data = {
            "team_one": team_one,
            "team_two": team_two,
            "contracts_one": [{"id": 6, "retention": 0}],
            "contracts_two": [{"id": 8, "retention": 0}],
        }
        warnings = trade.get_form_cap_warning(data)
        warnings.sort()
        self.assertEqual(len(warnings), 2)
        self.assertEqual(
            warnings[0], "Test Team One: under the cap floor with this trade"
        )
        self.assertEqual(
            warnings[1], "Test Team Two: over the salary cap with this trade"
        )

    def test_check_previously_retained(self):
        team_id = 5
        assets = [2, 3, 4]
        self.assertEqual(trade.check_previously_retained(team_id, assets), [])
        db.session.add(Asset(id=1))
        db.session.add(Asset(id=2))
        db.session.add(Asset(id=3))
        db.session.add(
            RetainedSalary(
                id=1,
                retained_by=team_id,
                contract_id=2,
            )
        )
        db.session.add(
            RetainedSalary(
                id=2,
                retained_by=team_id,
                contract_id=3,
            )
        )
        db.session.add(
            RetainedSalary(
                id=3,
                retained_by=team_id,
                contract_id=4,
            )
        )
        db.session.commit()
        self.assertEqual(
            trade.check_previously_retained(team_id, assets), [(2,), (3,), (4,)]
        )

    def test_check_previously_retained(self):
        team_id = 5
        assets = [2, 3, 4]
        self.assertEqual(trade.check_previously_retained(team_id, assets), [])
        db.session.add(Asset(id=1))
        db.session.add(Asset(id=2))
        db.session.add(Asset(id=3))
        db.session.add(
            RetainedSalary(
                id=1,
                retained_by=team_id,
                contract_id=2,
            )
        )
        db.session.add(
            RetainedSalary(
                id=2,
                retained_by=team_id,
                contract_id=3,
            )
        )
        db.session.add(
            RetainedSalary(
                id=3,
                retained_by=team_id,
                contract_id=4,
            )
        )
        db.session.commit()
        self.assertEqual(
            trade.check_previously_retained(team_id, assets), [(2,), (3,), (4,)]
        )

    def test_query_contract_data(self):
        asset_id = 847
        team_id = 44
        person_id = 27
        self.assertIsNone(trade.query_contract(asset_id))
        self.assertIsNone(trade.contract_data({"id": asset_id}))

        asset = Asset(
            id=asset_id,
            originating_team=team_id,
            current_team=team_id,
            type="Contract",
            active=True,
        )
        person = Person(
            id=person_id,
            name="Test Person",
        )
        contract = Contract(
            id=asset_id,
            person_id=person.id,
            years=1,
        )
        contract_year = ContractYear(
            contract_id=asset.id,
            season_id=self.app.config["CURRENT_SEASON"],
            cap_hit=123,
            nhl_salary=654,
        )
        db.session.add(asset)
        db.session.add(person)
        db.session.add(contract)
        db.session.add(contract_year)
        db.session.commit()

        raw_data = trade.query_contract(asset_id)
        self.assertEqual(raw_data[0], asset.id)
        self.assertEqual(raw_data[1], person.name)
        self.assertEqual(raw_data[2], contract_year.cap_hit)
        self.assertEqual(raw_data[3], contract_year.nhl_salary)
        self.assertEqual(raw_data[4], person.id)
        self.assertEqual(raw_data[5], asset.type)

        data = trade.contract_data({"id": asset_id, "retention": 0})
        self.assertEqual(data["name"], person.name)
        self.assertEqual(data["cap_hit"], contract_year.cap_hit)
        self.assertEqual(data["nhl_salary"], contract_year.nhl_salary)
        self.assertEqual(data["id"], person.id)
        self.assertEqual(data["type"], asset.type)
        self.assertTrue("retention" not in data)

    def test_query_rights_details(self):
        asset_id = 847
        team_id = 44
        person_id = 88
        self.assertIsNone(trade.query_rights_details(asset_id))

        asset = Asset(
            id=asset_id,
            originating_team=team_id,
            current_team=team_id,
            type="Signing Rights",
            active=True,
        )
        person = Person(
            name="Test Person",
            id=person_id,
        )
        rights = SigningRights(
            id=asset.id,
            person_id=person.id,
        )
        db.session.add(asset)
        db.session.add(person)
        db.session.add(rights)
        db.session.commit()

        raw_data = trade.query_rights_details(asset_id)
        self.assertEqual(raw_data[0], asset.id)
        self.assertEqual(raw_data[1], person.name)
        self.assertEqual(raw_data[2], 0)
        self.assertEqual(raw_data[3], 0)
        self.assertEqual(raw_data[4], person.id)
        self.assertEqual(raw_data[5], asset.type)

        raw_data2 = trade.query_contract(asset_id)
        self.assertEqual(raw_data2[0], asset.id)
        self.assertEqual(raw_data2[1], person.name)
        self.assertEqual(raw_data2[2], 0)
        self.assertEqual(raw_data2[3], 0)
        self.assertEqual(raw_data2[4], person.id)
        self.assertEqual(raw_data2[5], asset.type)

    def test_query_team_contracts(self):
        team_id = 88
        self.assertEqual(trade.query_team_contracts(team_id), [])

        assets = [
            Asset(current_team=team_id, active=True, type="Signing Rights"),
            Asset(current_team=team_id, active=True, type="Contract"),
            Asset(current_team=team_id, active=True, type="Draft Pick"),
            Asset(current_team=team_id, active=False, type="Signing Rights"),
            Asset(current_team=team_id, active=False, type="Contract"),
            Asset(current_team=team_id, active=False, type="Draft Pick"),
        ]
        for asset in assets:
            db.session.add(asset)
        db.session.commit()

        raw_data = trade.query_team_contracts(team_id)
        self.assertTrue((assets[0].id,) in raw_data)
        self.assertTrue((assets[1].id,) in raw_data)

    def test_query_trade_ids(self):
        self.assertEqual(trade.query_trade_ids(), [])
        trade_ = Transaction(type="Trade", id=55)
        not_trade = Transaction(type="Trade Proposal", id=32)
        db.session.add(trade_)
        db.session.add(not_trade)
        db.session.commit()
        self.assertEqual(trade.query_trade_ids(), [55])
        trade_ = Transaction(type="Trade", id=25)
        db.session.add(trade_)
        db.session.commit()
        self.assertEqual(trade.query_trade_ids(), [25, 55])

    def test_clause_warnings(self):
        data = [
            {"id": 52},
            {"id": 91},
            {"id": 29},
        ]
        self.assertIsNone(trade.get_clause_warning(data))
        self.assertEqual(trade.query_clauses([x["id"] for x in data]), [])

        person = Person(id=88, name="Test")
        contracts = [
            Contract(id=52, person_id=person.id),
            Contract(id=91, person_id=person.id),
            Contract(id=29, person_id=person.id),
        ]
        years = [
            ContractYear(
                contract_id=contracts[0].id,
                season_id=self.app.config["CURRENT_SEASON"],
                nmc=True,
                ntc=False,
                clause_limits=None,
            ),
            ContractYear(
                contract_id=contracts[1].id,
                season_id=self.app.config["CURRENT_SEASON"],
                nmc=False,
                ntc=True,
                clause_limits=None,
            ),
            ContractYear(
                contract_id=contracts[2].id,
                season_id=self.app.config["CURRENT_SEASON"],
                nmc=True,
                ntc=True,
                clause_limits="Limit",
            ),
        ]
        db.session.add(person)
        for contract in contracts:
            db.session.add(contract)
        for year in years:
            db.session.add(year)
        db.session.commit()

        queried = trade.query_clauses([x["id"] for x in data])
        self.assertEqual(len(queried), 3)
        self.assertEqual(queried[1], (True, False, None, "Test"))
        self.assertEqual(queried[2], (False, True, None, "Test"))
        self.assertEqual(queried[0], (True, True, "Limit", "Test"))

        queried = trade.get_clause_warning(data)
        self.assertEqual(len(queried), 3)
        self.assertEqual(queried[1], "Test: NMC")
        self.assertEqual(queried[2], "Test: NTC")
        self.assertEqual(queried[0], "Test: NMC, NTC (Limit)")

    def test_query_team_assets(self):
        TEAM_ID = 4
        self.assertEqual(trade.query_team_assets(TEAM_ID), [])

        assets = [
            Asset(
                current_team=TEAM_ID,
                active=True,
            ),
            Asset(
                current_team=TEAM_ID,
                active=False,
            ),
            Asset(
                current_team=TEAM_ID + 1,
                active=True,
            ),
            Asset(
                current_team=TEAM_ID,
                active=False,
            ),
            Asset(
                current_team=TEAM_ID,
                active=True,
            ),
        ]
        for asset in assets:
            db.session.add(asset)
        db.session.commit()

        self.assertEqual(
            trade.query_team_assets(TEAM_ID),
            [
                (assets[0].id,),
                (assets[4].id,),
            ],
        )

    def test_create_notes(self):
        TRADE = 832
        teams = [
            Team(id=88, name="TST", abbreviation="TST", logo="lol/no"),
            Team(id=22, name="TET", abbreviation="TET", logo="idk/logo.svg"),
        ]
        transaction_assets = [
            TransactionAsset(
                id=1,
                transaction_id=TRADE,
                former_team=teams[0].id,
                new_team=teams[1].id,
                asset_id=1,
            ),
            TransactionAsset(
                id=10,
                transaction_id=TRADE,
                former_team=teams[1].id,
                new_team=teams[0].id,
                asset_id=1,
            ),
        ]
        for record in teams + transaction_assets:
            db.session.add(record)
        db.session.commit()
        trade.create_notes(TRADE)
        db.session.commit()

        notes = Note.query.order_by(Note.record_id.asc()).all()
        self.assertEqual(len(notes), 2)
        self.assertEqual(notes[0].record_id, 1)
        self.assertEqual(notes[1].record_id, 10)
        self.assertTrue(notes[0].safe)
        self.assertTrue(notes[1].safe)
        self.assertEqual(notes[0].record_table, "transaction_asset")
        self.assertEqual(notes[1].record_table, "transaction_asset")
        self.assertEqual(
            notes[0].note,
            'To <a href="/team/TET"><img class="logo" alt="TET logo" src="/static/idk/logo.svg"> TET</a> - <a href="/trade/832">Details</a>',
        )
        self.assertEqual(
            notes[1].note,
            'To <a href="/team/TST"><img class="logo" alt="TST logo" src="/static/lol/no"> TST</a> - <a href="/trade/832">Details</a>',
        )


if __name__ == "__main__":
    unittest.main(verbosity=2)
