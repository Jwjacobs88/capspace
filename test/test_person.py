import sys
import random
import unittest
from datetime import datetime, date, timedelta
from werkzeug.exceptions import NotFound, InternalServerError
from sqlalchemy.exc import MultipleResultsFound
from app import create_app, db
from app.models import (
    Team,
    Season,
    Person,
    Asset,
    DraftPick,
    ProfessionalTryout,
    SigningRights,
    Contract,
    ContractYear,
    CachedContent,
)

from app.main import person, team

try:
    from .create_people import (
        create_zach_parise,
        create_jared_cowen,
        create_nathan_gerbe,
        create_mike_richards,
    )
except ImportError:
    from create_people import (
        create_zach_parise,
        create_jared_cowen,
        create_nathan_gerbe,
        create_mike_richards,
    )


# From https://stackoverflow.com/a/56842689
class reversor:
    def __init__(self, obj):
        self.obj = obj

    def __eq__(self, other):
        return other.obj == self.obj

    def __lt__(self, other):
        return other.obj < self.obj


class PersonTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def create_contract_year(self, contract_id, season_id, season_name):
        data = {
            "aav": random.randint(0, 3000000),
            "cap_hit": random.randint(0, 3000000),
            "minors_salary": random.randint(0, 300000),
            "nhl_salary": random.randint(0, 3000000),
            "performance_bonuses": random.randint(0, 1000000),
            "signing_bonus": random.randint(0, 100000),
            "ntc": random.choice([True, False, False, False]),
            "nmc": random.choice([True, False, False, False]),
            "clause_limits": "".join(
                random.choices("ABCDEFGHIJKLMNOPQRSTUVWXYZ", k=16)
            ),
        }

        contract_year = ContractYear(
            contract_id=contract_id, season_id=season_id, **data
        )
        db.session.add(contract_year)
        db.session.commit()
        data["season"] = season_name
        return data

    def compare_contract_years(self, test_data, check_data):
        test_seasons = [year["season"] for year in test_data]
        check_seasons = [year["season"] for year in check_data]
        self.assertTrue(test_seasons == check_seasons)
        for idx in range(len(test_data)):
            self.assertTrue(test_data[idx]["aav"] == check_data[idx]["aav"])
            self.assertTrue(test_data[idx]["cap_hit"] == check_data[idx]["cap_hit"])
            self.assertTrue(
                test_data[idx]["minors_salary"] == check_data[idx]["minors_salary"]
            )
            self.assertTrue(
                test_data[idx]["nhl_salary"] == check_data[idx]["nhl_salary"]
            )
            self.assertTrue(
                test_data[idx]["performance_bonuses"]
                == check_data[idx]["performance_bonuses"]
            )
            self.assertTrue(
                test_data[idx]["signing_bonus"] == check_data[idx]["signing_bonus"]
            )
            self.assertTrue(test_data[idx]["nmc"] == check_data[idx]["nmc"])
            self.assertTrue(test_data[idx]["ntc"] == check_data[idx]["ntc"])
            self.assertTrue(
                test_data[idx]["clause_limits"] == check_data[idx]["clause_limits"]
            )

    def create_contract(self, person_id, active):
        asset_data = {"current_team": random.randint(0, 32), "active": active}
        team_name = "".join(random.choices("qwertyuiopasdfghjklzxcvbnm ", k=15))
        team_data = {"name": team_name}
        team = Team(**team_data)
        asset = Asset(**asset_data)
        db.session.add(asset)
        db.session.add(team)
        db.session.commit()

        asset.originating_team = team.id
        contract_data = {
            "id": asset.id,
            "person_id": person_id,
            "years": random.randint(0, 9),
            "signing_date": datetime.strptime(
                f"{random.randint(1000,9999)}-{random.randint(1,12):02}-{random.randint(1,28):02}",
                self.app.config["DATE_FMT"],
            ),
            "total_value": random.randint(0, 20000000),
            "expiration_status": random.choice(["UFA", "RFA", "10.2c", "N/A"]),
            "entry_level": random.choice([True, False]),
            "thirty_five_plus": random.choice([True, False]),
        }
        contract = Contract(**contract_data)
        db.session.add(asset)
        db.session.add(contract)
        db.session.commit()
        contract_data["expires_as"] = contract_data["expiration_status"]
        contract_data["seasons"] = contract_data["years"]
        contract_data["years"] = []
        contract_data["current"] = bool(active)
        contract_data["signing_team"] = team_name
        contract_data["signing_date"] = contract_data["signing_date"].strftime(
            self.app.config["DATE_FMT"]
        )
        contract_data["id"] = contract.id

        letters = "abcdefghijklmnopqrstuvwxyz "
        seasons = []
        free_agency_opening = date(year=2020, month=1, day=1)
        for _ in range(contract_data["seasons"]):
            season_name = "".join(random.choices(letters, k=20))
            season = Season(name=season_name, free_agency_opening=free_agency_opening)
            month = free_agency_opening.month + 1
            year = free_agency_opening.year
            if month > 12:
                year += 1
                month = 1
            free_agency_opening = free_agency_opening.replace(month=month, year=year)
            seasons.append(season)
            db.session.add(season)
            db.session.commit()
        seasons.sort(key=lambda season: season.free_agency_opening)

        for season in seasons:
            contract_data["years"].append(
                self.create_contract_year(contract.id, season.id, season.name)
            )

        return contract_data

    def compare_contracts(self, test_data, check_data):
        for idx in range(len(test_data)):
            self.assertTrue(test_data[idx]["current"] == check_data[idx]["current"])
            self.assertTrue(
                test_data[idx]["expires_as"] == check_data[idx]["expires_as"]
            )
            self.assertTrue(test_data[idx]["seasons"] == check_data[idx]["seasons"])
            self.assertTrue(
                test_data[idx]["signing_date"] == check_data[idx]["signing_date"]
            )
            self.assertTrue(
                test_data[idx]["signing_team"] == check_data[idx]["signing_team"]
            )
            self.compare_contract_years(
                test_data[idx]["years"], check_data[idx]["years"]
            )

    def create_draft_pick(self, person_id):
        asset = Asset(active=False, originating_team=random.randint(0, 32))
        team_abbreviation = "".join(random.choices("qwertyuiopasdfghjklzxcvbnm ", k=15))
        team = Team(abbreviation=team_abbreviation)
        season_name = "".join(random.choices("abcdefghijklmnopqrstuvwxyz ", k=16))
        draft_date = datetime.strptime(
            f"{random.randint(1000,9999)}-{random.randint(1,12):02}-{random.randint(1,28):02}",
            self.app.config["DATE_FMT"],
        ).date()
        season = Season(name=season_name, draft_date=draft_date)
        db.session.add(asset)
        db.session.add(team)
        db.session.add(season)
        db.session.commit()
        asset.current_team = team.id

        draft_data = {
            "id": asset.id,
            "season_id": season.id,
            "round": random.randint(1, 7),
            "position": random.randint(1, 500),
        }
        draft_pick = DraftPick(**draft_data)

        db.session.add(asset)
        db.session.add(draft_pick)
        db.session.commit()

        draft_data["year"] = draft_date.year
        draft_data["team"] = team_abbreviation
        draft_data["id"] = draft_pick.id
        return draft_data

    def create_person(self, person_id):
        draft_data = self.create_draft_pick(person_id)
        person_data = {
            "id": person_id,
            "birthdate": datetime.strptime(
                f"{random.randint(1000,9999)}-{random.randint(1,12):02}-{random.randint(1,28):02}",
                self.app.config["DATE_FMT"],
            ).date(),
            "birthplace": "".join(random.choices(" qwertyuiopasdfghjklzxcvbnm", k=15)),
            "height": random.randint(0, 100),
            "weight": random.randint(0, 400),
            "name": "".join(random.choices(" qwertyuiopasdfghjklzxcvbnm", k=15)),
            "nationality": random.choice(
                ["USA", "CAN", "IDK", "BEL", "TEL", "COMCAST"]
            ),
            "nhl_id": random.randint(8000000, 9000000),
            "number": random.randint(0, 99),
            "position": random.choice(["D", "RW", "LW", "C", "RD", "LD", "G", ""]),
            "shoots": random.choice(["L", "R", ""]),
            "draft_pick": draft_data["id"],
        }
        person = Person(**person_data)
        db.session.add(person)
        db.session.commit()

        person_data["birthdate"] = person_data["birthdate"].strftime(
            self.app.config["DATE_FMT"]
        )
        person_data["draft_position"] = draft_data["position"]
        person_data["draft_round"] = draft_data["round"]
        person_data["draft_year"] = draft_data["year"]
        person_data["draft_team"] = draft_data["team"]
        person_data["contracts"] = [
            self.create_contract(person_id, random.choice([True, False]))
            for _ in range(random.randint(1, 10))
        ]
        person_data[
            "height"
        ] = f"{person_data['height'] // 12}' {person_data['height'] % 12}\""
        person_data["weight"] = f"{person_data['weight']}lbs"
        return person_data

    def compare_person(self, test_data, check_data):
        self.assertTrue(test_data["birthplace"] == check_data["birthplace"])
        self.assertTrue(test_data["birthdate"] == check_data["birthdate"])
        self.assertTrue(test_data["draft_position"] == check_data["draft_position"])
        self.assertTrue(test_data["draft_round"] == check_data["draft_round"])
        self.assertTrue(test_data["draft_year"] == check_data["draft_year"])
        self.assertTrue(test_data["draft_team"] == check_data["draft_team"])
        self.assertTrue(test_data["height"] == check_data["height"])
        self.assertTrue(test_data["name"] == check_data["name"])
        self.assertTrue(test_data["nationality"] == check_data["nationality"])
        self.assertTrue(test_data["nhl_id"] == check_data["nhl_id"])
        self.assertTrue(test_data["number"] == check_data["number"])
        self.assertTrue(test_data["position"] == check_data["position"])
        self.assertTrue(test_data["shoots"] == check_data["shoots"])
        self.assertTrue(test_data["weight"] == check_data["weight"])
        self.compare_contracts(test_data["contracts"], check_data["contracts"])

    def test_get_contract_years(self):
        asset = Asset(originating_team=1, current_team=1, active=True)
        db.session.add(asset)
        db.session.commit()
        contract = Contract(
            id=asset.id,
            person_id=1,
            years=3,
            signing_date=datetime.strptime(
                "1998-04-04", self.app.config["DATE_FMT"]
            ).date(),
            total_value=3000000,
            expiration_status="RFA",
            entry_level=True,
            thirty_five_plus=False,
        )
        db.session.add(contract)
        db.session.commit()

        characters = "abcdefghijklmnopqrstuvwzyz "
        season_names = ["".join(random.sample(characters, k=15)) for _ in range(3)]
        season_one = Season(
            name=season_names[0], free_agency_opening=date(year=2000, month=7, day=1)
        )
        season_two = Season(
            name=season_names[1], free_agency_opening=date(year=2001, month=7, day=1)
        )
        season_three = Season(
            name=season_names[2], free_agency_opening=date(year=2002, month=7, day=1)
        )
        db.session.add(season_three)
        db.session.add(season_one)
        db.session.add(season_two)
        db.session.commit()

        seasons_ordered = [
            [season_one.name, season_one.id],
            [season_two.name, season_two.id],
            [season_three.name, season_three.id],
        ]

        check_data = [
            self.create_contract_year(contract.id, season[1], season[0])
            for season in seasons_ordered
        ]
        test_data = person.get_contract_years(contract.id)

        self.compare_contract_years(test_data, check_data)

    def test_get_contracts(self):
        person_record = Person()
        db.session.add(person_record)
        db.session.commit()
        check_contracts = [
            self.create_contract(person_record.id, bool(random.randint(0, 1)))
            for _ in range(random.randint(1, 8))
        ]
        check_contracts.sort(
            key=lambda contract: (
                not contract["current"],
                contract["current"] * contract["signing_date"],
                reversor(contract["signing_date"]),
                reversor(contract["id"]),
            )
        )
        """
            Asset.active.desc(),
            Asset.active*Contract.signing_date.asc(),
            Contract.signing_date.desc(),
            Contract.id.desc()
        """
        test_contracts = person.get_contracts(person_record.id)
        self.compare_contracts(test_contracts, check_contracts)

    def test_get_data(self):
        person_id = random.randint(0, 5000)
        check_data = self.create_person(person_id)
        check_data["contracts"].sort(
            key=lambda contract: (
                not contract["current"],
                contract["current"] * contract["signing_date"],
                reversor(contract["signing_date"]),
                reversor(contract["id"]),
            )
        )
        test_data = person.get_data(person_id)
        self.compare_person(test_data, check_data)

    def test_get_url_person_nhlid(self):
        test_person = {
            "nhl_id": "1234567",
            "name": "NHL ID Test",
            "birthplace": "First Ave, Minneapolis",
            "nationality": "Armenian",
            "position": "D",
            "shoots": "R",
        }
        to_db = Person(**test_person)
        db.session.add(to_db)
        db.session.commit()

        # String NHLID because data from URLs is a string
        query_person, lookup_type = person.get_url_person(
            test_person["nhl_id"], "whocares"
        )
        self.assertEqual("NHLID", lookup_type)
        self.assertEqual(int(test_person["nhl_id"]), query_person.nhl_id)
        self.assertEqual(test_person["name"], query_person.name)
        self.assertEqual(test_person["birthplace"], query_person.birthplace)
        self.assertEqual(test_person["nationality"], query_person.nationality)
        self.assertEqual(test_person["position"], query_person.position)
        self.assertEqual(test_person["shoots"], query_person.shoots)

    def test_get_url_person_id(self):
        test_person = {
            "nhl_id": "1234567",
            "name": "ID Test",
            "birthplace": "First Ave, Minneapolis",
            "nationality": "Armenian",
            "position": "D",
            "shoots": "R",
        }
        to_db = Person(**test_person)
        db.session.add(to_db)
        db.session.commit()

        # String ID because data from URLs is a string
        query_person, lookup_type = person.get_url_person(str(to_db.id), "whocares")
        self.assertEqual("ID", lookup_type)
        self.assertEqual(int(test_person["nhl_id"]), query_person.nhl_id)
        self.assertEqual(test_person["name"], query_person.name)
        self.assertEqual(test_person["birthplace"], query_person.birthplace)
        self.assertEqual(test_person["nationality"], query_person.nationality)
        self.assertEqual(test_person["position"], query_person.position)
        self.assertEqual(test_person["shoots"], query_person.shoots)

    def test_get_url_person_name(self):
        test_person = {
            "nhl_id": "1234567",
            "name": "Name Test",
            "birthplace": "First Ave, Minneapolis",
            "nationality": "Armenian",
            "position": "D",
            "shoots": "R",
        }
        to_db = Person(**test_person)
        db.session.add(to_db)
        db.session.commit()

        # String ID because data from URLs is a string
        query_person, lookup_type = person.get_url_person("name-test", "whocares")
        self.assertEqual("NAME", lookup_type)
        self.assertEqual(int(test_person["nhl_id"]), query_person.nhl_id)
        self.assertEqual(test_person["name"], query_person.name)
        self.assertEqual(test_person["birthplace"], query_person.birthplace)
        self.assertEqual(test_person["nationality"], query_person.nationality)
        self.assertEqual(test_person["position"], query_person.position)
        self.assertEqual(test_person["shoots"], query_person.shoots)

    def test_get_url_person_namestub_match(self):
        test_person = {
            "nhl_id": "1234567",
            "name": "NAme Test",
            "birthplace": "First Ave, Minneapolis",
            "nationality": "Armenian",
            "position": "D",
            "shoots": "R",
        }
        to_db = Person(**test_person)
        db.session.add(to_db)
        db.session.commit()

        # String ID because data from URLs is a string
        query_person, lookup_type = person.get_url_person("name-test", "whocares")
        self.assertEqual("NAMESTUB", lookup_type)
        self.assertEqual(int(test_person["nhl_id"]), query_person.nhl_id)
        self.assertEqual(test_person["name"], query_person.name)
        self.assertEqual(test_person["birthplace"], query_person.birthplace)
        self.assertEqual(test_person["nationality"], query_person.nationality)
        self.assertEqual(test_person["position"], query_person.position)
        self.assertEqual(test_person["shoots"], query_person.shoots)

    def test_get_url_person_namestub_no_match(self):
        test_person = {
            "nhl_id": "1234567",
            "name": "NAme Test",
            "birthplace": "First Ave, Minneapolis",
            "nationality": "Armenian",
            "position": "D",
            "shoots": "R",
        }
        to_db = Person(**test_person)
        db.session.add(to_db)
        db.session.commit()

        try:
            # String ID because data from URLs is a string
            person.get_url_person("mame-test", "whocares")
        except NotFound as e:
            self.assertEqual(e.code, 404)

    def test_get_url_person_namestub_multiple_matches(self):
        test_person_one = {
            "nhl_id": "1234567",
            "name": "nNAme Test",
            "birthplace": "First Ave, Minneapolis",
            "nationality": "Armenian",
            "position": "D",
            "shoots": "R",
        }
        test_person_two = {
            "nhl_id": "1234568",
            "name": "NAme Testt",
            "birthplace": "First Ave, Minneapolis",
            "nationality": "Armenian",
            "position": "D",
            "shoots": "R",
        }
        to_db_one = Person(**test_person_one)
        to_db_two = Person(**test_person_two)
        db.session.add(to_db_one)
        db.session.add(to_db_two)
        db.session.commit()

        try:
            person.get_url_person("ame-tes", "whocares")
        except InternalServerError as e:
            self.assertEqual(e.code, 500)

    def test_get_draft_pick_drafted(self):
        asset = {
            "originating_team": 1,
            "current_team": 1,
            "active": False,
        }
        season = {
            "name": "Test Season",
            "draft_date": date.today(),
        }
        team = {
            "name": "Test Team",
            "abbreviation": "TST",
        }
        db_asset = Asset(**asset)
        db_season = Season(**season)
        db_team = Team(**team)
        db.session.add(db_asset)
        db.session.add(db_season)
        db.session.add(db_team)
        db.session.commit()

        draft_pick = {
            "id": db_asset.id,
            "round": random.randint(1, 7),
            "position": 88,
            "season_id": db_season.id,
        }
        test_person = {
            "nhl_id": "1234567",
            "name": "nNAme Test",
            "birthplace": "First Ave, Minneapolis",
            "nationality": "Armenian",
            "position": "D",
            "shoots": "R",
            "draft_pick": db_asset.id,
        }
        db_person = Person(**test_person)
        db_pick = DraftPick(**draft_pick)
        db.session.add(db_pick)
        db.session.add(db_person)
        db.session.commit()
        draft_pick["year"] = season["draft_date"].year
        draft_pick["team"] = "TST"

        pick = person.get_draft_pick(test_person["draft_pick"])
        self.assertEqual(pick["draft_round"], draft_pick["round"])
        self.assertEqual(pick["draft_position"], draft_pick["position"])
        self.assertEqual(pick["draft_year"], draft_pick["year"])
        self.assertEqual(pick["draft_team"], draft_pick["team"])

    def test_get_draft_pick_undrafted(self):
        test_person = {
            "nhl_id": "1234567",
            "name": "nNAme Test",
            "birthplace": "First Ave, Minneapolis",
            "nationality": "Armenian",
            "position": "D",
            "shoots": "R",
        }
        db_person = Person(**test_person)
        db.session.add(db_person)
        db.session.commit()

        pick = person.get_draft_pick(db_person.draft_pick)
        self.assertEqual(pick["draft_round"], "N/A")
        self.assertEqual(pick["draft_position"], "N/A")
        self.assertEqual(pick["draft_team"], "N/A")
        self.assertEqual(pick["draft_year"], "Undrafted")

    def test_ep(self):
        nolan = Person(name="Owen Nolan", id=17626)
        cached = CachedContent(
            record_table="person",
            record_id=nolan.id,
            expiration=datetime.now() + timedelta(days=14),
            content="Test content. Blah",
        )
        db.session.add(nolan)
        db.session.add(cached)
        db.session.commit()

        self.assertEqual(
            person.get_ep_stats(nolan.id, nolan.ep_id), "Test content. Blah"
        )
        cached.expiration = datetime.now() - timedelta(days=14)
        db.session.add(cached)
        db.session.commit()

        data = person.get_ep_stats(nolan.id, nolan.ep_id)
        self.assertEqual(data, "")

        nolan.ep_id = 9674
        db.session.add(nolan)
        db.session.commit()
        data = person.get_ep_stats(nolan.id, nolan.ep_id)
        self.assertTrue("Nordiques" in data)
        self.assertTrue("Sharks" in data)
        self.assertTrue("ZSC" in data)
        self.assertTrue("Thorold Blackhawks U15 BB" in data)

    def test_jared_cowen(self):
        db_data = create_jared_cowen(db)
        lookup_data = person.get_data(db_data["person"].id)
        self.assertEqual(
            db_data["person"].birthdate.strftime(self.app.config["DATE_FMT"]),
            lookup_data["birthdate"],
        )
        self.assertEqual(db_data["person"].birthplace, lookup_data["birthplace"])
        self.assertEqual(db_data["person"].name, lookup_data["name"])
        self.assertEqual(db_data["person"].nationality, lookup_data["nationality"])
        self.assertEqual(db_data["person"].nhl_id, lookup_data["nhl_id"])
        self.assertEqual(db_data["person"].number, lookup_data["number"])
        self.assertEqual(db_data["person"].position, lookup_data["position"])
        self.assertEqual(db_data["person"].position, lookup_data["position"])
        self.assertEqual(len(db_data["contracts"]), len(lookup_data["contracts"]))
        self.assertEqual(
            db_data["assets"][0].active, lookup_data["contracts"][1]["current"]
        )
        self.assertEqual(db_data["contracts"][0].id, lookup_data["contracts"][1]["id"])
        self.assertEqual(
            db_data["contracts"][0].years, lookup_data["contracts"][1]["seasons"]
        )
        self.assertEqual(
            db_data["contracts"][0].signing_date.strftime(self.app.config["DATE_FMT"]),
            lookup_data["contracts"][1]["signing_date"],
        )
        signing_team = [
            team.name
            for team in db_data["teams"]
            if team.id == db_data["assets"][0].originating_team
        ][0]
        self.assertEqual(signing_team, lookup_data["contracts"][1]["signing_team"])
        self.assertEqual(
            db_data["contracts"][0].total_value,
            lookup_data["contracts"][1]["total_value"],
        )
        self.assertFalse("buyout_date" in lookup_data["contracts"][1])
        self.assertNotEqual(
            db_data["contracts"][0].years, len(lookup_data["contracts"][1]["years"])
        )
        self.assertEqual(
            db_data["contract_years"][0].entry_level_slide,
            lookup_data["contracts"][1]["years"][0]["entry_level_slide"],
        )
        self.assertFalse("minors_salary" in lookup_data["contracts"][1]["years"][0])
        self.assertFalse("nhl_salary" in lookup_data["contracts"][1]["years"][0])
        self.assertFalse(
            "performance_bonuses" in lookup_data["contracts"][1]["years"][0]
        )
        self.assertEqual(
            db_data["contract_years"][0].aav,
            lookup_data["contracts"][1]["years"][0]["aav"],
        )
        self.assertEqual(
            db_data["contract_years"][0].cap_hit,
            lookup_data["contracts"][1]["years"][0]["cap_hit"],
        )
        self.assertEqual(
            db_data["contract_years"][0].signing_bonus,
            lookup_data["contracts"][1]["years"][0]["signing_bonus"],
        )
        for idx in range(1, 4):
            self.assertEqual(
                db_data["contract_years"][idx].aav,
                lookup_data["contracts"][1]["years"][idx]["aav"],
            )
            self.assertEqual(
                db_data["contract_years"][idx].bought_out,
                lookup_data["contracts"][1]["years"][idx]["bought_out"],
            )
            self.assertEqual(
                db_data["contract_years"][idx].cap_hit,
                lookup_data["contracts"][1]["years"][idx]["cap_hit"],
            )
            self.assertEqual(
                db_data["contract_years"][idx].minors_salary,
                lookup_data["contracts"][1]["years"][idx]["minors_salary"],
            )
            self.assertEqual(
                db_data["contract_years"][idx].nhl_salary,
                lookup_data["contracts"][1]["years"][idx]["nhl_salary"],
            )
            self.assertEqual(
                db_data["contract_years"][idx].performance_bonuses,
                lookup_data["contracts"][1]["years"][idx]["performance_bonuses"],
            )
            self.assertEqual(
                db_data["contract_years"][idx].signing_bonus,
                lookup_data["contracts"][1]["years"][idx]["signing_bonus"],
            )
        self.assertEqual(
            db_data["assets"][1].active, lookup_data["contracts"][0]["current"]
        )
        self.assertEqual(db_data["contracts"][1].id, lookup_data["contracts"][0]["id"])
        self.assertEqual(
            db_data["contracts"][1].years, lookup_data["contracts"][0]["seasons"]
        )
        self.assertEqual(
            db_data["contracts"][1].signing_date.strftime(self.app.config["DATE_FMT"]),
            lookup_data["contracts"][0]["signing_date"],
        )
        signing_team = [
            team.name
            for team in db_data["teams"]
            if team.id == db_data["assets"][1].originating_team
        ][0]
        self.assertEqual(signing_team, lookup_data["contracts"][0]["signing_team"])
        self.assertEqual(
            db_data["contracts"][1].total_value,
            lookup_data["contracts"][0]["total_value"],
        )
        self.assertTrue("buyout_date" in lookup_data["contracts"][0])
        self.assertEqual(
            db_data["contracts"][1].buyout_date.strftime(self.app.config["DATE_FMT"]),
            lookup_data["contracts"][0]["buyout_date"],
        )
        self.assertEqual(
            db_data["contracts"][1].years, len(lookup_data["contracts"][0]["years"])
        )
        for idx in range(4):
            self.assertEqual(
                db_data["contract_years"][4 + idx].aav,
                lookup_data["contracts"][0]["years"][idx]["aav"],
            )
            self.assertEqual(
                db_data["contract_years"][4 + idx].bought_out,
                lookup_data["contracts"][0]["years"][idx]["bought_out"],
            )
            self.assertEqual(
                db_data["contract_years"][4 + idx].cap_hit,
                lookup_data["contracts"][0]["years"][idx]["cap_hit"],
            )
            self.assertEqual(
                db_data["contract_years"][4 + idx].minors_salary,
                lookup_data["contracts"][0]["years"][idx]["minors_salary"],
            )
            self.assertEqual(
                db_data["contract_years"][4 + idx].nhl_salary,
                lookup_data["contracts"][0]["years"][idx]["nhl_salary"],
            )
            self.assertEqual(
                db_data["contract_years"][4 + idx].performance_bonuses,
                lookup_data["contracts"][0]["years"][idx]["performance_bonuses"],
            )
            self.assertEqual(
                db_data["contract_years"][4 + idx].signing_bonus,
                lookup_data["contracts"][0]["years"][idx]["signing_bonus"],
            )
        self.assertEqual(len(lookup_data["contracts"][0]["buyout_years"]), 2)
        self.assertEqual(
            db_data["buyout_years"][0].cap_hit,
            lookup_data["contracts"][0]["buyout_years"][0]["cap_hit"],
        )
        self.assertEqual(
            db_data["buyout_years"][0].nhl_salary,
            lookup_data["contracts"][0]["buyout_years"][0]["nhl_salary"],
        )
        self.assertEqual(
            db_data["buyout_years"][0].signing_bonus,
            lookup_data["contracts"][0]["buyout_years"][0]["signing_bonus"],
        )
        self.assertEqual(
            db_data["buyout_years"][1].cap_hit,
            lookup_data["contracts"][0]["buyout_years"][1]["cap_hit"],
        )
        self.assertEqual(
            db_data["buyout_years"][1].nhl_salary,
            lookup_data["contracts"][0]["buyout_years"][1]["nhl_salary"],
        )
        self.assertEqual(
            db_data["buyout_years"][1].signing_bonus,
            lookup_data["contracts"][0]["buyout_years"][1]["signing_bonus"],
        )

    def compare_real_contract(self, asset, contract, lookup, teams):
        self.assertEqual(asset.active, lookup["current"])
        self.assertEqual(contract.id, lookup["id"])
        self.assertEqual(contract.years, lookup["seasons"])
        self.assertEqual(
            contract.signing_date.strftime(self.app.config["DATE_FMT"]),
            lookup["signing_date"],
        )
        signing_team = [
            team.name for team in teams if team.id == asset.originating_team
        ][0]
        self.assertEqual(signing_team, lookup["signing_team"])
        self.assertEqual(contract.total_value, lookup["total_value"])

    def compare_real_contract_years(self, contract_years, lookup):
        for idx in range(len(contract_years)):
            self.assertEqual(contract_years[idx].aav, lookup[idx]["aav"])
            self.assertEqual(contract_years[idx].bought_out, lookup[idx]["bought_out"])
            self.assertEqual(contract_years[idx].terminated, lookup[idx]["terminated"])
            self.assertEqual(contract_years[idx].cap_hit, lookup[idx]["cap_hit"])
            self.assertEqual(
                contract_years[idx].minors_salary, lookup[idx]["minors_salary"]
            )
            self.assertEqual(contract_years[idx].nhl_salary, lookup[idx]["nhl_salary"])
            self.assertEqual(
                contract_years[idx].performance_bonuses,
                lookup[idx]["performance_bonuses"],
            )
            self.assertEqual(
                contract_years[idx].signing_bonus, lookup[idx]["signing_bonus"]
            )

    def test_zach_parise(self):
        db_data = create_zach_parise(db)
        lookup_data = person.get_data(db_data["person"].id)
        self.assertEqual(
            db_data["person"].birthdate.strftime(self.app.config["DATE_FMT"]),
            lookup_data["birthdate"],
        )
        self.assertEqual(db_data["person"].birthplace, lookup_data["birthplace"])
        self.assertEqual(db_data["person"].name, lookup_data["name"])
        self.assertEqual(db_data["person"].nationality, lookup_data["nationality"])
        self.assertEqual(db_data["person"].nhl_id, lookup_data["nhl_id"])
        self.assertEqual(db_data["person"].number, lookup_data["number"])
        self.assertEqual(db_data["person"].position, lookup_data["position"])
        self.assertEqual(db_data["person"].position, lookup_data["position"])
        self.assertEqual(len(db_data["contracts"]), len(lookup_data["contracts"]))
        idx = 4
        contract = db_data["contracts"][0]
        asset = db_data["assets"][0]
        contract_years = db_data["contract_years"][:2]
        self.assertNotEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        idx = 3
        contract = db_data["contracts"][1]
        asset = db_data["assets"][1]
        contract_years = db_data["contract_years"][2:5]
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        idx = 2
        contract = db_data["contracts"][2]
        asset = db_data["assets"][2]
        contract_years = db_data["contract_years"][6:7]
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        idx = 1
        contract = db_data["contracts"][3]
        asset = db_data["assets"][3]
        contract_years = db_data["contract_years"][7:20]
        self.assertTrue("buyout_date" in lookup_data["contracts"][idx])
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        idx = 0
        contract = db_data["contracts"][4]
        asset = db_data["assets"][4]
        contract_years = db_data["contract_years"][-1:]
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

    def test_jost(self):
        player = Person(
            name="Tyson Jost",
            number=17,
            birthdate=date(year=1998, month=3, day=14),
            birthplace="St. Albert, AB, CAN",
            nationality="CAN",
            shoots="L",
            position="C",
            height=71,
            weight=187,
            nhl_id="87654321",
        )
        seasons = [
            Season(
                name="2018-2019", free_agency_opening=date(year=2018, month=7, day=1)
            ),
            Season(
                name="2019-2020", free_agency_opening=date(year=2019, month=7, day=1)
            ),
            Season(
                name="2020-2021", free_agency_opening=date(year=2020, month=7, day=1)
            ),
            Season(
                name="2021-2022", free_agency_opening=date(year=2021, month=7, day=1)
            ),
        ]
        team = Team(name="Colorado Avalanche")
        db.session.add(player)
        db.session.add(team)
        for season in seasons:
            db.session.add(season)
        db.session.flush()
        assets = [
            Asset(originating_team=team.id, current_team=team.id, active=False),
            Asset(originating_team=team.id, current_team=team.id, active=False),
            Asset(originating_team=team.id, current_team=team.id, active=True),
            Asset(originating_team=team.id, current_team=team.id, active=True),
        ]
        for asset in assets:
            db.session.add(asset)
        db.session.flush()
        contracts = [
            Contract(
                id=assets[0].id,
                person_id=player.id,
                signing_date=date(year=2018, month=7, day=15),
                total_value=975000,
                expiration_status="RFA",
                entry_level=True,
                years=1,
                extension=False,
            ),
            Contract(
                id=assets[1].id,
                person_id=player.id,
                signing_date=date(year=2019, month=8, day=16),
                total_value=975000,
                expiration_status="RFA",
                entry_level=False,
                years=1,
                extension=False,
            ),
            Contract(
                id=assets[2].id,
                person_id=player.id,
                signing_date=date(year=2020, month=9, day=18),
                total_value=874125,
                expiration_status="RFA",
                entry_level=False,
                years=1,
                extension=False,
            ),
            Contract(
                id=assets[3].id,
                person_id=player.id,
                signing_date=date(year=2021, month=10, day=19),
                total_value=5000000,
                expiration_status="UFA",
                entry_level=False,
                years=1,
                extension=True,
            ),
        ]
        for contract in contracts:
            db.session.add(contract)
        db.session.flush()

        contract_years = [
            ContractYear(
                contract_id=contracts[0].id,
                season_id=seasons[0].id,
                aav=975000,
                cap_hit=975000,
                minors_salary=80000,
                nhl_salary=975000,
                performance_bonuses=0,
                earned_performance_bonuses=0,
                signing_bonus=0,
                bought_out=False,
                two_way=True,
            ),
            ContractYear(
                contract_id=contracts[1].id,
                season_id=seasons[1].id,
                aav=975000,
                cap_hit=975000,
                minors_salary=975000,
                nhl_salary=975000,
                performance_bonuses=0,
                earned_performance_bonuses=0,
                signing_bonus=0,
                bought_out=False,
                two_way=False,
            ),
            ContractYear(
                contract_id=contracts[2].id,
                season_id=seasons[2].id,
                aav=874125,
                cap_hit=874125,
                nhl_salary=874125,
                minors_salary=874125,
                performance_bonuses=0,
                earned_performance_bonuses=0,
                signing_bonus=0,
                bought_out=False,
                two_way=True,
            ),
            ContractYear(
                contract_id=contracts[3].id,
                season_id=seasons[3].id,
                aav=5000000,
                cap_hit=5000000,
                minors_salary=5000000,
                nhl_salary=5000000,
                performance_bonuses=0,
                earned_performance_bonuses=0,
                signing_bonus=0,
                bought_out=False,
                two_way=False,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()

        data = person.get_data(player.id)
        self.assertFalse("error" in data)
        self.assertEqual(data["id"], player.id)
        self.assertEqual(data["name"], "Tyson Jost")
        self.assertEqual(data["birthdate"], "1998-03-14")
        self.assertEqual(data["birthplace"], "St. Albert, AB, CAN")
        self.assertEqual(data["nationality"], "CAN")
        self.assertEqual(data["shoots"], "L")
        self.assertEqual(data["number"], 17)
        self.assertEqual(data["position"], "C")
        self.assertEqual(data["height"], "5' 11\"")
        self.assertEqual(data["weight"], "187lbs")
        self.assertEqual(data["nhl_id"], 87654321)
        self.assertEqual(data["draft_year"], "Undrafted")
        self.assertEqual(data["draft_team"], "N/A")
        self.assertEqual(data["draft_round"], "N/A")
        self.assertEqual(data["draft_position"], "N/A")

        self.assertEqual(len(data["contracts"]), 4)
        self.assertEqual(data["contracts"][0]["seasons"], 1)
        self.assertEqual(data["contracts"][0]["expires_as"], "RFA")
        self.assertEqual(data["contracts"][0]["total_value"], 874125)
        self.assertEqual(data["contracts"][0]["signing_date"], "2020-09-18")
        self.assertEqual(data["contracts"][0]["signing_team"], "Colorado Avalanche")
        self.assertEqual(data["contracts"][0]["entry_level"], False)
        self.assertEqual(data["contracts"][0]["thirty_five_plus"], False)
        self.assertEqual(data["contracts"][0]["id"], contracts[2].id)
        self.assertFalse("buyout_date" in data["contracts"][0])
        self.assertEqual(data["contracts"][0]["extension"], False)
        self.assertEqual(data["contracts"][0]["current"], True)
        self.assertEqual(data["contracts"][0]["years"][0]["season"], "2020-2021")
        self.assertEqual(data["contracts"][0]["years"][0]["cap_hit"], 874125)
        self.assertEqual(data["contracts"][0]["years"][0]["aav"], 874125)
        self.assertEqual(data["contracts"][0]["years"][0]["nhl_salary"], 874125)
        self.assertEqual(data["contracts"][0]["years"][0]["minors_salary"], 874125)
        self.assertEqual(data["contracts"][0]["years"][0]["signing_bonus"], 0)
        self.assertEqual(data["contracts"][0]["years"][0]["performance_bonuses"], 0)
        self.assertEqual(data["contracts"][0]["years"][0]["bought_out"], False)
        self.assertFalse("entry_level_slide" in data["contracts"][0]["years"][0])
        self.assertEqual(data["contracts"][0]["years"][0]["nmc"], False)
        self.assertEqual(data["contracts"][0]["years"][0]["ntc"], False)
        self.assertEqual(data["contracts"][0]["years"][0]["clause_limits"], None)

        self.assertEqual(data["contracts"][1]["seasons"], 1)
        self.assertEqual(data["contracts"][1]["expires_as"], "UFA")
        self.assertEqual(data["contracts"][1]["total_value"], 5000000)
        self.assertEqual(data["contracts"][1]["signing_date"], "2021-10-19")
        self.assertEqual(data["contracts"][1]["signing_team"], "Colorado Avalanche")
        self.assertEqual(data["contracts"][1]["entry_level"], False)
        self.assertEqual(data["contracts"][1]["thirty_five_plus"], False)
        self.assertEqual(data["contracts"][1]["id"], contracts[3].id)
        self.assertFalse("buyout_date" in data["contracts"][1])
        self.assertEqual(data["contracts"][1]["extension"], True)
        self.assertEqual(data["contracts"][1]["current"], True)
        self.assertEqual(data["contracts"][1]["years"][0]["season"], "2021-2022")
        self.assertEqual(data["contracts"][1]["years"][0]["cap_hit"], 5000000)
        self.assertEqual(data["contracts"][1]["years"][0]["aav"], 5000000)
        self.assertEqual(data["contracts"][1]["years"][0]["nhl_salary"], 5000000)
        self.assertEqual(data["contracts"][1]["years"][0]["minors_salary"], 5000000)
        self.assertEqual(data["contracts"][1]["years"][0]["signing_bonus"], 0)
        self.assertEqual(data["contracts"][1]["years"][0]["performance_bonuses"], 0)
        self.assertEqual(data["contracts"][1]["years"][0]["bought_out"], False)
        self.assertFalse("entry_level_slide" in data["contracts"][1]["years"][0])
        self.assertEqual(data["contracts"][1]["years"][0]["nmc"], False)
        self.assertEqual(data["contracts"][1]["years"][0]["ntc"], False)
        self.assertEqual(data["contracts"][1]["years"][0]["clause_limits"], None)

        self.assertEqual(data["contracts"][2]["seasons"], 1)
        self.assertEqual(data["contracts"][2]["expires_as"], "RFA")
        self.assertEqual(data["contracts"][2]["total_value"], 975000)
        self.assertEqual(data["contracts"][2]["signing_date"], "2019-08-16")
        self.assertEqual(data["contracts"][2]["signing_team"], "Colorado Avalanche")
        self.assertEqual(data["contracts"][2]["entry_level"], False)
        self.assertEqual(data["contracts"][2]["thirty_five_plus"], False)
        self.assertEqual(data["contracts"][2]["id"], contracts[1].id)
        self.assertFalse("buyout_date" in data["contracts"][2])
        self.assertEqual(data["contracts"][2]["extension"], False)
        self.assertEqual(data["contracts"][2]["current"], False)
        self.assertEqual(data["contracts"][2]["years"][0]["season"], "2019-2020")
        self.assertEqual(data["contracts"][2]["years"][0]["cap_hit"], 975000)
        self.assertEqual(data["contracts"][2]["years"][0]["aav"], 975000)
        self.assertEqual(data["contracts"][2]["years"][0]["nhl_salary"], 975000)
        self.assertEqual(data["contracts"][2]["years"][0]["minors_salary"], 975000)
        self.assertEqual(data["contracts"][2]["years"][0]["signing_bonus"], 0)
        self.assertEqual(data["contracts"][2]["years"][0]["performance_bonuses"], 0)
        self.assertEqual(data["contracts"][2]["years"][0]["bought_out"], False)
        self.assertFalse("entry_level_slide" in data["contracts"][2]["years"][0])
        self.assertEqual(data["contracts"][2]["years"][0]["nmc"], False)
        self.assertEqual(data["contracts"][2]["years"][0]["ntc"], False)
        self.assertEqual(data["contracts"][2]["years"][0]["clause_limits"], None)

        self.assertEqual(data["contracts"][3]["seasons"], 1)
        self.assertEqual(data["contracts"][3]["expires_as"], "RFA")
        self.assertEqual(data["contracts"][3]["total_value"], 975000)
        self.assertEqual(data["contracts"][3]["signing_date"], "2018-07-15")
        self.assertEqual(data["contracts"][3]["signing_team"], "Colorado Avalanche")
        self.assertEqual(data["contracts"][3]["entry_level"], True)
        self.assertEqual(data["contracts"][3]["thirty_five_plus"], False)
        self.assertEqual(data["contracts"][3]["id"], contracts[0].id)
        self.assertFalse("buyout_date" in data["contracts"][3])
        self.assertEqual(data["contracts"][3]["extension"], False)
        self.assertEqual(data["contracts"][3]["current"], False)
        self.assertEqual(data["contracts"][3]["years"][0]["season"], "2018-2019")
        self.assertEqual(data["contracts"][3]["years"][0]["cap_hit"], 975000)
        self.assertEqual(data["contracts"][3]["years"][0]["aav"], 975000)
        self.assertEqual(data["contracts"][3]["years"][0]["nhl_salary"], 975000)
        self.assertEqual(data["contracts"][3]["years"][0]["minors_salary"], 80000)
        self.assertEqual(data["contracts"][3]["years"][0]["signing_bonus"], 0)
        self.assertEqual(data["contracts"][3]["years"][0]["performance_bonuses"], 0)
        self.assertEqual(data["contracts"][3]["years"][0]["bought_out"], False)
        self.assertFalse("entry_level_slide" in data["contracts"][3]["years"][0])
        self.assertEqual(data["contracts"][3]["years"][0]["nmc"], False)
        self.assertEqual(data["contracts"][3]["years"][0]["ntc"], False)
        self.assertEqual(data["contracts"][3]["years"][0]["clause_limits"], None)

    def test_nathan_gerbe(self):
        db_data = create_nathan_gerbe(db)
        lookup_data = person.get_data(db_data["person"].id)

        self.assertEqual(
            db_data["person"].birthdate.strftime(self.app.config["DATE_FMT"]),
            lookup_data["birthdate"],
        )
        self.assertEqual(db_data["person"].birthplace, lookup_data["birthplace"])
        self.assertEqual(db_data["person"].name, lookup_data["name"])
        self.assertEqual(db_data["person"].nationality, lookup_data["nationality"])
        self.assertEqual(db_data["person"].nhl_id, lookup_data["nhl_id"])
        self.assertEqual(db_data["person"].number, lookup_data["number"])
        self.assertEqual(db_data["person"].position, lookup_data["position"])
        self.assertEqual(db_data["person"].position, lookup_data["position"])
        self.assertEqual(len(db_data["contracts"]), len(lookup_data["contracts"]))

        idx = 0
        contract = db_data["contracts"][7 - idx]
        asset = db_data["assets"][7 - idx]
        contract_years = db_data["contract_years"][13:]
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        idx += 1
        contract = db_data["contracts"][7 - idx]
        asset = db_data["assets"][7 - idx]
        contract_years = db_data["contract_years"][11:13]
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        idx += 1
        contract = db_data["contracts"][7 - idx]
        asset = db_data["assets"][7 - idx]
        contract_years = db_data["contract_years"][10:11]
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        idx += 1
        contract = db_data["contracts"][7 - idx]
        asset = db_data["assets"][7 - idx]
        contract_years = db_data["contract_years"][9:10]
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        idx += 1
        contract = db_data["contracts"][7 - idx]
        asset = db_data["assets"][7 - idx]
        contract_years = db_data["contract_years"][7:9]
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        idx += 1
        contract = db_data["contracts"][7 - idx]
        asset = db_data["assets"][7 - idx]
        contract_years = db_data["contract_years"][6:7]
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        idx += 1
        contract = db_data["contracts"][7 - idx]
        asset = db_data["assets"][7 - idx]
        contract_years = db_data["contract_years"][3:6]
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertTrue("buyout_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        idx += 1
        contract = db_data["contracts"][7 - idx]
        asset = db_data["assets"][7 - idx]
        contract_years = db_data["contract_years"][:3]
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

    def test_mike_richards(self):
        db_data = create_mike_richards(db)
        lookup_data = person.get_data(db_data["person"].id)

        self.assertEqual(
            db_data["person"].birthdate.strftime(self.app.config["DATE_FMT"]),
            lookup_data["birthdate"],
        )
        self.assertEqual(db_data["person"].birthplace, lookup_data["birthplace"])
        self.assertEqual(db_data["person"].name, lookup_data["name"])
        self.assertEqual(db_data["person"].nationality, lookup_data["nationality"])
        self.assertEqual(db_data["person"].nhl_id, lookup_data["nhl_id"])
        self.assertEqual(db_data["person"].number, lookup_data["number"])
        self.assertEqual(db_data["person"].position, lookup_data["position"])
        self.assertEqual(db_data["person"].position, lookup_data["position"])
        self.assertEqual(len(db_data["contracts"]), len(lookup_data["contracts"]))

        idx = 0
        contract = db_data["contracts"][2 - idx]
        asset = db_data["assets"][2 - idx]
        contract_years = db_data["contract_years"][16:]
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.assertFalse("termination_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        idx += 1
        contract = db_data["contracts"][2 - idx]
        asset = db_data["assets"][2 - idx]
        contract_years = db_data["contract_years"][3:15]
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.assertTrue("termination_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        idx += 1
        contract = db_data["contracts"][2 - idx]
        asset = db_data["assets"][2 - idx]
        contract_years = db_data["contract_years"][:3]
        self.assertEqual(contract.years, len(lookup_data["contracts"][idx]["years"]))
        self.assertFalse("buyout_date" in lookup_data["contracts"][idx])
        self.assertFalse("termination_date" in lookup_data["contracts"][idx])
        self.compare_real_contract(
            asset, contract, lookup_data["contracts"][idx], db_data["teams"]
        )
        self.compare_real_contract_years(
            contract_years, lookup_data["contracts"][idx]["years"]
        )

        penalties = lookup_data["contracts"][1]["penalty_years"]
        self.assertEqual(len(db_data["penalties"]), 2)
        self.assertTrue("Cap Recapture" in [value["type"] for value in penalties])
        self.assertTrue("Termination Fees" in [value["type"] for value in penalties])
        cap_rec = penalties[0]["years"]
        term_fee = penalties[1]["years"]
        self.assertEqual(len(cap_rec) + len(term_fee), len(db_data["penalty_years"]))
        db_cap_rec = db_data["penalty_years"][:5]
        db_term_fee = db_data["penalty_years"][5:]
        for idx in range(len(cap_rec)):
            self.assertEqual(cap_rec[idx]["cap_hit"], 1320000)
            self.assertEqual(
                cap_rec[idx]["season"], Season.query.get(db_cap_rec[idx].season_id).name
            )
        for idx in range(len(term_fee)):
            self.assertEqual(term_fee[idx]["cap_hit"], db_term_fee[idx].cap_hit)
            self.assertEqual(
                term_fee[idx]["season"],
                Season.query.get(db_term_fee[idx].season_id).name,
            )

    def test_get_current_team(self):
        PERSON_ID = 4877
        data = person.get_current_team(PERSON_ID)
        self.assertEqual(data["name"], None)
        self.assertEqual(data["logo"], None)

        db.session.add(
            Person(
                id=PERSON_ID,
                name="idek",
            )
        )
        db.session.add(
            Asset(
                id=88,
                current_team=28,
                type="Signing Rights",
                active=True,
            )
        )
        db.session.add(
            SigningRights(
                id=88,
                person_id=PERSON_ID,
            )
        )
        db.session.add(
            Team(
                id=28,
                logo="img/test.svg",
                name="Team Test",
            )
        )
        db.session.commit()

        data = person.get_current_team(PERSON_ID)
        self.assertEqual(data["name"], "Team Test")
        self.assertEqual(data["logo"], "img/test.svg")

    def test_get_professional_tryouts(self):
        PERSON_ID = 283
        TEAM_ID = 99
        self.assertEqual(person.get_professional_tryouts(PERSON_ID), [])

        db.session.add(Team(id=TEAM_ID, name="Test Team"))
        db.session.add(Person(id=PERSON_ID, name="Dummy Person"))
        db.session.add(Asset(id=33, type="Professional Tryout", current_team=TEAM_ID))
        db.session.add(Season(id=self.app.config["CURRENT_SEASON"], name="Test Season"))
        db.session.add(
            ProfessionalTryout(
                id=33,
                person_id=PERSON_ID,
                season_id=self.app.config["CURRENT_SEASON"],
                signing_date=date.today(),
            )
        )
        db.session.commit()

        ptos = person.get_professional_tryouts(PERSON_ID)
        self.assertEqual(len(ptos), 1)
        pto = ptos.pop()
        self.assertEqual(pto["signing_team"], "Test Team")
        self.assertEqual(pto["signing_date"], date.today().strftime("%Y-%m-%d"))
        self.assertTrue(pto["active"])


if __name__ == "__main__":
    unittest.main(verbosity=2)
