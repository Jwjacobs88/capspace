import sys
import random
import unittest
from wtforms.validators import ValidationError
from app import create_app, db
from app.models import User

from app.user.forms import RegistrationForm, EditProfileForm

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class UserTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_reg_validate_username(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = RegistrationForm()
        form.username.data = "ig"
        self.assertIsNone(form.validate_username(form.username))

        user = User(username="ig")
        db.session.add(user)
        db.session.commit()

        with self.assertRaises(ValidationError) as exc:
            form.validate_username(form.username)
            self.assertEqual(
                exc.exception,
                "Username ig already registered. Please choose a different username",
            )

        ctx.pop()

    def test_reg_validate_email(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = RegistrationForm()
        form.email.data = "myexample@fake.domain"
        self.assertIsNone(form.validate_email(form.email))

        user = User(email="myexample@fake.domain")
        db.session.add(user)
        db.session.commit()

        with self.assertRaises(ValidationError) as exc:
            form.validate_username(form.username)
            self.assertEqual(
                exc.exception,
                "An account already exists for myexample@fake.domain. Please use a different email address",
            )

        ctx.pop()

    def test_update_validate_username(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = EditProfileForm("ig")

        form.username.data = "ig"
        self.assertIsNone(form.validate_username(form.username))

        user = User(username="ig")
        db.session.add(user)
        db.session.commit()
        self.assertIsNone(form.validate_username(form.username))

        form.username.data = "ig2"
        self.assertIsNone(form.validate_username(form.username))

        user = User(username="ig2")
        db.session.add(user)
        db.session.commit()

        with self.assertRaises(ValidationError) as exc:
            form.validate_username(form.username)
            self.assertEqual(
                exc.exception,
                "Username ig2 already registered. Please choose a different username",
            )

        ctx.pop()


if __name__ == "__main__":
    unittest.main(verbosity=2)
