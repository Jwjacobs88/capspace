import sys
import json
import random
import pathlib
import unittest
from unittest.mock import patch
from datetime import datetime, date
from app import create_app, db
from app.models import (
    Person,
    GoalieSeason,
    SkaterSeason,
    Season,
    Asset,
    Contract,
    ContractYear,
    Alias,
    Transaction,
    TransactionAsset,
    Team,
)

from app.admin import update_person

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


def sample_goalie_stats():
    cwd = pathlib.Path(__file__).resolve().parent
    file_name = cwd / "sample_goalie_stats.json"
    with open(file_name) as file:
        return json.loads(file.read())


def sample_skater_stats():
    cwd = pathlib.Path(__file__).resolve().parent
    file_name = cwd / "sample_skater_stats.json"
    with open(file_name) as file:
        return json.loads(file.read())


class UpdatePersonTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_validate_person(self):
        no_person = update_person.validate_person("Test")
        self.assertTrue("error" in no_person)
        self.assertEqual(no_person["error"], "Identifier Test belongs to 0 people")

        person = Person(
            name="Test",
            nhl_id=654,
            birthdate=date(year=2019, month=4, day=4),
            birthplace="nowhere",
            nationality="TEXAS",
            position="E",
            number=44,
            weight=55,
            current_status="hungry,drafted",
            active=True,
            captain=False,
            rookie=True,
            alternate_captain=False,
            non_roster=True,
        )
        db.session.add(person)
        yes_person = update_person.validate_person("Test")
        self.assertEqual(yes_person["name"], person.name)
        self.assertEqual(yes_person["id"], person.id)
        self.assertEqual(yes_person["nhl_id"], person.nhl_id)
        self.assertEqual(
            yes_person["birthdate"],
            person.birthdate.strftime(self.app.config["DATE_FMT"]),
        )
        self.assertEqual(yes_person["birthplace"], person.birthplace)
        self.assertEqual(yes_person["nationality"], person.nationality)
        self.assertEqual(yes_person["position"], person.position)
        self.assertEqual(yes_person["number"], person.number)
        self.assertEqual(yes_person["weight"], person.weight)
        self.assertEqual(yes_person["current_status"], person.current_status or "")
        self.assertEqual(yes_person["active"], person.active)
        self.assertEqual(yes_person["rookie"], person.rookie)
        self.assertEqual(yes_person["captain"], person.captain)
        self.assertEqual(yes_person["alternate_captain"], person.alternate_captain)
        self.assertEqual(yes_person["non_roster"], person.non_roster)

    def test_update_person(self):
        CONTRACT = 237
        TEAM = 1111
        PERSON = 5
        ctx = self.app.test_request_context()
        ctx.push()
        form = update_person.UpdatePerson()
        db.session.add(
            Person(
                id=PERSON,
                name="Test",
                nhl_id=4321,
                position="RW",
                number=88,
                weight=150,
                current_status="LTIR",
                active=True,
                rookie=False,
                captain=True,
                alternate_captain=True,
                non_roster=False,
            )
        )
        db.session.add(
            Asset(
                id=CONTRACT,
                active=True,
                current_team=TEAM,
                originating_team=TEAM,
                type="Contract",
            )
        )
        db.session.add(
            Contract(
                id=CONTRACT,
                person_id=PERSON,
            )
        )
        db.session.add(
            ContractYear(
                contract_id=CONTRACT,
                season_id=self.app.config["CURRENT_SEASON"],
            )
        )
        db.session.commit()

        person = Person.query.filter(Person.id == 5).first()
        self.assertIsNotNone(person)
        self.assertEqual(person.name, "Test")
        self.assertEqual(person.id, 5)
        self.assertEqual(person.nhl_id, 4321)
        self.assertEqual(person.weight, 150)
        self.assertEqual(person.number, 88)
        self.assertEqual(person.active, True)
        self.assertEqual(person.rookie, False)
        self.assertEqual(person.captain, True)
        self.assertEqual(person.alternate_captain, True)
        self.assertEqual(person.non_roster, False)
        self.assertEqual(person.current_status, "LTIR")
        self.assertEqual(Transaction.query.count(), 0)

        form.id.data = 5
        form.weight.data = 140
        form.number.data = 100
        form.active.data = False
        form.rookie.data = True
        form.captain.data = False
        form.alternate_captain.data = False
        form.non_roster.data = True
        form.current_status.data = ["Drafted", "IR"]
        form.update_person()

        self.assertEqual(person.name, "Test")
        self.assertEqual(person.id, 5)
        self.assertEqual(person.nhl_id, 4321)
        self.assertEqual(person.weight, 140)
        self.assertEqual(person.number, 100)
        self.assertEqual(person.active, False)
        self.assertEqual(person.rookie, True)
        self.assertEqual(person.captain, False)
        self.assertEqual(person.alternate_captain, False)
        self.assertEqual(person.non_roster, True)
        self.assertTrue("IR" in person.current_status)
        self.assertTrue("Drafted" in person.current_status)
        self.assertEqual(Transaction.query.count(), 1)
        transaction = Transaction.query.first()
        self.assertEqual(transaction.type, "Placed on IR")

        ctx.pop()

    def test_set_values(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = update_person.UpdatePerson()

        person = Person(
            name="Test",
            nhl_id=654,
            birthdate=date(year=2019, month=4, day=4),
            birthplace="nowhere",
            nationality="TEXAS",
            position="E",
            number=44,
            weight=55,
            current_status="hungry,drafted",
            active=True,
            captain=False,
            rookie=True,
            alternate_captain=False,
            non_roster=True,
        )
        db.session.add(person)
        db.session.commit()

        data = update_person.validate_person("Test")
        self.assertIsNone(form.person.data)
        self.assertIsNone(form.id.data)
        self.assertIsNone(form.nhl_id.data)
        self.assertIsNone(form.number.data)
        self.assertIsNone(form.weight.data)
        self.assertIsNone(form.current_status.data)
        self.assertTrue(form.active.data)
        self.assertTrue(form.rookie.data)
        self.assertFalse(form.captain.data)
        self.assertFalse(form.alternate_captain.data)
        self.assertTrue(form.non_roster.data)

        form.set_values(data)
        self.assertEqual(form.person.data, data["id"])
        self.assertEqual(form.id.data, data["id"])
        self.assertEqual(form.nhl_id.data, data["nhl_id"])
        self.assertEqual(form.number.data, data["number"])
        self.assertEqual(form.weight.data, data["weight"])
        self.assertEqual(form.current_status.data, data["current_status"].split(","))
        self.assertEqual(form.active.data, data["active"])
        self.assertEqual(form.rookie.data, data["rookie"])
        self.assertEqual(form.captain.data, data["captain"])
        self.assertEqual(form.alternate_captain.data, data["alternate_captain"])
        self.assertEqual(form.non_roster.data, data["non_roster"])

        ctx.pop()

    def test_validate_ep_id(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = update_person.UpdatePerson()
        db.session.add(
            Person(
                id=6,
                ep_id=88,
                name="Test",
                nhl_id=4321,
                position="RW",
                number=88,
                weight=150,
                current_status="LTIR",
                active=True,
                rookie=False,
                captain=True,
                alternate_captain=True,
                non_roster=False,
            )
        )
        db.session.add(
            Person(
                id=5,
                name="Test",
                nhl_id=4322,
                position="RW",
                number=88,
                weight=150,
                current_status="LTIR",
                active=True,
                rookie=False,
                captain=True,
                alternate_captain=True,
                non_roster=False,
            )
        )
        db.session.commit()

        form.id.data = 5
        form.weight.data = 140
        form.number.data = 100
        form.active.data = False
        form.rookie.data = True
        form.captain.data = False
        form.alternate_captain.data = False
        form.non_roster.data = True
        form.current_status.data = ["Drafted"]
        self.assertTrue(form.validate_ep_id(form.ep_id))
        form.ep_id.data = 89
        self.assertTrue(form.validate_ep_id(form.ep_id))
        form.ep_id.data = 88
        form.ep_id.errors = list(form.ep_id.errors)
        self.assertFalse(form.validate_ep_id(form.ep_id))
        self.assertEqual(
            form.ep_id.errors[0], "EliteProspects ID already found in database"
        )

        ctx.pop()

    @patch(
        "app.admin.update_person.get_person_stats", new=lambda x: sample_goalie_stats()
    )
    def test_update_goalie_stats(self):
        PERSON = 29
        TEAM = Team(id="54", abbreviation="VGK")
        db.session.add(TEAM)
        db.session.commit()
        self.assertEqual(GoalieSeason.query.count(), 0)
        update_person.update_goalie_stats(PERSON)
        self.assertEqual(GoalieSeason.query.count(), 1)
        stats_record = GoalieSeason.query.first()
        self.assertEqual(stats_record.starts, 9)
        self.assertEqual(stats_record.wins, 7)
        self.assertEqual(stats_record.losses, 1)
        self.assertEqual(stats_record.overtime_losses, 1)
        self.assertEqual(stats_record.shutouts, 2)
        self.assertEqual(stats_record.shots_against, 262)
        self.assertEqual(stats_record.goals_against, 16)
        self.assertEqual(stats_record.games_played, 9)

    @patch(
        "app.admin.update_person.get_person_stats", new=lambda x: sample_skater_stats()
    )
    def test_update_skater_stats(self):
        PERSON = 29
        SJS = Team(id="28", abbreviation="SJS")
        MIN = Team(id="30", abbreviation="MIN")
        db.session.add(SJS)
        db.session.add(MIN)
        db.session.commit()
        self.assertEqual(SkaterSeason.query.count(), 0)
        update_person.update_skater_stats(PERSON)
        self.assertEqual(SkaterSeason.query.count(), 2)
        stats_record = SkaterSeason.query.filter(SkaterSeason.team_id == MIN.id).first()
        self.assertEqual(stats_record.goals, 0)
        self.assertEqual(stats_record.assists, 5)
        self.assertEqual(stats_record.points, 5)
        self.assertEqual(stats_record.games_played, 12)
        self.assertEqual(stats_record.shots, 18)
        self.assertEqual(stats_record.penalty_minutes, 6)
        stats_record = SkaterSeason.query.filter(SkaterSeason.team_id == SJS.id).first()
        self.assertEqual(stats_record.goals, 0)
        self.assertEqual(stats_record.assists, 0)
        self.assertEqual(stats_record.points, 0)
        self.assertEqual(stats_record.games_played, 2)
        self.assertEqual(stats_record.shots, 4)
        self.assertEqual(stats_record.penalty_minutes, 0)

    def test_update_skater_waiver_exemption(self):
        PERSON = 11483
        CONTRACT = 182
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, None))
        seasons = [
            Season(
                id=23,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2000, month=1, day=1),
            ),
            Season(
                id=13,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2001, month=1, day=1),
            ),
            Season(
                id=3,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2002, month=1, day=1),
            ),
            Season(
                id=24,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2003, month=1, day=1),
            ),
            Season(
                id=29,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2004, month=1, day=1),
            ),
            Season(
                id=22,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2005, month=1, day=1),
            ),
            Season(
                id=18,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2006, month=1, day=1),
            ),
        ]
        skater_seasons = [
            SkaterSeason(person_id=PERSON, season_id=seasons[0].id, games_played=10),
            SkaterSeason(person_id=PERSON, season_id=seasons[1].id, games_played=10),
            SkaterSeason(person_id=PERSON, season_id=seasons[2].id, games_played=10),
            SkaterSeason(person_id=PERSON, season_id=seasons[3].id, games_played=10),
            SkaterSeason(person_id=PERSON, season_id=seasons[4].id, games_played=10),
            SkaterSeason(person_id=PERSON, season_id=seasons[5].id, games_played=10),
            SkaterSeason(person_id=PERSON, season_id=seasons[6].id, games_played=10),
        ]
        asset = Asset(
            id=CONTRACT,
        )
        contract = Contract(
            id=CONTRACT,
            person_id=PERSON,
            signing_season_id=23,
        )
        db.session.add(skater_seasons[0])
        db.session.add(seasons[0])
        db.session.add(asset)
        db.session.add(contract)
        db.session.commit()

        # age >= 25 and seasons > 1
        self.assertTrue(update_person.update_skater_waiver_exemption(PERSON, "25"))
        db.session.add(skater_seasons[1])
        db.session.add(seasons[1])
        db.session.commit()
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "25"))

        # age == 24 and (seasons > 2 or games > 60):
        self.assertTrue(update_person.update_skater_waiver_exemption(PERSON, "24"))
        skater_seasons[1].games_played = 51
        db.session.add(skater_seasons[1])
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "24"))
        skater_seasons[1].games_played = 10
        db.session.add(skater_seasons[1])
        db.session.add(skater_seasons[2])
        db.session.add(seasons[2])
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "24"))

        # Games played
        # age == 19 and data[0][0] > 10 and (seasons > 3 or games > 160):
        # age == 18 and data[0][0] > 10 and (seasons > 3 or games > 160):
        skater_seasons[0].games_played = 11
        db.session.add(skater_seasons[0])
        db.session.commit()
        self.assertTrue(update_person.update_skater_waiver_exemption(PERSON, "19"))
        self.assertTrue(update_person.update_skater_waiver_exemption(PERSON, "18"))
        skater_seasons[1].games_played = 150
        db.session.add(skater_seasons[1])
        db.session.commit()
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "19"))
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "18"))
        skater_seasons[1].games_played = 10
        db.session.add(skater_seasons[1])
        db.session.commit()

        # age == 23 and (seasons > 3 or games > 60):
        # age == 22 and (seasons > 3 or games > 70):
        # age == 21 and (seasons > 3 or games > 80):
        # age == 20 and (seasons > 3 or games > 160):
        self.assertTrue(update_person.update_skater_waiver_exemption(PERSON, "23"))
        self.assertTrue(update_person.update_skater_waiver_exemption(PERSON, "22"))
        self.assertTrue(update_person.update_skater_waiver_exemption(PERSON, "21"))
        self.assertTrue(update_person.update_skater_waiver_exemption(PERSON, "20"))
        skater_seasons[1].games_played = 150
        db.session.add(skater_seasons[1])
        db.session.commit()
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "23"))
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "22"))
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "21"))
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "20"))
        skater_seasons[1].games_played = 10
        db.session.add(skater_seasons[1])
        db.session.add(skater_seasons[3])
        db.session.add(seasons[3])
        db.session.commit()
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "23"))
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "22"))
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "21"))
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "20"))

        # Seasons
        # age == 19 and data[0][0] > 10 and (seasons > 3 or games > 160):
        # age == 18 and data[0][0] > 10 and (seasons > 3 or games > 160):
        skater_seasons[0].games_played = 11
        db.session.add(skater_seasons[0])
        db.session.commit()
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "19"))
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "18"))

        # age == 19 and (seasons > 4 or games > 160):
        # age == 18 and (seasons > 5 or games > 160):
        skater_seasons[0].games_played = 10
        db.session.add(skater_seasons[0])
        db.session.commit()
        self.assertTrue(update_person.update_skater_waiver_exemption(PERSON, "19"))
        skater_seasons[1].games_played = 150
        db.session.add(skater_seasons[1])
        db.session.commit()
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "19"))
        skater_seasons[1].games_played = 10
        db.session.add(skater_seasons[1])
        db.session.add(skater_seasons[4])
        db.session.add(seasons[4])
        db.session.commit()
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "19"))
        self.assertTrue(update_person.update_skater_waiver_exemption(PERSON, "18"))
        skater_seasons[1].games_played = 150
        db.session.add(skater_seasons[1])
        db.session.commit()
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "18"))
        skater_seasons[1].games_played = 10
        db.session.add(skater_seasons[1])
        db.session.add(skater_seasons[5])
        db.session.add(seasons[5])
        db.session.commit()
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "18"))

        db.session.add(skater_seasons[6])
        db.session.add(seasons[6])
        db.session.commit()
        self.assertFalse(update_person.update_skater_waiver_exemption(PERSON, "18"))

    def test_update_goalie_waiver_exemption(self):
        PERSON = 11483
        CONTRACT = 348
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, None))
        seasons = [
            Season(
                id=23,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2000, month=1, day=1),
            ),
            Season(
                id=13,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2001, month=1, day=1),
            ),
            Season(
                id=3,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2002, month=1, day=1),
            ),
            Season(
                id=24,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2003, month=1, day=1),
            ),
            Season(
                id=29,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2004, month=1, day=1),
            ),
            Season(
                id=22,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2005, month=1, day=1),
            ),
            Season(
                id=18,
                free_agency_opening=date.today(),
                regular_season_start=date(year=2006, month=1, day=1),
            ),
        ]
        goalie_seasons = [
            GoalieSeason(person_id=PERSON, season_id=seasons[0].id, games_played=10),
            GoalieSeason(person_id=PERSON, season_id=seasons[1].id, games_played=10),
            GoalieSeason(person_id=PERSON, season_id=seasons[2].id, games_played=10),
            GoalieSeason(person_id=PERSON, season_id=seasons[3].id, games_played=10),
            GoalieSeason(person_id=PERSON, season_id=seasons[4].id, games_played=10),
            GoalieSeason(person_id=PERSON, season_id=seasons[5].id, games_played=10),
            GoalieSeason(person_id=PERSON, season_id=seasons[6].id, games_played=10),
        ]
        asset = Asset(
            id=CONTRACT,
        )
        contract = Contract(
            id=CONTRACT,
            person_id=PERSON,
            signing_season_id=23,
        )
        db.session.add(asset)
        db.session.add(contract)
        db.session.add(seasons[0])
        db.session.add(goalie_seasons[0])
        db.session.commit()

        # age >= 25 and seasons > 1
        self.assertTrue(update_person.update_goalie_waiver_exemption(PERSON, "25"))
        db.session.add(goalie_seasons[1])
        db.session.add(seasons[1])
        db.session.commit()
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "25"))

        # age == 24 and (seasons > 2 or games > 60):
        self.assertTrue(update_person.update_goalie_waiver_exemption(PERSON, "24"))
        goalie_seasons[1].games_played = 51
        db.session.add(goalie_seasons[1])
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "24"))
        goalie_seasons[1].games_played = 10
        db.session.add(goalie_seasons[1])
        db.session.add(goalie_seasons[2])
        db.session.add(seasons[2])
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "24"))

        # age == 23 and (seasons > 3 or games > 60):
        self.assertTrue(update_person.update_goalie_waiver_exemption(PERSON, "23"))
        goalie_seasons[1].games_played = 150
        db.session.add(goalie_seasons[1])
        db.session.commit()
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "23"))
        goalie_seasons[1].games_played = 10
        db.session.add(goalie_seasons[1])
        db.session.add(goalie_seasons[3])
        db.session.add(seasons[3])
        db.session.commit()
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "23"))

        # Games played
        # age == 19 and data[0][0] > 10 and (seasons > 4 or games > 80):
        # age == 18 and data[0][0] > 10 and (seasons > 4 or games > 80):
        goalie_seasons[0].games_played = 11
        db.session.add(goalie_seasons[0])
        db.session.commit()
        self.assertTrue(update_person.update_goalie_waiver_exemption(PERSON, "19"))
        self.assertTrue(update_person.update_goalie_waiver_exemption(PERSON, "18"))
        goalie_seasons[1].games_played = 150
        db.session.add(goalie_seasons[1])
        db.session.commit()
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "19"))
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "18"))
        goalie_seasons[1].games_played = 10
        db.session.add(goalie_seasons[1])
        db.session.commit()

        # age == 22 and (seasons > 4 or games > 60):
        # age == 21 and (seasons > 4 or games > 60):
        # age == 20 and (seasons > 4 or games > 80):
        self.assertTrue(update_person.update_goalie_waiver_exemption(PERSON, "22"))
        self.assertTrue(update_person.update_goalie_waiver_exemption(PERSON, "21"))
        self.assertTrue(update_person.update_goalie_waiver_exemption(PERSON, "20"))
        goalie_seasons[1].games_played = 150
        db.session.add(goalie_seasons[1])
        db.session.commit()
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "22"))
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "21"))
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "20"))
        goalie_seasons[1].games_played = 10
        db.session.add(goalie_seasons[1])
        db.session.add(goalie_seasons[4])
        db.session.add(seasons[4])
        db.session.commit()
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "22"))
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "21"))
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "20"))

        # Seasons
        # age == 19 and data[0][0] > 10 and (seasons > 4 or games > 160):
        # age == 18 and data[0][0] > 10 and (seasons > 4 or games > 160):
        goalie_seasons[0].games_played = 11
        db.session.add(goalie_seasons[0])
        db.session.commit()
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "19"))
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "18"))

        # age == 19 and (seasons > 5 or games > 160):
        # age == 18 and (seasons > 6 or games > 160):
        goalie_seasons[0].games_played = 10
        db.session.add(goalie_seasons[0])
        db.session.commit()
        self.assertTrue(update_person.update_goalie_waiver_exemption(PERSON, "19"))
        goalie_seasons[1].games_played = 150
        db.session.add(goalie_seasons[1])
        db.session.commit()
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "19"))
        goalie_seasons[1].games_played = 10
        db.session.add(goalie_seasons[1])
        db.session.add(goalie_seasons[5])
        db.session.add(seasons[5])
        db.session.commit()
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "19"))
        self.assertTrue(update_person.update_goalie_waiver_exemption(PERSON, "18"))
        goalie_seasons[1].games_played = 150
        db.session.add(goalie_seasons[1])
        db.session.commit()
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "18"))
        goalie_seasons[1].games_played = 10
        db.session.add(goalie_seasons[1])
        db.session.add(goalie_seasons[6])
        db.session.add(seasons[6])
        db.session.commit()
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "18"))

        db.session.add(goalie_seasons[6])
        db.session.commit()
        self.assertFalse(update_person.update_goalie_waiver_exemption(PERSON, "18"))

    def test_contract_exempt(self):
        assets = [
            Asset(id=24, type="Contract", active=True),
            Asset(id=52, type="Contract", active=True),
        ]
        people = [
            Person(id=298, birthdate=date(year=1990, month=4, day=3)),
            Person(id=99492, birthdate=date.today()),
        ]
        contracts = [
            Contract(
                id=assets[0].id,
                person_id=people[0].id,
                on_loan=True,
                contract_limit_exempt=True,
            ),
            Contract(
                id=assets[1].id,
                person_id=people[1].id,
                on_loan=True,
                contract_limit_exempt=False,
            ),
        ]
        stats = [
            SkaterSeason(
                season_id=self.app.config["CURRENT_SEASON"],
                person_id=people[0].id,
                games_played=20,
            ),
            GoalieSeason(
                season_id=self.app.config["CURRENT_SEASON"],
                person_id=people[1].id,
                games_played=10,
            ),
        ]
        season = Season(
            id=self.app.config["CURRENT_SEASON"],
            regular_season_start=date(year=2022, month=10, day=10),
        )
        for record in assets + people + contracts + stats + [season]:
            db.session.add(record)
        db.session.commit()

        self.assertTrue(contracts[0].contract_limit_exempt)
        self.assertFalse(contracts[1].contract_limit_exempt)
        update_person.update_contract_exempt()
        db.session.commit()
        self.assertTrue(contracts[1].contract_limit_exempt)
        self.assertFalse(contracts[0].contract_limit_exempt)

        # One's exempt and set to non-exempt. The other vice versa
        return

    def test_update_aliases(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = update_person.UpdatePerson()
        form.id.data = 83
        form.aliases.data = ["Test Two", "Test Four", "Dummy"]

        aliases = [
            Alias(person_id=form.id.data, search=False, value="Test One"),
            Alias(person_id=form.id.data, search=False, value="Test Two"),
            Alias(person_id=form.id.data, search=False, value="Test Three"),
            Alias(person_id=form.id.data, search=False, value="Dummy"),
        ]
        for record in aliases:
            db.session.add(record)
        db.session.commit()

        self.assertEqual(Alias.query.count(), 4)
        form.update_aliases()
        db.session.commit()
        self.assertEqual(Alias.query.count(), 3)
        aliases = Alias.query.order_by(Alias.value.asc()).all()
        self.assertEqual(aliases[0].value, "Dummy")
        self.assertEqual(aliases[1].value, "Test Four")
        self.assertEqual(aliases[2].value, "Test Two")

        ctx.pop()

    def test_create_transaction(self):
        PERSON = 21389
        ASSET = 888
        TEAM = 233
        db.session.add(
            Person(
                id=PERSON,
                name="Test",
                nhl_id=4321,
                position="RW",
                number=88,
                weight=150,
                current_status="LTIR",
                active=True,
                rookie=False,
                captain=True,
                alternate_captain=True,
                non_roster=False,
            )
        )
        db.session.add(
            Asset(
                id=ASSET,
                active=True,
                current_team=TEAM,
                originating_team=TEAM,
                type="Contract",
            )
        )
        db.session.add(
            Contract(
                id=ASSET,
                person_id=PERSON,
            )
        )
        db.session.add(
            ContractYear(
                contract_id=ASSET,
                season_id=self.app.config["CURRENT_SEASON"],
            )
        )
        db.session.add(
            Team(
                id=TEAM,
                name="Test Team",
                abbreviation="TT",
            )
        )
        db.session.commit()

        self.assertEqual(Transaction.query.count(), 0)
        self.assertEqual(TransactionAsset.query.count(), 0)
        update_person.create_transaction("Fake Move", PERSON)
        self.assertEqual(Transaction.query.count(), 1)
        self.assertEqual(TransactionAsset.query.count(), 1)
        transaction = Transaction.query.first()
        tr_asset = TransactionAsset.query.first()
        self.assertEqual(tr_asset.transaction_id, transaction.id)
        self.assertEqual(transaction.type, "Fake Move")
        self.assertEqual(tr_asset.new_team, TEAM)
        self.assertEqual(tr_asset.former_team, TEAM)
        self.assertEqual(tr_asset.asset_id, ASSET)

    def test_create_status_transactions(self):
        PERSON = 21389
        ASSET = 888
        TEAM = 233
        db.session.add(
            Person(
                id=PERSON,
                name="Test",
                nhl_id=4321,
                position="RW",
                number=88,
                weight=150,
                current_status="LTIR",
                active=True,
                rookie=False,
                captain=True,
                alternate_captain=True,
                non_roster=False,
            )
        )
        db.session.add(
            Asset(
                id=ASSET,
                active=True,
                current_team=TEAM,
                originating_team=TEAM,
                type="Contract",
            )
        )
        db.session.add(
            Contract(
                id=ASSET,
                person_id=PERSON,
            )
        )
        db.session.add(
            ContractYear(
                contract_id=ASSET,
                season_id=self.app.config["CURRENT_SEASON"],
            )
        )
        db.session.add(
            Team(
                id=TEAM,
                name="Test Team",
                abbreviation="TT",
            )
        )
        db.session.commit()

        self.assertEqual(Transaction.query.count(), 0)
        self.assertEqual(TransactionAsset.query.count(), 0)
        update_person.create_status_transactions(
            {"LTIR", "Under Contract"}, {"IR", "Under Contract"}, PERSON
        )
        self.assertEqual(Transaction.query.count(), 1)
        self.assertEqual(TransactionAsset.query.count(), 1)
        transaction = Transaction.query.first()
        tr_asset = TransactionAsset.query.first()
        self.assertEqual(tr_asset.transaction_id, transaction.id)
        self.assertEqual(transaction.type, "Placed on IR")
        self.assertEqual(tr_asset.new_team, TEAM)
        self.assertEqual(tr_asset.former_team, TEAM)
        self.assertEqual(tr_asset.asset_id, ASSET)

        update_person.create_status_transactions(
            {"LTIR", "Under Contract"}, {"Under Contract"}, PERSON
        )
        self.assertEqual(Transaction.query.count(), 2)
        self.assertEqual(TransactionAsset.query.count(), 2)
        transaction = Transaction.query.get(2)
        tr_asset = TransactionAsset.query.get(2)
        self.assertEqual(tr_asset.transaction_id, transaction.id)
        self.assertEqual(transaction.type, "Activated from LTIR")
        self.assertEqual(tr_asset.new_team, TEAM)
        self.assertEqual(tr_asset.former_team, TEAM)
        self.assertEqual(tr_asset.asset_id, ASSET)

        update_person.create_status_transactions(
            {"Under Contract"}, {"Under Contract", "Suspended"}, PERSON
        )
        self.assertEqual(Transaction.query.count(), 3)
        self.assertEqual(TransactionAsset.query.count(), 3)
        transaction = Transaction.query.get(3)
        tr_asset = TransactionAsset.query.get(3)
        self.assertEqual(tr_asset.transaction_id, transaction.id)
        self.assertEqual(transaction.type, "Suspended")
        self.assertEqual(tr_asset.new_team, TEAM)
        self.assertEqual(tr_asset.former_team, TEAM)
        self.assertEqual(tr_asset.asset_id, ASSET)


if __name__ == "__main__":
    unittest.main(verbosity=2)
