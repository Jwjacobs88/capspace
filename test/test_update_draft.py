import sys
import json
import random
import unittest
from datetime import date, datetime, timedelta
from app import create_app, db
from app.models import (
    Team,
    Season,
    Person,
    Asset,
    DraftPick,
    SigningRights,
    Note,
    Transaction,
    TransactionAsset,
)

from app.admin import update_draft as draft

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "


class UpdateDraftTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def create_season(self):
        data = {
            "name": "".join(random.choices(LETTERS, k=9)),
            "salary_cap": random.randint(1, 100000000),
            "salary_floor": random.randint(1, 50000000),
            "minimum_salary": random.randint(1, 1500000),
            "max_buriable_hit": random.randint(1, 2000000),
            "bonus_overage_limit": random.randint(1, 10000000),
            "draft_date": datetime.strptime(
                f"{random.randint(1950,2100)}-{random.randint(1,12):02}-{random.randint(1,28):02}",
                self.app.config["DATE_FMT"],
            ).date(),
        }
        if random.randint(1, 100) > 90:
            del data["draft_date"]
        season = Season(**data)
        db.session.add(season)
        db.session.commit()
        data["id"] = season.id
        return data

    def create_teams(self):
        to_create = random.randint(5, 20)
        team_data = []
        for _ in range(to_create):
            data = {
                "name": "".join(random.choices(LETTERS, k=16)),
                "general_manager": "".join(random.choices(LETTERS, k=16)),
                "head_coach": "".join(random.choices(LETTERS, k=16)),
                "abbreviation": "".join(random.choices(LETTERS, k=3)),
                "team_name": "".join(random.choices(LETTERS, k=8)),
                "team_location": "".join(random.choices(LETTERS, k=8)),
                "first_year": random.randint(1890, 2100),
                "logo": "".join(random.choices(LETTERS, k=500)),
            }
            while (
                Team.query.filter(Team.abbreviation == data["abbreviation"]).count()
                != 0
            ):
                data["abbreviation"] = "".join(random.choices(LETTERS, k=3))
            team_row = Team(**data)
            db.session.add(team_row)
            db.session.commit()
            data["id"] = team_row.id
            team_data.append(data)
        return team_data

    def create_draft_picks(self, season_id):
        to_create = random.randint(50, 100)
        pick_data = []
        team_data = self.create_teams()
        for _ in range(to_create):
            asset_data = {
                "current_team": random.choice(team_data)["id"],
                "originating_team": random.choice(team_data)["id"],
            }
            asset = Asset(**asset_data)
            db.session.add(asset)
            db.session.flush()
            person_data = None
            if random.randint(1, 10) > 5:
                person_data = {
                    "name": "".join(random.choices(LETTERS, k=16)),
                    "draft_pick": asset.id,
                }
                person = Person(**person_data)
                db.session.add(person)
                db.session.flush()
                person_data["id"] = person.id
            draft_pick_data = {
                "id": asset.id,
                "season_id": season_id,
                "round": random.randint(1, 7),
                "position": random.randint(1, 250),
            }
            if random.randint(1, 10) > 5:
                del draft_pick_data["position"]
            if person_data:
                draft_pick_data["person_id"] = person_data["id"]
            draft_pick = DraftPick(**draft_pick_data)
            db.session.add(draft_pick)
            db.session.flush()
            draft_pick_data["player"] = None
            draft_pick_data["person_id"] = None
            if person_data:
                draft_pick_data["player"] = person_data["name"]
                draft_pick_data["person_id"] = person_data["id"]
            team_abbreviation = ""
            original_abbreviation = ""
            for team in team_data:
                if team["id"] == asset.current_team:
                    team_abbreviation = team["abbreviation"]
                if team["id"] == asset.originating_team:
                    original_abbreviation = team["abbreviation"]
            if team_abbreviation == original_abbreviation:
                original_abbreviation = None
            draft_pick_data["team"] = team_abbreviation
            draft_pick_data["original"] = original_abbreviation
            if "position" not in draft_pick_data:
                draft_pick_data["position"] = None
            pick_data.append(draft_pick_data)
        db.session.commit()
        pick_data.sort(
            key=lambda pick: (
                pick["round"],
                pick["position"] or 1000,
                pick["original"] or pick["team"],
            )
        )
        # Round, Position (nulls last), Originating team's abbreviation
        return pick_data

    # draft.py tests
    def test_get_draft_picks_current_season(self):
        season = self.create_season()
        self.assertEqual(self.app.config["CURRENT_SEASON"], season["id"])
        picks = self.create_draft_picks(self.app.config["CURRENT_SEASON"])
        query_picks = draft.get_draft_picks()
        self.assertEqual(len(picks), len(query_picks))
        for idx in range(len(picks)):
            self.assertEqual(picks[idx]["round"], query_picks[idx]["round"])
            self.assertEqual(picks[idx]["position"], query_picks[idx]["position"])
            self.assertEqual(picks[idx]["team"], query_picks[idx]["team"])
            self.assertEqual(picks[idx]["original"], query_picks[idx]["original"])
            self.assertEqual(picks[idx]["player"], query_picks[idx]["player"])
            self.assertEqual(picks[idx]["person_id"], query_picks[idx]["person_id"])

    def test_get_draft_picks_specified_invalid_year(self):
        season = self.create_season()
        self.assertEqual(self.app.config["CURRENT_SEASON"], season["id"])
        picks = self.create_draft_picks(self.app.config["CURRENT_SEASON"])
        query_picks = draft.get_draft_picks(3000)
        self.assertEqual(len(picks), len(query_picks))
        for idx in range(len(picks)):
            self.assertEqual(picks[idx]["round"], query_picks[idx]["round"])
            self.assertEqual(picks[idx]["position"], query_picks[idx]["position"])
            self.assertEqual(picks[idx]["team"], query_picks[idx]["team"])
            self.assertEqual(picks[idx]["original"], query_picks[idx]["original"])
            self.assertEqual(picks[idx]["player"], query_picks[idx]["player"])
            self.assertEqual(picks[idx]["person_id"], query_picks[idx]["person_id"])

    def test_query_season_id(self):
        season = Season(id=939, draft_date=date(year=1993, month=10, day=31))
        db.session.add(season)
        db.session.commit()

        self.assertIsNone(draft.query_season_id(1997))
        self.assertEqual(draft.query_season_id(1993), season.id)

    # def test_get_draft_picks_specified_year(self):
    #    self.create_season()
    #    self.create_draft_picks(self.app.config["CURRENT_SEASON"])
    #    season = self.create_season()
    #    while "draft_date" not in season:
    #        season = self.create_season()
    #    self.assertFalse(season["id"]==self.app.config["CURRENT_SEASON"])
    #    picks = self.create_draft_picks(season["id"])
    #    query_picks = draft.get_draft_picks(season["draft_date"].year)
    #    self.assertEqual(len(picks), len(query_picks))
    #    for idx in range(len(picks)):
    #        self.assertEqual(picks[idx]["round"], query_picks[idx]["round"])
    #        self.assertEqual(picks[idx]["position"], query_picks[idx]["position"])
    #        self.assertEqual(picks[idx]["team"], query_picks[idx]["team"])
    #        self.assertEqual(picks[idx]["original"], query_picks[idx]["original"])
    #        self.assertEqual(picks[idx]["player"], query_picks[idx]["player"])
    #        self.assertEqual(picks[idx]["person_id"], query_picks[idx]["person_id"])

    def test_get_logos(self):
        data = self.create_teams()
        team_logos = {team["abbreviation"]: team["logo"] for team in data}
        queried_team_logos = draft.get_logos()
        self.assertEqual(len(team_logos.keys()), len(queried_team_logos.keys()))
        abbrs = team_logos.keys()
        for team in abbrs:
            self.assertEqual(team_logos[team], queried_team_logos[team])

    def test_get_pick_position(self):
        self.assertEqual(draft.get_pick_position("idk"), {"error": "No pick"})
        self.assertEqual(draft.get_pick_position(7), {"error": "No pick"})

        asset = Asset()
        db.session.add(asset)
        db.session.flush()

        pick = DraftPick(id=asset.id)
        db.session.add(pick)
        db.session.commit()
        self.assertEqual(
            draft.get_pick_position(pick.id), {"round": "", "position": ""}
        )

        pick.round = 5
        db.session.add(pick)
        db.session.commit()
        self.assertEqual(draft.get_pick_position(pick.id), {"round": 5, "position": ""})

        pick.position = 12
        db.session.add(pick)
        db.session.commit()
        self.assertEqual(draft.get_pick_position(pick.id), {"round": 5, "position": 12})

    def test_set_pick_position(self):
        set_asset = Asset()
        db.session.add(set_asset)
        db.session.flush()
        set_pick = DraftPick(id=set_asset.id, round=5, position=12)
        db.session.add(set_pick)
        db.session.commit()
        self.assertEqual(
            draft.set_pick_position(set_pick.id, set_pick.round, set_pick.position),
            "Draft pick position already occupied",
        )

        unset_asset = Asset()
        db.session.add(unset_asset)
        db.session.flush()
        unset_pick = DraftPick(id=unset_asset.id, round=4)
        db.session.add(unset_pick)
        db.session.commit()
        self.assertTrue(draft.set_pick_position(unset_pick.id, unset_pick.round, 20))

    def test_validate_name(self):
        person_one = Person()
        db.session.add(person_one)
        db.session.flush()
        person_two = Person(nhl_id=person_one.id)
        db.session.add(person_two)
        db.session.commit()
        self.assertEqual(
            draft.validate_name(person_one.id),
            {"error": f"Identifier {person_one.id} belongs to 2 people"},
        )

        person_three = Person(draft_pick=3)
        db.session.add(person_three)
        db.session.commit()
        self.assertEqual(
            draft.validate_name(person_three.id),
            {"id": person_three.id, "name": None, "nhl_id": None},
        )

        person_four = Person(name="Test Success", nhl_id=90)
        db.session.add(person_four)
        db.session.commit()
        self.assertEqual(
            draft.validate_name(person_four.id),
            {"name": "Test Success", "id": person_four.id, "nhl_id": 90},
        )

    def test_set_name(self):
        self.assertEqual(
            draft.set_name(1, 1), {"error": f"Expected: 1 update. Actual: 0 updates"}
        )

        person_one = Person()
        db.session.add(person_one)
        db.session.commit()
        self.assertEqual(draft.set_name(1, 1), {"error": "Pick not found"})

        asset = Asset()
        db.session.add(asset)
        db.session.flush()
        pick_one = DraftPick(id=asset.id, season_id=self.app.config["CURRENT_SEASON"])
        db.session.add(pick_one)
        season = Season(
            id=self.app.config["CURRENT_SEASON"],
            draft_date=date.today(),
            name="Test Season",
        )
        db.session.add(season)
        db.session.commit()
        self.assertEqual(draft.set_name(1, 1), {})
        self.assertTrue(person_one.status_is("Drafted"))
        self.assertFalse(asset.active)

        signing_rights = SigningRights.query.filter(
            SigningRights.person_id == person_one.id
        ).all()
        self.assertEqual(len(signing_rights), 1)
        self.assertEqual(signing_rights[0].expiration_date.month, 6)
        self.assertEqual(signing_rights[0].expiration_date.day, 1)

    def test_current_draft_year(self):
        seasons = [
            Season(draft_date=datetime(year=2020, month=7, day=1).date()),
            Season(draft_date=(datetime.now() + timedelta(days=55)).date()),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.commit()
        draft_date = draft.get_current_draft_year()
        self.assertEqual(draft_date, 2020)
        self.app.config["DRAFT_SEASON"] = seasons[1].id
        draft_date = draft.get_current_draft_year()
        self.assertEqual(draft_date, (datetime.now() + timedelta(days=55)).year)

    def test_get_draft_years(self):
        ctx = self.app.test_request_context()
        ctx.push()
        form = draft.SelectDraftSeason()
        seasons = [
            Season(draft_date=date(year=8372, month=1, day=1)),
            Season(draft_date=date(year=1066, month=1, day=1)),
            Season(draft_date=date(year=1993, month=1, day=1)),
            Season(draft_date=date(year=2000, month=1, day=1)),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.commit()
        self.assertEqual(form.get_draft_years(), [1066, 1993, 2000])

        ctx.pop()

    def test_create_transaction(self):
        records = [
            Season(
                id=self.app.config["CURRENT_SEASON"],
                draft_date=date(year=1999, month=7, day=2),
            ),
            Asset(
                id=1,
                type="Draft Pick",
                active=0,
                current_team=28,
                originating_team=1,
            ),
            DraftPick(
                id=1,
                season_id=self.app.config["CURRENT_SEASON"],
                round=2,
                person_id=87,
            ),
        ]
        for record in records:
            db.session.add(record)
        db.session.commit()

        self.assertEqual(Transaction.query.count(), 0)
        self.assertEqual(TransactionAsset.query.count(), 0)
        draft.create_transaction(records.pop())
        db.session.commit()
        self.assertEqual(Transaction.query.count(), 1)
        self.assertEqual(TransactionAsset.query.count(), 1)
        transaction = Transaction.query.first()
        self.assertEqual(transaction.type, "Drafted")
        self.assertEqual(
            transaction.date.date(), records[0].draft_date + timedelta(days=1)
        )
        self.assertEqual(transaction.season_id, self.app.config["CURRENT_SEASON"])
        transaction_asset = TransactionAsset.query.first()
        self.assertEqual(transaction_asset.transaction_id, transaction.id)
        self.assertEqual(transaction_asset.asset_id, records[1].id)
        self.assertEqual(transaction_asset.new_team, transaction_asset.former_team)
        self.assertEqual(transaction_asset.new_team, records[1].current_team)

    def test_add_compensatory(self):
        pick_id = 44
        team_id = 1
        records = [
            Team(
                id=team_id,
                name="Test Team",
                abbreviation="TST",
            ),
            DraftPick(
                id=pick_id,
                season_id=self.app.config["CURRENT_SEASON"],
                round=2,
                position=5,
            ),
            Asset(
                id=pick_id,
                type="Draft Pick",
                current_team=team_id,
                originating_team=team_id,
            ),
        ]
        for record in records:
            db.session.add(record)
        db.session.commit()

        draft.add_compensatory_pick("TST", 4)
        self.assertEqual(DraftPick.query.count(), 2)
        self.assertEqual(Note.query.count(), 1)
        note = Note.query.first().note
        self.assertTrue("unsigned first round" in note)
        self.assertTrue("Compensatory pick" in note)

        original = DraftPick.query.get(pick_id)
        self.assertIsNotNone(original)
        self.assertEqual(original.position, 6)

        new = DraftPick.query.filter(DraftPick.id != pick_id).first()
        self.assertIsNotNone(new)
        self.assertEqual(new.position, 4)
        self.assertEqual(new.round, 2)
        self.assertEqual(new.season_id, self.app.config["CURRENT_SEASON"])
        new_asset = Asset.query.filter(Asset.id != pick_id).first()
        self.assertIsNotNone(new_asset)
        self.assertEqual(new_asset.originating_team, team_id)
        self.assertEqual(new_asset.current_team, team_id)
        new_id = new.id

        draft.add_compensatory_pick("TST", 5)
        self.assertEqual(DraftPick.query.count(), 3)
        self.assertEqual(Note.query.count(), 2)

        original = DraftPick.query.get(pick_id)
        self.assertIsNotNone(original)
        self.assertEqual(original.position, 7)
        new = DraftPick.query.get(new_id)
        self.assertIsNotNone(new)
        self.assertEqual(new.position, 4)
        newest = DraftPick.query.filter(DraftPick.id.not_in([new_id, pick_id])).first()
        self.assertEqual(newest.position, 5)
        self.assertEqual(newest.round, 2)
        self.assertEqual(newest.season_id, self.app.config["CURRENT_SEASON"])


if __name__ == "__main__":
    unittest.main(verbosity=2)
