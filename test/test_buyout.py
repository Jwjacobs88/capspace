import sys
import random
import unittest
from datetime import datetime, date
from app import create_app, db
from app.models import (
    Team,
    Season,
    Person,
    Asset,
    DraftPick,
    Contract,
    ContractYear,
    BuyoutYear,
)
from app.main import buyout
from sqlalchemy.orm.exc import MultipleResultsFound


class BuyoutTests(unittest.TestCase):
    def setUp(self):
        print(file=sys.stderr)
        self.app = create_app("test")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def create_cba1(self):
        person = Person(
            id=4000,
            name="CBA Test 1",
            birthdate=datetime.strptime(
                f"1990-{random.randint(1,12):02}-{random.randint(1,28):02}",
                self.app.config["DATE_FMT"],
            ).date(),
        )
        team = Team(name="Test CBA1")
        seasons = [
            Season(name="AAAAA", free_agency_opening=date(year=2020, month=6, day=1)),
            Season(name="BBBBB", free_agency_opening=date(year=2020, month=6, day=2)),
            Season(name="CCCCC", free_agency_opening=date(year=2020, month=6, day=3)),
            Season(name="DDDDD", free_agency_opening=date(year=2020, month=6, day=4)),
            Season(name="EEEEE", free_agency_opening=date(year=2020, month=6, day=5)),
            Season(name="FFFFF", free_agency_opening=date(year=2020, month=6, day=6)),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.add(person)
        db.session.add(team)
        db.session.commit()
        asset = Asset(
            current_team=random.randint(0, 32), originating_team=team.id, active=True
        )
        db.session.add(asset)
        db.session.commit()

        contract = Contract(
            person_id=person.id, id=asset.id, years=3, total_value=10800000
        )
        db.session.add(contract)
        db.session.commit()

        contract_years = [
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[0].id,
                aav=3600000,
                nhl_salary=4000000,
                minors_salary=4000000,
                cap_hit=3600000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[1].id,
                aav=3600000,
                nhl_salary=3600000,
                minors_salary=3600000,
                cap_hit=3600000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[2].id,
                aav=3600000,
                nhl_salary=3200000,
                minors_salary=3200000,
                cap_hit=3600000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        return person.id, seasons[0].name

    def create_cba2(self):
        person = Person(
            id=5000,
            name="CBA Test 2",
            birthdate=datetime.strptime(
                f"1990-{random.randint(1,12):02}-{random.randint(1,28):02}",
                self.app.config["DATE_FMT"],
            ).date(),
        )
        team = Team(name="TEst CBA2")
        seasons = [
            Season(name="AAAAA", free_agency_opening=date(year=2020, month=6, day=1)),
            Season(name="BBBBB", free_agency_opening=date(year=2020, month=6, day=2)),
            Season(name="CCCCC", free_agency_opening=date(year=2020, month=6, day=3)),
            Season(name="DDDDD", free_agency_opening=date(year=2020, month=6, day=4)),
            Season(name="EEEEE", free_agency_opening=date(year=2020, month=6, day=5)),
            Season(name="FFFFF", free_agency_opening=date(year=2020, month=6, day=6)),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.add(person)
        db.session.add(team)
        db.session.commit()
        asset = Asset(
            current_team=random.randint(0, 32), originating_team=team.id, active=True
        )
        db.session.add(asset)
        db.session.commit()

        contract = Contract(
            person_id=person.id, id=asset.id, years=3, total_value=19500000
        )
        db.session.add(contract)
        db.session.commit()

        contract_years = [
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[0].id,
                aav=6500000,
                cap_hit=6500000,
                nhl_salary=6000000,
                minors_salary=6000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[1].id,
                aav=6500000,
                cap_hit=6500000,
                nhl_salary=6500000,
                minors_salary=6500000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[2].id,
                aav=6500000,
                cap_hit=6500000,
                nhl_salary=7000000,
                minors_salary=7000000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        return person.id, seasons[0].name

    def create_cba3(self):
        person = Person(
            id=6000,
            name="CBA Test 3",
            birthdate=datetime.strptime(
                f"1990-{random.randint(1,12):02}-{random.randint(1,28):02}",
                self.app.config["DATE_FMT"],
            ).date(),
        )
        team = Team(name="Test CBA3")
        seasons = [
            Season(name="AAAAA", free_agency_opening=date(year=2020, month=6, day=1)),
            Season(name="BBBBB", free_agency_opening=date(year=2020, month=6, day=2)),
            Season(name="CCCCC", free_agency_opening=date(year=2020, month=6, day=3)),
            Season(name="DDDDD", free_agency_opening=date(year=2020, month=6, day=4)),
            Season(name="EEEEE", free_agency_opening=date(year=2020, month=6, day=5)),
            Season(name="FFFFF", free_agency_opening=date(year=2020, month=6, day=6)),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.add(person)
        db.session.add(team)
        db.session.commit()
        asset = Asset(
            current_team=random.randint(0, 32), originating_team=team.id, active=True
        )
        db.session.add(asset)
        db.session.commit()

        contract = Contract(
            person_id=person.id, id=asset.id, years=3, total_value=13500000
        )
        db.session.add(contract)
        db.session.commit()

        contract_years = [
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[0].id,
                aav=4500000,
                cap_hit=4500000,
                nhl_salary=5000000,
                minors_salary=5000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[1].id,
                aav=4500000,
                cap_hit=4500000,
                nhl_salary=4500000,
                minors_salary=4500000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[2].id,
                aav=4500000,
                cap_hit=4500000,
                nhl_salary=4000000,
                minors_salary=4000000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        return person.id, seasons[0].name

    def create_jared_cowen(self):
        birthdate = date.today()
        birthdate = birthdate.replace(year=birthdate.year - 24)
        person = Person(id=6000, name="Jared Cowen", birthdate=birthdate)
        team = Team(
            name="Ottawa Senators",
        )
        seasons = [
            Season(
                name="2013-2014", free_agency_opening=date(year=2013, month=7, day=1)
            ),
            Season(
                name="2014-2015", free_agency_opening=date(year=2014, month=7, day=1)
            ),
            Season(
                name="2015-2016", free_agency_opening=date(year=2015, month=7, day=1)
            ),
            Season(
                name="2016-2017", free_agency_opening=date(year=2016, month=7, day=1)
            ),
            Season(
                name="2017-2018", free_agency_opening=date(year=2017, month=7, day=1)
            ),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.add(person)
        db.session.add(team)
        db.session.commit()
        asset = Asset(
            current_team=random.randint(0, 32), originating_team=team.id, active=True
        )
        db.session.add(asset)
        db.session.commit()

        contract = Contract(
            person_id=person.id, id=asset.id, years=4, total_value=12400000
        )
        db.session.add(contract)
        db.session.commit()
        contract_years = [
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[0].id,
                aav=3100000,
                cap_hit=3100000,
                nhl_salary=1500000,
                minors_salary=1500000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[1].id,
                aav=3100000,
                cap_hit=3100000,
                nhl_salary=2700000,
                minors_salary=2700000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[2].id,
                aav=3100000,
                cap_hit=3100000,
                nhl_salary=3700000,
                minors_salary=3700000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[3].id,
                aav=3100000,
                cap_hit=3100000,
                nhl_salary=4500000,
                minors_salary=4500000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        return person.id, seasons[2].name

    def create_zach_parise(self):
        birthdate = date.today()
        birthdate = birthdate.replace(year=birthdate.year - 36)
        person = Person(id=7000, name="Zach Parise", birthdate=birthdate)
        team = Team(
            name="Minnesota Wild",
        )
        seasons = [
            Season(
                name="2012-2013", free_agency_opening=date(year=2012, month=7, day=1)
            ),
            Season(
                name="2013-2014", free_agency_opening=date(year=2013, month=7, day=1)
            ),
            Season(
                name="2014-2015", free_agency_opening=date(year=2014, month=7, day=1)
            ),
            Season(
                name="2015-2016", free_agency_opening=date(year=2015, month=7, day=1)
            ),
            Season(
                name="2016-2017", free_agency_opening=date(year=2016, month=7, day=1)
            ),
            Season(
                name="2017-2018", free_agency_opening=date(year=2017, month=7, day=1)
            ),
            Season(
                name="2018-2019", free_agency_opening=date(year=2018, month=7, day=1)
            ),
            Season(
                name="2019-2020", free_agency_opening=date(year=2019, month=7, day=1)
            ),
            Season(
                name="2020-2021", free_agency_opening=date(year=2020, month=10, day=9)
            ),
            Season(
                name="2021-2022", free_agency_opening=date(year=2021, month=7, day=1)
            ),
            Season(
                name="2022-2023", free_agency_opening=date(year=2022, month=7, day=1)
            ),
            Season(
                name="2023-2024", free_agency_opening=date(year=2023, month=7, day=1)
            ),
            Season(
                name="2024-2025", free_agency_opening=date(year=2024, month=7, day=1)
            ),
            Season(
                name="2025-2026", free_agency_opening=date(year=2025, month=7, day=1)
            ),
            Season(
                name="2026-2027", free_agency_opening=date(year=2026, month=7, day=1)
            ),
            Season(
                name="2027-2028", free_agency_opening=date(year=2027, month=7, day=1)
            ),
            Season(
                name="2028-2029", free_agency_opening=date(year=2028, month=7, day=1)
            ),
        ]
        for season in seasons:
            db.session.add(season)
        db.session.add(person)
        db.session.add(team)
        db.session.commit()
        asset = Asset(
            current_team=random.randint(0, 32), originating_team=team.id, active=True
        )
        db.session.add(asset)
        db.session.commit()

        contract = Contract(
            person_id=person.id, id=asset.id, years=13, total_value=98000000
        )
        db.session.add(contract)
        db.session.commit()
        contract_years = [
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[0].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=2000000,
                minors_salary=12000000,
                signing_bonus=10000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[1].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=2000000,
                minors_salary=12000000,
                signing_bonus=10000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[2].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=6000000,
                minors_salary=11000000,
                signing_bonus=5000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[3].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=9000000,
                minors_salary=9000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[4].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=9000000,
                minors_salary=9000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[5].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=9000000,
                minors_salary=9000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[6].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=9000000,
                minors_salary=9000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[7].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=9000000,
                minors_salary=9000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[8].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=8000000,
                minors_salary=8000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[9].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=6000000,
                minors_salary=6000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[10].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=2000000,
                minors_salary=2000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[11].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=1000000,
                minors_salary=1000000,
            ),
            ContractYear(
                contract_id=contract.id,
                season_id=seasons[12].id,
                aav=7538462,
                cap_hit=7538462,
                nhl_salary=1000000,
                minors_salary=1000000,
            ),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        return person.id, seasons[8].name

    def test_get_data_CBA1(self):
        person, season = self.create_cba1()
        buyout_data = buyout.get_data(person, season)
        self.assertTrue(buyout_data["name"] == "CBA Test 1")
        self.assertTrue(buyout_data["buyout"]["buyout_amount"] == 4533333)
        self.assertTrue(buyout_data["buyout"]["buyout_ratio"] == 0.67)
        self.assertTrue(buyout_data["buyout"]["remaining_salary"] == 6800000)
        self.assertTrue(buyout_data["buyout"]["remaining_seasons"] == 2)
        self.assertTrue(buyout_data["buyout"]["remaining_signing_bonuses"] == 0)
        self.assertTrue(buyout_data["buyout"]["yearly_buyout_amount"] == 1133333)

        # Check buyout seasons
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_amount"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_cap_hit"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_pay"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_savings"] == 2466667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["original_cap_hit"] == 3600000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["original_salary"] == 3600000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][0]["season"] == "BBBBB")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][0]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_amount"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_cap_hit"] == 1533333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_pay"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_savings"] == 2066667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["original_cap_hit"] == 3600000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["original_salary"] == 3200000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][1]["season"] == "CCCCC")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][1]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_amount"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_cap_hit"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_pay"] == 1133333
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["buyout_savings"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["original_cap_hit"] == 0
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["original_salary"] == 0
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["season"] == "DDDDD")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_amount"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_cap_hit"] == 1133333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_pay"] == 1133333
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["buyout_savings"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["original_cap_hit"] == 0
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["original_salary"] == 0
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["season"] == "EEEEE")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["signing_bonus"] == 0)

    def test_get_data_CBA2(self):
        person, season = self.create_cba2()
        buyout_data = buyout.get_data(person, season)
        self.assertTrue(buyout_data["name"] == "CBA Test 2")
        self.assertTrue(buyout_data["buyout"]["buyout_amount"] == 9000000)
        self.assertTrue(buyout_data["buyout"]["buyout_ratio"] == 0.67)
        self.assertTrue(buyout_data["buyout"]["remaining_salary"] == 13500000)
        self.assertTrue(buyout_data["buyout"]["remaining_seasons"] == 2)
        self.assertTrue(buyout_data["buyout"]["remaining_signing_bonuses"] == 0)
        self.assertTrue(buyout_data["buyout"]["yearly_buyout_amount"] == 2250000)

        # Check buyout seasons
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_amount"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_cap_hit"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_pay"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_savings"] == 4250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["original_cap_hit"] == 6500000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["original_salary"] == 6500000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][0]["season"] == "BBBBB")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][0]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_amount"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_cap_hit"] == 1750000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_pay"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_savings"] == 4750000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["original_cap_hit"] == 6500000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["original_salary"] == 7000000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][1]["season"] == "CCCCC")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][1]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_amount"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_cap_hit"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_pay"] == 2250000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["buyout_savings"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["original_cap_hit"] == 0
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["original_salary"] == 0
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["season"] == "DDDDD")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_amount"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_cap_hit"] == 2250000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_pay"] == 2250000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["buyout_savings"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["original_cap_hit"] == 0
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["original_salary"] == 0
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["season"] == "EEEEE")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["signing_bonus"] == 0)

    def test_get_data_CBA3(self):
        person, season = self.create_cba3()
        buyout_data = buyout.get_data(person, season)
        self.assertTrue(buyout_data["name"] == "CBA Test 3")
        self.assertTrue(buyout_data["buyout"]["buyout_amount"] == 5666667)
        self.assertTrue(buyout_data["buyout"]["buyout_ratio"] == 0.67)
        self.assertTrue(buyout_data["buyout"]["remaining_salary"] == 8500000)
        self.assertTrue(buyout_data["buyout"]["remaining_seasons"] == 2)
        self.assertTrue(buyout_data["buyout"]["remaining_signing_bonuses"] == 0)
        self.assertTrue(buyout_data["buyout"]["yearly_buyout_amount"] == 1416667)

        # Check buyout seasons
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_amount"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_cap_hit"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_pay"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["buyout_savings"] == 3083333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["original_cap_hit"] == 4500000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][0]["original_salary"] == 4500000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][0]["season"] == "BBBBB")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][0]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_amount"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_cap_hit"] == 1916667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_pay"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["buyout_savings"] == 2583333
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["original_cap_hit"] == 4500000
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][1]["original_salary"] == 4000000
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][1]["season"] == "CCCCC")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][1]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_amount"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_cap_hit"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["buyout_pay"] == 1416667
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["buyout_savings"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["original_cap_hit"] == 0
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][2]["original_salary"] == 0
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["season"] == "DDDDD")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][2]["signing_bonus"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_amount"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_cap_hit"] == 1416667
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["buyout_pay"] == 1416667
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["buyout_savings"] == 0)
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["original_cap_hit"] == 0
        )
        self.assertTrue(
            buyout_data["buyout"]["buyout_years"][3]["original_salary"] == 0
        )
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["season"] == "EEEEE")
        self.assertTrue(buyout_data["buyout"]["buyout_years"][3]["signing_bonus"] == 0)

    def test_buyout_form_seasons(self):
        current = self.app.config["CURRENT_SEASON"]
        asset_id = 2347
        person_id = 347
        contract = Contract(
            id=asset_id,
            person_id=person_id,
        )
        db.session.add(contract)
        asset = Asset(
            id=asset_id,
            current_team=2,
            originating_team=3,
            active=True,
        )
        db.session.add(asset)
        seasons = [
            Season(
                id=current,
                name="AAAAA",
                free_agency_opening=date(year=2020, month=6, day=1),
            ),
            Season(
                id=current + 1,
                name="BBBBB",
                free_agency_opening=date(year=2020, month=6, day=2),
            ),
            Season(
                id=current + 2,
                name="CCCCC",
                free_agency_opening=date(year=2020, month=6, day=3),
            ),
            Season(
                id=current + 3,
                name="DDDDD",
                free_agency_opening=date(year=2020, month=6, day=4),
            ),
            Season(
                id=current + 4,
                name="EEEEE",
                free_agency_opening=date(year=2020, month=6, day=5),
            ),
            Season(
                id=current + 5,
                name="FFFFF",
                free_agency_opening=date(year=2020, month=6, day=6),
            ),
        ]
        for season in seasons:
            db.session.add(season)
        contract_years = [
            ContractYear(contract_id=contract.id, season_id=seasons[0].id),
            ContractYear(contract_id=contract.id, season_id=seasons[1].id),
            ContractYear(contract_id=contract.id, season_id=seasons[2].id),
            ContractYear(contract_id=contract.id, season_id=seasons[3].id),
            ContractYear(contract_id=contract.id, season_id=seasons[4].id),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        query = buyout.query_buyout_form_seasons(person_id)
        self.assertEqual(len(query), 5)
        self.assertEqual(query[0][0], "AAAAA")
        self.assertEqual(query[1][0], "BBBBB")
        self.assertEqual(query[2][0], "CCCCC")
        self.assertEqual(query[3][0], "DDDDD")
        self.assertEqual(query[4][0], "EEEEE")
        query = buyout.get_buyout_form_seasons(person_id)
        self.assertEqual(len(query), 5)
        self.assertEqual(query, ["AAAAA", "BBBBB", "CCCCC", "DDDDD", "EEEEE"])

        seasons = [
            Season(
                id=current + 6,
                name="IIIII",
                free_agency_opening=date(year=2020, month=6, day=7),
            ),
            Season(
                id=current + 7,
                name="HHHHH",
                free_agency_opening=date(year=2020, month=6, day=8),
            ),
            Season(
                id=current + 8,
                name="GGGGG",
                free_agency_opening=date(year=2020, month=6, day=9),
            ),
            Season(
                id=current + 9,
                name="ABABA",
                free_agency_opening=date(year=2020, month=6, day=10),
            ),
            Season(
                id=current + 10,
                name="BABAB",
                free_agency_opening=date(year=2020, month=6, day=15),
            ),
        ]
        for season in seasons:
            db.session.add(season)
        contract_years = [
            ContractYear(contract_id=contract.id, season_id=seasons[0].id),
            ContractYear(contract_id=contract.id, season_id=seasons[3].id),
        ]
        for year in contract_years:
            db.session.add(year)
        db.session.commit()
        query = buyout.query_buyout_form_seasons(person_id)
        self.assertEqual(len(query), 7)
        self.assertEqual(query[0][0], "AAAAA")
        self.assertEqual(query[1][0], "BBBBB")
        self.assertEqual(query[2][0], "CCCCC")
        self.assertEqual(query[3][0], "DDDDD")
        self.assertEqual(query[4][0], "EEEEE")
        self.assertEqual(query[5][0], "IIIII")
        self.assertEqual(query[6][0], "ABABA")
        query = buyout.get_buyout_form_seasons(person_id)
        self.assertEqual(len(query), 7)
        self.assertEqual(
            query, ["AAAAA", "BBBBB", "CCCCC", "DDDDD", "EEEEE", "IIIII", "ABABA"]
        )

    def test_buyout_candidates(self):
        person_one = Person(name="Test One")
        person_two = Person(name="Test Two")
        db.session.add(person_one)
        db.session.add(person_two)
        db.session.flush()
        asset_one = Asset(current_team=5)
        asset_two = Asset(current_team=7)
        db.session.add(asset_one)
        db.session.add(asset_two)
        db.session.flush()
        contract_one = Contract(id=asset_one.id, person_id=person_one.id)
        contract_two = Contract(id=asset_two.id, person_id=person_two.id)
        db.session.add(contract_one)
        db.session.add(contract_two)
        db.session.commit()

        query = buyout.query_buyout_candidates()
        self.assertEqual(len(query), 2)
        self.assertEqual(query[0][0], person_one.id)
        self.assertEqual(query[0][1], "Test One")
        self.assertEqual(query[1][0], person_two.id)
        self.assertEqual(query[1][1], "Test Two")
        query = buyout.query_buyout_candidates(team_id=5)
        self.assertEqual(len(query), 1)
        self.assertEqual(query[0][0], person_one.id)
        self.assertEqual(query[0][1], "Test One")

        query = buyout.get_buyout_candidates()
        self.assertEqual(len(query), 2)
        self.assertEqual(query[0][0], str(person_one.id))
        self.assertEqual(query[0][1], "Test One")
        self.assertEqual(query[1][0], str(person_two.id))
        self.assertEqual(query[1][1], "Test Two")
        query = buyout.get_buyout_candidates(team_id=5)
        self.assertEqual(len(query), 1)
        self.assertEqual(len(query[0]), 1)
        self.assertEqual(query[0][0], str(person_one.id))

    def test_get_contract_id(self):
        person = Person(name="Test")
        asset = Asset()
        db.session.add(person)
        db.session.add(asset)
        db.session.commit()
        self.assertIsNone(buyout.get_contract_id(person.id))

        contract = Contract(person_id=person.id, id=asset.id)
        db.session.add(contract)
        db.session.commit()
        self.assertEqual(buyout.get_contract_id(person.id), contract.id)


if __name__ == "__main__":
    unittest.main(verbosity=2)
