"""add ELC limit to season

Revision ID: 4f17a9c3f8c1
Revises: 209f1e305279
Create Date: 2022-04-21 17:55:14.391059

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "4f17a9c3f8c1"
down_revision = "209f1e305279"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("season", sa.Column("max_elc_salary", sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("season", "max_elc_salary")
    # ### end Alembic commands ###
