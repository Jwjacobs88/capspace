import sys
from datetime import date

from app import db
from app.models import Asset, Contract, ContractYear


def create_contract(**kwargs):
    if not minimum_fields(kwargs):
        sys.exit(1)

    asset = create_asset(kwargs)
    contract = create_contract_record(asset, kwargs)
    years = create_contract_years(asset, contract, kwargs["year_data"])


def minimum_fields(kwargs):
    fields = [
        "originating_team",
        "person_id",
        "years",
        "total_value",
        "expiration_status",
        "entry_level",
        "year_data",
    ]
    for field in fields:
        if field not in kwargs:
            print(f"Missing field {field}")
            return False

    year_fields = [
        "season_id",
        "nhl_salary",
        "cap_hit",
        "aav",
    ]
    for season in kwargs["year_data"]:
        for field in year_fields:
            if field not in season:
                print(f"Missing field contract_year.{field}", file=sys.stderr)
                return False

    return True


def create_asset(kwargs):
    asset = Asset(originating_team=kwargs["originating_team"], active=True)
    if "current_team" in kwargs:
        asset.current_team = kwargs["current_team"]
    if "active" in kwargs:
        asset.active = kwargs["active"]
    db.session.add(asset)
    db.session.commit()
    return asset


def create_contract_record(asset, kwargs):
    contract = Contract(
        asset_id=asset.id,
        person_id=kwargs["person_id"],
        years=kwargs["years"],
        total_value=kwargs["total_value"],
        expiration_status=kwargs["expiration_status"],
        entry_level=kwargs["entry_level"],
    )
    if "signing_date" in kwargs:
        try:
            contract.signing_date = date.fromisoformat(kwargs["signing_date"])
        except ValueError as e:
            print(f'{e}\n\tSkipping "signing_date"', file=sys.stderr)
    if "elc_slide" in kwargs:
        contract.elc_slide = kwargs["elc_slide"]
    if "non_roster" in kwargs:
        contract.non_roster = kwargs["non_roster"]
    db.session.add(contract)
    db.session.commit()
    return contract


def create_contract_years(asset, contract, year_data):
    for season in year_data:
        contract_year = ContractYear(
            contract_id=contract.id,
            season_id=season["season_id"],
            nhl_salary=season["nhl_salary"],
            cap_hit=season["cap_hit"],
            aav=season["aav"],
        )
        if "minors_salary" in season:
            contract_year.minors_salary = season["minors_salary"]
        else:
            contract_year.minors_salary = contract_year.nhl_salary
        if "performance_bonuses" in season:
            contract_year.performance_bonuses = season["performance_bonuses"]
        if "signing_bonus" in season:
            contract_year.signing_bonus = season["signing_bonus"]
        db.session.add(contract_year)
    db.session.commit()


if __name__ == "__main__":
    import json

    contracts = json.loads(open("contracts.json").read())
    for contract in contracts:
        create_contract(**contract)
