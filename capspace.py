import sys
import click
import coverage
from app import create_app, db
from app.models import (
    Person,
    Team,
    Season,
    Asset,
    Contract,
    ContractYear,
    BuyoutYear,
    DraftPick,
    Transaction,
    TransactionAsset,
    RetainedSalary,
    TeamSeason,
    Note,
    SigningRights,
    DailyCap,
    GoalieSeason,
    SkaterSeason,
    Alias,
)

app = create_app("default")


@app.shell_context_processor
def make_shell_context():
    return {
        "db": db,
        "Person": Person,
        "Team": Team,
        "Season": Season,
        "Asset": Asset,
        "Contract": Contract,
        "ContractYear": ContractYear,
        "BuyoutYear": BuyoutYear,
        "DraftPick": DraftPick,
        "Transaction": Transaction,
        "TransactionAsset": TransactionAsset,
        "RetainedSalary": RetainedSalary,
        "TeamSeason": TeamSeason,
        "Note": Note,
        "SigningRights": SigningRights,
        "DailyCap": DailyCap,
        "GoalieSeason": GoalieSeason,
        "SkaterSeason": SkaterSeason,
        "Alias": Alias,
    }


@app.cli.command()
@click.option(
    "--coverage/--no-coverage",
    "check_coverage",
    default=False,
    help="Run tests under coverage.py",
)
@click.argument("names", nargs=-1)
def test(check_coverage, names):
    import unittest

    if check_coverage:
        import coverage

        cov = coverage.coverage(branch=True, include="app/*")
        cov.start()
    if names:
        tests = unittest.TestLoader().loadTestsFromNames(names)
    else:
        tests = unittest.TestLoader().discover("test")
    results = unittest.TextTestRunner(verbosity=2).run(tests)
    fail_count = len(results.errors) + len(results.failures)
    if check_coverage:
        cov.stop()
        cov.save()
        print("Summary: ")
        cov.report()
        cov.erase()
    sys.exit(fail_count)
